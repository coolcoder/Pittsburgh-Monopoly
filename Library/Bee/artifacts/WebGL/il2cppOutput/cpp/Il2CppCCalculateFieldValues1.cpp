﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<System.Boolean>
struct Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C;
// System.Action`1<UnityEngine.Camera>
struct Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA;
// System.Action`1<UnityEngine.Font>
struct Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC;
// System.Action`1<System.Int32>
struct Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404;
// System.Action`1<System.IntPtr>
struct Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8;
// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>>
struct Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t6AC338B3CEB934A66B363042F19213FE666F6818;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_tEFC8016EC28460E6CE058A5F413FAB656883AA5F;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph>
struct Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7;
// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>>
struct Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9;
// System.Func`1<System.Boolean>
struct Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457;
// System.Func`2<System.Exception,System.Boolean>
struct Func_2_tDDBE08B46BEFDD869DE0B97D023CB9C89674FED6;
// System.Func`3<System.Int32,System.IntPtr,System.Boolean>
struct Func_3_t2376B3D8C7A437FC32F21C4C4E4B3E7D2302007C;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_tA5BCD116CC751A5F35C7D3D7B96DC3A5D22B9C82;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t67A1600A303BB89506DFD21B59687088B7E0675B;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A;
// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode>
struct List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t1BA4BCF4D4D32DF07E9B84F1750D964DF33B0FEC;
// UnityEngine.TextCore.Glyph[]
struct GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5;
// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[]
struct GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E;
// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[]
struct GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7;
// UnityEngine.TextCore.GlyphRect[]
struct GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t8869694C217655DA7B1315DC02C80F1308B78B78;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// PW.Product[]
struct ProductU5BU5D_t43FCCECDF03ACE141268985EA3F7358784CEE920;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
// System.UInt32[]
struct UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// UnityEngine.AnimationCurve
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354;
// UnityEngine.AnimationState
struct AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE;
// UnityEngine.Animator
struct Animator_t8A52E42AE54F76681838FE9E632683EF3952E883;
// System.Collections.ArrayList
struct ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A;
// UnityEngine.Yoga.BaselineFunction
struct BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679;
// PW.BewerageMaker
struct BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3;
// UnityEngine.UI.Button
struct Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A;
// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// PW.CookableProduct
struct CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9;
// PW.CookingGameObject
struct CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E;
// PW.DrinkableProduct
struct DrinkableProduct_t7E1CB310C37D13C6450F26F3EF6EF3C629AC7FDC;
// PW.FillCupHelper
struct FillCupHelper_tDEB2B35BCDCA764B728281A92DB81A5099E80A11;
// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6;
// UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F;
// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D;
// UnityEngine.GUISettings
struct GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847;
// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580;
// UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngineInternal.GenericStack
struct GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49;
// PW.HeatableProduct
struct HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// UnityEngine.Yoga.Logger
struct Logger_t092B1218ED93DD47180692D5761559B2054234A0;
// UnityEngine.Yoga.MeasureFunction
struct MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09;
// PW.Microwave
struct Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// PW.OrderGenerator
struct OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555;
// UnityEngine.UI.Outline
struct Outline_t9CF146E077DC65F441EDEC463AA6710374108084;
// UnityEngine.ParticleSystem
struct ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1;
// PW.PlayerSlots
struct PlayerSlots_t9ACB68E0686BD97247ABB9ABAFD440D739F69508;
// PW.ProductGameObject
struct ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011;
// PW.ProductWithCover
struct ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103;
// PW.ProgressHelper
struct ProgressHelper_t99435CC1BFDDD2BF74478C6E61ECB6827C080469;
// UnityEngine.UI.RawImage
struct RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179;
// PW.ReadyToServe
struct ReadyToServe_t471256A83D0E86C0CC1DEEC3AFBDE5FCFBE93867;
// UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5;
// PW.ServeOrder
struct ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30;
// PW.StoveGameObject
struct StoveGameObject_t60DB9BA20178D8177199C2B8CCECB35E520DCDA1;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.WeakReference
struct WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E;
// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345;
// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072;
// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler
struct SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177;
// PW.BasicGameEvents/OnOrderCancelled
struct OnOrderCancelled_t2E913C055B0B9B69F1529DF076D33EF0ABA814C3;
// PW.BasicGameEvents/OnOrderCompleted
struct OnOrderCompleted_t32F2E47CDFE18D499A2E2C5869C847C5D96FE854;
// PW.BasicGameEvents/OnPlaceHolderRequired
struct OnPlaceHolderRequired_tF15B3DFBCD3CD6F1267B73BA3681F8E15D857493;
// PW.BasicGameEvents/OnProductAddedToSlot
struct OnProductAddedToSlot_t6E89D675BEB0512A325DA0C8E7F4707F9A50C381;
// PW.BasicGameEvents/OnProductDeletedFromSlot
struct OnProductDeletedFromSlot_t62F10DC6573C41AAF6971911C3091126E76E7F1A;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98;
// UnityTemplateProject.SimpleCameraController/CameraState
struct CameraState_t20CBEE55211900851700899BE504EE3E90BB3080;

struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9;
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com;
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke;
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com;
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke;
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com;
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.ASN1
struct ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F  : public RuntimeObject
{
	// System.Byte Mono.Security.ASN1::m_nTag
	uint8_t ___m_nTag_0;
	// System.Byte[] Mono.Security.ASN1::m_aValue
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_aValue_1;
	// System.Collections.ArrayList Mono.Security.ASN1::elist
	ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A* ___elist_2;
};

// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};

// UnityEngine.Experimental.Audio.AudioSampleProvider
struct AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2  : public RuntimeObject
{
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesAvailable
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesAvailable_0;
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesOverflow
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesOverflow_1;
};

// UnityEngine.AudioSettings
struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD  : public RuntimeObject
{
};

struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields
{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177* ___OnAudioConfigurationChanged_0;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemShuttingDown
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemShuttingDown_1;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemStartedUp
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemStartedUp_2;
};

// UnityEngine.TextCore.LowLevel.FontEngine
struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A  : public RuntimeObject
{
};

struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields
{
	// UnityEngine.TextCore.Glyph[] UnityEngine.TextCore.LowLevel.FontEngine::s_Glyphs
	GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5* ___s_Glyphs_0;
	// System.UInt32[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphIndexes_MarshallingArray_A
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___s_GlyphIndexes_MarshallingArray_A_1;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_IN
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_IN_2;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_OUT
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_OUT_3;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_FreeGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_FreeGlyphRects_4;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_UsedGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_UsedGlyphRects_5;
	// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[] UnityEngine.TextCore.LowLevel.FontEngine::s_PairAdjustmentRecords_MarshallingArray
	GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7* ___s_PairAdjustmentRecords_MarshallingArray_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph> UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphLookupDictionary
	Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7* ___s_GlyphLookupDictionary_7;
};

// UnityEngine.GUIUtility
struct GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A  : public RuntimeObject
{
};

struct GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields
{
	// System.Int32 UnityEngine.GUIUtility::s_ControlCount
	int32_t ___s_ControlCount_0;
	// System.Int32 UnityEngine.GUIUtility::s_SkinMode
	int32_t ___s_SkinMode_1;
	// System.Int32 UnityEngine.GUIUtility::s_OriginalID
	int32_t ___s_OriginalID_2;
	// System.Action UnityEngine.GUIUtility::takeCapture
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___takeCapture_3;
	// System.Action UnityEngine.GUIUtility::releaseCapture
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___releaseCapture_4;
	// System.Func`3<System.Int32,System.IntPtr,System.Boolean> UnityEngine.GUIUtility::processEvent
	Func_3_t2376B3D8C7A437FC32F21C4C4E4B3E7D2302007C* ___processEvent_5;
	// System.Action UnityEngine.GUIUtility::cleanupRoots
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___cleanupRoots_6;
	// System.Func`2<System.Exception,System.Boolean> UnityEngine.GUIUtility::endContainerGUIFromException
	Func_2_tDDBE08B46BEFDD869DE0B97D023CB9C89674FED6* ___endContainerGUIFromException_7;
	// System.Action UnityEngine.GUIUtility::guiChanged
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___guiChanged_8;
	// System.Boolean UnityEngine.GUIUtility::<guiIsExiting>k__BackingField
	bool ___U3CguiIsExitingU3Ek__BackingField_9;
	// System.Func`1<System.Boolean> UnityEngine.GUIUtility::s_HasCurrentWindowKeyFocusFunc
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___s_HasCurrentWindowKeyFocusFunc_10;
};

// UnityEngine.Physics
struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56  : public RuntimeObject
{
};

struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields
{
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEvent
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEvent_0;
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEventCCD
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEventCCD_1;
};

// UnityEngine.Physics2D
struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D  : public RuntimeObject
{
};

struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76* ___m_LastDisabledRigidbody2D_0;
};

// UnityEngine.RectTransformUtility
struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244  : public RuntimeObject
{
};

struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields
{
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Corners_0;
};

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative
struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938  : public RuntimeObject
{
};

struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields
{
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::RepaintOverlayPanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___RepaintOverlayPanelsCallback_0;
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::UpdateRuntimePanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___UpdateRuntimePanelsCallback_1;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Xml.XmlReader
struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD  : public RuntimeObject
{
};

struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields
{
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;
};

// PW.BewerageMaker/<DoFillAnimation>d__23
struct U3CDoFillAnimationU3Ed__23_t02E2CD905F550B46D9ED72540B2147CF5DF661B6  : public RuntimeObject
{
	// System.Int32 PW.BewerageMaker/<DoFillAnimation>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.BewerageMaker/<DoFillAnimation>d__23::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.BewerageMaker PW.BewerageMaker/<DoFillAnimation>d__23::<>4__this
	BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3* ___U3CU3E4__this_2;
	// System.Single PW.BewerageMaker/<DoFillAnimation>d__23::<fillCurrent>5__2
	float ___U3CfillCurrentU3E5__2_3;
};

// PW.CookableProduct/<AnimateGoingToSlot>d__13
struct U3CAnimateGoingToSlotU3Ed__13_t9E32B5A36F314F1B0A646B04070F1BBECD786954  : public RuntimeObject
{
	// System.Int32 PW.CookableProduct/<AnimateGoingToSlot>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.CookableProduct/<AnimateGoingToSlot>d__13::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.CookableProduct PW.CookableProduct/<AnimateGoingToSlot>d__13::<>4__this
	CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9* ___U3CU3E4__this_2;
};

// PW.CookingGameObject/<Cooking>d__16
struct U3CCookingU3Ed__16_t477A3C0B021BBC3BF44AA61EB4F3E43F39446EC4  : public RuntimeObject
{
	// System.Int32 PW.CookingGameObject/<Cooking>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.CookingGameObject/<Cooking>d__16::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.CookingGameObject PW.CookingGameObject/<Cooking>d__16::<>4__this
	CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E* ___U3CU3E4__this_2;
	// System.Single PW.CookingGameObject/<Cooking>d__16::<curTime>5__2
	float ___U3CcurTimeU3E5__2_3;
};

// PW.DrinkableProduct/<AnimateGoingToSlot>d__0
struct U3CAnimateGoingToSlotU3Ed__0_t800C169F4245838F789E277FD2416F196B28297F  : public RuntimeObject
{
	// System.Int32 PW.DrinkableProduct/<AnimateGoingToSlot>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.DrinkableProduct/<AnimateGoingToSlot>d__0::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.DrinkableProduct PW.DrinkableProduct/<AnimateGoingToSlot>d__0::<>4__this
	DrinkableProduct_t7E1CB310C37D13C6450F26F3EF6EF3C629AC7FDC* ___U3CU3E4__this_2;
};

// PW.FillCupHelper/<FillAnimation>d__6
struct U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1  : public RuntimeObject
{
	// System.Int32 PW.FillCupHelper/<FillAnimation>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.FillCupHelper/<FillAnimation>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.FillCupHelper PW.FillCupHelper/<FillAnimation>d__6::<>4__this
	FillCupHelper_tDEB2B35BCDCA764B728281A92DB81A5099E80A11* ___U3CU3E4__this_2;
	// System.Single PW.FillCupHelper/<FillAnimation>d__6::amount
	float ___amount_3;
	// System.Single PW.FillCupHelper/<FillAnimation>d__6::<timeAmount>5__2
	float ___U3CtimeAmountU3E5__2_4;
	// System.Single PW.FillCupHelper/<FillAnimation>d__6::<totalDist>5__3
	float ___U3CtotalDistU3E5__3_5;
	// System.Single PW.FillCupHelper/<FillAnimation>d__6::<totalScale>5__4
	float ___U3CtotalScaleU3E5__4_6;
};

// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60  : public RuntimeObject
{
	// System.Int32 UnityEngine.GUILayoutUtility/LayoutCache::<id>k__BackingField
	int32_t ___U3CidU3Ek__BackingField_0;
	// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility/LayoutCache::topLevel
	GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D* ___topLevel_1;
	// UnityEngineInternal.GenericStack UnityEngine.GUILayoutUtility/LayoutCache::layoutGroups
	GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49* ___layoutGroups_2;
	// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility/LayoutCache::windows
	GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D* ___windows_3;
};

// PW.HeatableProduct/<AnimateGoingToSlot>d__10
struct U3CAnimateGoingToSlotU3Ed__10_t6D7FB9E4228E747E03FDD0B84DAB5B67CAECD38A  : public RuntimeObject
{
	// System.Int32 PW.HeatableProduct/<AnimateGoingToSlot>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.HeatableProduct/<AnimateGoingToSlot>d__10::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.HeatableProduct PW.HeatableProduct/<AnimateGoingToSlot>d__10::<>4__this
	HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824* ___U3CU3E4__this_2;
};

// PW.HeatableProduct/<MoveToMicrowave>d__9
struct U3CMoveToMicrowaveU3Ed__9_t901B8B465F5D06C4FD70452B831F62AB40A78083  : public RuntimeObject
{
	// System.Int32 PW.HeatableProduct/<MoveToMicrowave>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.HeatableProduct/<MoveToMicrowave>d__9::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.HeatableProduct PW.HeatableProduct/<MoveToMicrowave>d__9::<>4__this
	HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824* ___U3CU3E4__this_2;
};

// PW.Microwave/<Heating>d__14
struct U3CHeatingU3Ed__14_t15955D9AD16B142EFE4EC01CC97A570FC472F7F2  : public RuntimeObject
{
	// System.Int32 PW.Microwave/<Heating>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.Microwave/<Heating>d__14::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.Microwave PW.Microwave/<Heating>d__14::<>4__this
	Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE* ___U3CU3E4__this_2;
	// System.Single PW.Microwave/<Heating>d__14::<curProcess>5__2
	float ___U3CcurProcessU3E5__2_3;
};

// PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12
struct U3COpenMicrowaveAndHeatProductU3Ed__12_tB118EFE51A52BAEAE9756C970D63F5F31A3AA323  : public RuntimeObject
{
	// System.Int32 PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.Microwave PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::<>4__this
	Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE* ___U3CU3E4__this_2;
};

// PW.Microwave/<PlayDoorAnim>d__13
struct U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B  : public RuntimeObject
{
	// System.Int32 PW.Microwave/<PlayDoorAnim>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.Microwave/<PlayDoorAnim>d__13::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.Microwave PW.Microwave/<PlayDoorAnim>d__13::<>4__this
	Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE* ___U3CU3E4__this_2;
	// System.Boolean PW.Microwave/<PlayDoorAnim>d__13::open
	bool ___open_3;
	// System.Boolean PW.Microwave/<PlayDoorAnim>d__13::alsoReverse
	bool ___alsoReverse_4;
	// System.Single PW.Microwave/<PlayDoorAnim>d__13::<totalTime>5__2
	float ___U3CtotalTimeU3E5__2_5;
	// System.Single PW.Microwave/<PlayDoorAnim>d__13::<curTime>5__3
	float ___U3CcurTimeU3E5__3_6;
	// System.Single PW.Microwave/<PlayDoorAnim>d__13::<totalAngle>5__4
	float ___U3CtotalAngleU3E5__4_7;
	// System.Single PW.Microwave/<PlayDoorAnim>d__13::<multiplier>5__5
	float ___U3CmultiplierU3E5__5_8;
	// System.Single PW.Microwave/<PlayDoorAnim>d__13::<finalAngle>5__6
	float ___U3CfinalAngleU3E5__6_9;
};

// PW.OrderGenerator/<GenerateOrderRoutine>d__11
struct U3CGenerateOrderRoutineU3Ed__11_t6D86BC9D0B281E30D54EE586F033C7F87E67961C  : public RuntimeObject
{
	// System.Int32 PW.OrderGenerator/<GenerateOrderRoutine>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.OrderGenerator/<GenerateOrderRoutine>d__11::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.OrderGenerator PW.OrderGenerator/<GenerateOrderRoutine>d__11::<>4__this
	OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555* ___U3CU3E4__this_2;
	// System.Single PW.OrderGenerator/<GenerateOrderRoutine>d__11::intervalTime
	float ___intervalTime_3;
};

// PW.ProductManager/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t2CBA28A751DF6FEB484554653AFF6BAD965BBB19  : public RuntimeObject
{
	// System.Int32 PW.ProductManager/<>c__DisplayClass6_0::orderID
	int32_t ___orderID_0;
};

// PW.ReadyToServe/<AnimateGoingToSlot>d__4
struct U3CAnimateGoingToSlotU3Ed__4_t9BB07FD98271F129F63C328344CCC8D40B7C8233  : public RuntimeObject
{
	// System.Int32 PW.ReadyToServe/<AnimateGoingToSlot>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.ReadyToServe/<AnimateGoingToSlot>d__4::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.ReadyToServe PW.ReadyToServe/<AnimateGoingToSlot>d__4::<>4__this
	ReadyToServe_t471256A83D0E86C0CC1DEEC3AFBDE5FCFBE93867* ___U3CU3E4__this_2;
};

// UnityTemplateProject.SimpleCameraController/CameraState
struct CameraState_t20CBEE55211900851700899BE504EE3E90BB3080  : public RuntimeObject
{
	// System.Single UnityTemplateProject.SimpleCameraController/CameraState::yaw
	float ___yaw_0;
	// System.Single UnityTemplateProject.SimpleCameraController/CameraState::pitch
	float ___pitch_1;
	// System.Single UnityTemplateProject.SimpleCameraController/CameraState::roll
	float ___roll_2;
	// System.Single UnityTemplateProject.SimpleCameraController/CameraState::x
	float ___x_3;
	// System.Single UnityTemplateProject.SimpleCameraController/CameraState::y
	float ___y_4;
	// System.Single UnityTemplateProject.SimpleCameraController/CameraState::z
	float ___z_5;
};

// PW.StoveGameObject/<PlayDoorAnim>d__6
struct U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D  : public RuntimeObject
{
	// System.Int32 PW.StoveGameObject/<PlayDoorAnim>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.StoveGameObject/<PlayDoorAnim>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.StoveGameObject PW.StoveGameObject/<PlayDoorAnim>d__6::<>4__this
	StoveGameObject_t60DB9BA20178D8177199C2B8CCECB35E520DCDA1* ___U3CU3E4__this_2;
	// System.Boolean PW.StoveGameObject/<PlayDoorAnim>d__6::open
	bool ___open_3;
	// System.Boolean PW.StoveGameObject/<PlayDoorAnim>d__6::alsoReverse
	bool ___alsoReverse_4;
	// System.Single PW.StoveGameObject/<PlayDoorAnim>d__6::<totalTime>5__2
	float ___U3CtotalTimeU3E5__2_5;
	// System.Single PW.StoveGameObject/<PlayDoorAnim>d__6::<curTime>5__3
	float ___U3CcurTimeU3E5__3_6;
	// System.Single PW.StoveGameObject/<PlayDoorAnim>d__6::<totalAngle>5__4
	float ___U3CtotalAngleU3E5__4_7;
	// System.Single PW.StoveGameObject/<PlayDoorAnim>d__6::<multiplier>5__5
	float ___U3CmultiplierU3E5__5_8;
	// System.Single PW.StoveGameObject/<PlayDoorAnim>d__6::<finalAngle>5__6
	float ___U3CfinalAngleU3E5__6_9;
};

// UnityEngine.AnimatorClipInfo
struct AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 
{
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;
};

// UnityEngine.AnimatorStateInfo
struct AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 
{
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;
};

// UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD 
{
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_FullPath
	int32_t ___m_FullPath_0;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_UserName
	int32_t ___m_UserName_1;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_Name
	int32_t ___m_Name_2;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_HasFixedDuration
	bool ___m_HasFixedDuration_3;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_Duration
	float ___m_Duration_4;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_NormalizedTime
	float ___m_NormalizedTime_5;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_AnyState
	bool ___m_AnyState_6;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_TransitionType
	int32_t ___m_TransitionType_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_pinvoke
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};
// Native definition for COM marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_com
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};

// UnityEngine.AssetFileNameExtensionAttribute
struct AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.AssetFileNameExtensionAttribute::<preferredExtension>k__BackingField
	String_t* ___U3CpreferredExtensionU3Ek__BackingField_0;
	// System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.AssetFileNameExtensionAttribute::<otherExtensions>k__BackingField
	RuntimeObject* ___U3CotherExtensionsU3Ek__BackingField_1;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// UnityEngine.UIElements.UIR.DrawBufferRange
struct DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4 
{
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::firstIndex
	int32_t ___firstIndex_0;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::indexCount
	int32_t ___indexCount_1;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::minIndexVal
	int32_t ___minIndexVal_2;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::vertsReferenced
	int32_t ___vertsReferenced_3;
};

// UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756 
{
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_FaceIndex
	int32_t ___m_FaceIndex_0;
	// System.String UnityEngine.TextCore.FaceInfo::m_FamilyName
	String_t* ___m_FamilyName_1;
	// System.String UnityEngine.TextCore.FaceInfo::m_StyleName
	String_t* ___m_StyleName_2;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_PointSize
	int32_t ___m_PointSize_3;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Scale
	float ___m_Scale_4;
	// System.Single UnityEngine.TextCore.FaceInfo::m_LineHeight
	float ___m_LineHeight_5;
	// System.Single UnityEngine.TextCore.FaceInfo::m_AscentLine
	float ___m_AscentLine_6;
	// System.Single UnityEngine.TextCore.FaceInfo::m_CapLine
	float ___m_CapLine_7;
	// System.Single UnityEngine.TextCore.FaceInfo::m_MeanLine
	float ___m_MeanLine_8;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Baseline
	float ___m_Baseline_9;
	// System.Single UnityEngine.TextCore.FaceInfo::m_DescentLine
	float ___m_DescentLine_10;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptOffset
	float ___m_SuperscriptOffset_11;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptSize
	float ___m_SuperscriptSize_12;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptOffset
	float ___m_SubscriptOffset_13;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptSize
	float ___m_SubscriptSize_14;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineOffset
	float ___m_UnderlineOffset_15;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineThickness
	float ___m_UnderlineThickness_16;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughOffset
	float ___m_StrikethroughOffset_17;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughThickness
	float ___m_StrikethroughThickness_18;
	// System.Single UnityEngine.TextCore.FaceInfo::m_TabWidth
	float ___m_TabWidth_19;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_pinvoke
{
	int32_t ___m_FaceIndex_0;
	char* ___m_FamilyName_1;
	char* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	float ___m_LineHeight_5;
	float ___m_AscentLine_6;
	float ___m_CapLine_7;
	float ___m_MeanLine_8;
	float ___m_Baseline_9;
	float ___m_DescentLine_10;
	float ___m_SuperscriptOffset_11;
	float ___m_SuperscriptSize_12;
	float ___m_SubscriptOffset_13;
	float ___m_SubscriptSize_14;
	float ___m_UnderlineOffset_15;
	float ___m_UnderlineThickness_16;
	float ___m_StrikethroughOffset_17;
	float ___m_StrikethroughThickness_18;
	float ___m_TabWidth_19;
};
// Native definition for COM marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_com
{
	int32_t ___m_FaceIndex_0;
	Il2CppChar* ___m_FamilyName_1;
	Il2CppChar* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	float ___m_LineHeight_5;
	float ___m_AscentLine_6;
	float ___m_CapLine_7;
	float ___m_MeanLine_8;
	float ___m_Baseline_9;
	float ___m_DescentLine_10;
	float ___m_SuperscriptOffset_11;
	float ___m_SuperscriptSize_12;
	float ___m_SubscriptOffset_13;
	float ___m_SubscriptSize_14;
	float ___m_UnderlineOffset_15;
	float ___m_UnderlineThickness_16;
	float ___m_StrikethroughOffset_17;
	float ___m_StrikethroughThickness_18;
	float ___m_TabWidth_19;
};

// UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172 
{
	// System.String UnityEngine.TextCore.LowLevel.FontReference::familyName
	String_t* ___familyName_0;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::styleName
	String_t* ___styleName_1;
	// System.Int32 UnityEngine.TextCore.LowLevel.FontReference::faceIndex
	int32_t ___faceIndex_2;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::filePath
	String_t* ___filePath_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_pinvoke
{
	char* ___familyName_0;
	char* ___styleName_1;
	int32_t ___faceIndex_2;
	char* ___filePath_3;
};
// Native definition for COM marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_com
{
	Il2CppChar* ___familyName_0;
	Il2CppChar* ___styleName_1;
	int32_t ___faceIndex_2;
	Il2CppChar* ___filePath_3;
};

// UnityEngine.GUITargetAttribute
struct GUITargetAttribute_t3F08CE7E00D79CB555B94AA7FA1BCB4B144B922B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Int32 UnityEngine.GUITargetAttribute::displayMask
	int32_t ___displayMask_0;
};

// UnityEngine.TextCore.GlyphMetrics
struct GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A 
{
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Width
	float ___m_Width_0;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Height
	float ___m_Height_1;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingX
	float ___m_HorizontalBearingX_2;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingY
	float ___m_HorizontalBearingY_3;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalAdvance
	float ___m_HorizontalAdvance_4;
};

// UnityEngine.TextCore.GlyphRect
struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D 
{
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Height
	int32_t ___m_Height_3;
};

struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields
{
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.GlyphRect::s_ZeroGlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___s_ZeroGlyphRect_4;
};

// UnityEngine.TextCore.LowLevel.GlyphValueRecord
struct GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E 
{
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XPlacement
	float ___m_XPlacement_0;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YPlacement
	float ___m_YPlacement_1;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XAdvance
	float ___m_XAdvance_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YAdvance
	float ___m_YAdvance_3;
};

// UnityEngine.Bindings.IgnoreAttribute
struct IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.IgnoreAttribute::<DoesNotContributeToSize>k__BackingField
	bool ___U3CDoesNotContributeToSizeU3Ek__BackingField_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;
	// System.String UnityEngine.NativeClassAttribute::<Declaration>k__BackingField
	String_t* ___U3CDeclarationU3Ek__BackingField_1;
};

// UnityEngine.Bindings.NativeConditionalAttribute
struct NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeConditionalAttribute::<Condition>k__BackingField
	String_t* ___U3CConditionU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Bindings.NativeConditionalAttribute::<Enabled>k__BackingField
	bool ___U3CEnabledU3Ek__BackingField_1;
};

// UnityEngine.Bindings.NativeHeaderAttribute
struct NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeHeaderAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeMethodAttribute
struct NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeMethodAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsThreadSafe>k__BackingField
	bool ___U3CIsThreadSafeU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsFreeFunction>k__BackingField
	bool ___U3CIsFreeFunctionU3Ek__BackingField_2;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<HasExplicitThis>k__BackingField
	bool ___U3CHasExplicitThisU3Ek__BackingField_4;
};

// UnityEngine.Bindings.NativeNameAttribute
struct NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeNameAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeThrowsAttribute
struct NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.NativeThrowsAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeTypeAttribute
struct NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<IntermediateScriptingStructName>k__BackingField
	String_t* ___U3CIntermediateScriptingStructNameU3Ek__BackingField_1;
	// UnityEngine.Bindings.CodegenOptions UnityEngine.Bindings.NativeTypeAttribute::<CodegenOptions>k__BackingField
	int32_t ___U3CCodegenOptionsU3Ek__BackingField_2;
};

// UnityEngine.Bindings.NotNullAttribute
struct NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NotNullAttribute::<Exception>k__BackingField
	String_t* ___U3CExceptionU3Ek__BackingField_0;
};

// UnityEngine.PhysicsScene
struct PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE 
{
	// System.Int32 UnityEngine.PhysicsScene::m_Handle
	int32_t ___m_Handle_0;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<GenerateProxy>k__BackingField
	bool ___U3CGenerateProxyU3Ek__BackingField_2;
};

// UnityEngine.Bindings.StaticAccessorAttribute
struct StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.StaticAccessorAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// UnityEngine.Bindings.StaticAccessorType UnityEngine.Bindings.StaticAccessorAttribute::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
};

// UnityEngine.UILineInfo
struct UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC 
{
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;
};

// System.UIntPtr
struct UIntPtr_t 
{
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;
};

struct UIntPtr_t_StaticFields
{
	// System.UIntPtr System.UIntPtr::Zero
	uintptr_t ___Zero_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// UnityEngine.Yoga.YogaSize
struct YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA 
{
	// System.Single UnityEngine.Yoga.YogaSize::width
	float ___width_0;
	// System.Single UnityEngine.Yoga.YogaSize::height
	float ___height_1;
};

// UnityEngine.Yoga.YogaValue
struct YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C 
{
	// System.Single UnityEngine.Yoga.YogaValue::value
	float ___value_0;
	// UnityEngine.Yoga.YogaUnit UnityEngine.Yoga.YogaValue::unit
	int32_t ___unit_1;
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D12_t1BDD2193C3F925556BCD5FF35C0AC52DDB0CAB8F 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t1BDD2193C3F925556BCD5FF35C0AC52DDB0CAB8F__padding[12];
	};
};
#pragma pack(pop, tp)

// UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_pinvoke
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_com
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA  : public RuntimeObject
{
};

struct U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA_StaticFields
{
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::8688D249E9D047B4FC2FB89CE05AFE9EC89252FFCCDD969DE6EEF260DD7FFB21
	__StaticArrayInitTypeSizeU3D12_t1BDD2193C3F925556BCD5FF35C0AC52DDB0CAB8F ___8688D249E9D047B4FC2FB89CE05AFE9EC89252FFCCDD969DE6EEF260DD7FFB21_0;
};

// UnityEngine.Animations.AnimationHumanStream
struct AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC 
{
	// System.IntPtr UnityEngine.Animations.AnimationHumanStream::stream
	intptr_t ___stream_0;
};

// UnityEngine.Animations.AnimationStream
struct AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A 
{
	// System.UInt32 UnityEngine.Animations.AnimationStream::m_AnimatorBindingsVersion
	uint32_t ___m_AnimatorBindingsVersion_0;
	// System.IntPtr UnityEngine.Animations.AnimationStream::constant
	intptr_t ___constant_1;
	// System.IntPtr UnityEngine.Animations.AnimationStream::input
	intptr_t ___input_2;
	// System.IntPtr UnityEngine.Animations.AnimationStream::output
	intptr_t ___output_3;
	// System.IntPtr UnityEngine.Animations.AnimationStream::workspace
	intptr_t ___workspace_4;
	// System.IntPtr UnityEngine.Animations.AnimationStream::inputStreamAccessor
	intptr_t ___inputStreamAccessor_5;
	// System.IntPtr UnityEngine.Animations.AnimationStream::animationHandleBinder
	intptr_t ___animationHandleBinder_6;
};

// UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0  : public RuntimeObject
{
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	// UnityEngine.Component UnityEngine.Collision::m_Body
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	// System.Int32 UnityEngine.Collision::m_ContactCount
	int32_t ___m_ContactCount_4;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_ReusedContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_ReusedContacts_5;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_LegacyContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_LegacyContacts_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};

// UnityEngine.ContactPoint
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 
{
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;
};

// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92  : public RuntimeObject
{
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_pinvoke
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_com
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};

// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F  : public RuntimeObject
{
	// System.Single UnityEngine.GUILayoutEntry::minWidth
	float ___minWidth_0;
	// System.Single UnityEngine.GUILayoutEntry::maxWidth
	float ___maxWidth_1;
	// System.Single UnityEngine.GUILayoutEntry::minHeight
	float ___minHeight_2;
	// System.Single UnityEngine.GUILayoutEntry::maxHeight
	float ___maxHeight_3;
	// UnityEngine.Rect UnityEngine.GUILayoutEntry::rect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect_4;
	// System.Int32 UnityEngine.GUILayoutEntry::stretchWidth
	int32_t ___stretchWidth_5;
	// System.Int32 UnityEngine.GUILayoutEntry::stretchHeight
	int32_t ___stretchHeight_6;
	// System.Boolean UnityEngine.GUILayoutEntry::consideredForMargin
	bool ___consideredForMargin_7;
	// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::m_Style
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_Style_8;
};

struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F_StaticFields
{
	// UnityEngine.Rect UnityEngine.GUILayoutEntry::kDummyRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___kDummyRect_9;
	// System.Int32 UnityEngine.GUILayoutEntry::indent
	int32_t ___indent_10;
};

// UnityEngine.GUILayoutUtility
struct GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F  : public RuntimeObject
{
};

struct GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache> UnityEngine.GUILayoutUtility::s_StoredLayouts
	Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598* ___s_StoredLayouts_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache> UnityEngine.GUILayoutUtility::s_StoredWindows
	Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598* ___s_StoredWindows_1;
	// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::current
	LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60* ___current_2;
	// UnityEngine.Rect UnityEngine.GUILayoutUtility::kDummyRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___kDummyRect_3;
};

// UnityEngine.GUISettings
struct GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847  : public RuntimeObject
{
	// System.Boolean UnityEngine.GUISettings::m_DoubleClickSelectsWord
	bool ___m_DoubleClickSelectsWord_0;
	// System.Boolean UnityEngine.GUISettings::m_TripleClickSelectsLine
	bool ___m_TripleClickSelectsLine_1;
	// UnityEngine.Color UnityEngine.GUISettings::m_CursorColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_CursorColor_2;
	// System.Single UnityEngine.GUISettings::m_CursorFlashSpeed
	float ___m_CursorFlashSpeed_3;
	// UnityEngine.Color UnityEngine.GUISettings::m_SelectionColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectionColor_4;
};

// UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95  : public RuntimeObject
{
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_SourceStyle_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com* ___m_SourceStyle_1;
};

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange
struct GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97 
{
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::offsetFromWriteStart
	uint32_t ___offsetFromWriteStart_0;
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::size
	uint32_t ___size_1;
	// System.UIntPtr UnityEngine.UIElements.UIR.GfxUpdateBufferRange::source
	uintptr_t ___source_2;
};

// UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F  : public RuntimeObject
{
	// System.UInt32 UnityEngine.TextCore.Glyph::m_Index
	uint32_t ___m_Index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.Glyph::m_Metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.Glyph::m_GlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	// System.Single UnityEngine.TextCore.Glyph::m_Scale
	float ___m_Scale_3;
	// System.Int32 UnityEngine.TextCore.Glyph::m_AtlasIndex
	int32_t ___m_AtlasIndex_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_pinvoke
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_com
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
};

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord
struct GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphIndex
	uint32_t ___m_GlyphIndex_0;
	// UnityEngine.TextCore.LowLevel.GlyphValueRecord UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphValueRecord
	GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E ___m_GlyphValueRecord_1;
};

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct
struct GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::index
	uint32_t ___index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::glyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___glyphRect_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::scale
	float ___scale_3;
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::atlasIndex
	int32_t ___atlasIndex_4;
};

// UnityEngine.HumanLimit
struct HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E 
{
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Min
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Min_0;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Max
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Max_1;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Center
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Center_2;
	// System.Single UnityEngine.HumanLimit::m_AxisLength
	float ___m_AxisLength_3;
	// System.Int32 UnityEngine.HumanLimit::m_UseDefaultValues
	int32_t ___m_UseDefaultValues_4;
};

// UnityEngine.ModifiableContactPair
struct ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960 
{
	// System.IntPtr UnityEngine.ModifiableContactPair::actor
	intptr_t ___actor_0;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherActor
	intptr_t ___otherActor_1;
	// System.IntPtr UnityEngine.ModifiableContactPair::shape
	intptr_t ___shape_2;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherShape
	intptr_t ___otherShape_3;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_4;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_5;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::otherRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___otherRotation_6;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::otherPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___otherPosition_7;
	// System.Int32 UnityEngine.ModifiableContactPair::numContacts
	int32_t ___numContacts_8;
	// System.IntPtr UnityEngine.ModifiableContactPair::contacts
	intptr_t ___contacts_9;
};

// UnityEngine.Bindings.NativePropertyAttribute
struct NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
	// UnityEngine.Bindings.TargetType UnityEngine.Bindings.NativePropertyAttribute::<TargetType>k__BackingField
	int32_t ___U3CTargetTypeU3Ek__BackingField_5;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ObjectGUIState
struct ObjectGUIState_t7BE88DC8B9C7187A77D63BBCBE9DB7B674863C15  : public RuntimeObject
{
	// System.IntPtr UnityEngine.ObjectGUIState::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Playables.PlayableHandle
struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 
{
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;
};

struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4_StaticFields
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::m_Null
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Null_2;
};

// UnityEngine.Playables.PlayableOutputHandle
struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 
{
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;
};

struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883_StaticFields
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::m_Null
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Null_2;
};

// PW.Product
struct Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4  : public RuntimeObject
{
	// UnityEngine.GameObject PW.Product::productPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___productPrefab_0;
	// System.Int32 PW.Product::orderID
	int32_t ___orderID_1;
	// System.String PW.Product::productName
	String_t* ___productName_2;
	// System.Single PW.Product::productPrice
	float ___productPrice_3;
	// ProductType PW.Product::productType
	int32_t ___productType_4;
	// System.Boolean PW.Product::addToPlateBeforeServed
	bool ___addToPlateBeforeServed_5;
	// System.Boolean PW.Product::dontIncludeInThisScene
	bool ___dontIncludeInThisScene_6;
	// UnityEngine.GameObject PW.Product::servedAsDifferentGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___servedAsDifferentGameObject_7;
	// UnityEngine.Vector3 PW.Product::plateOffset
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___plateOffset_8;
	// System.Boolean PW.Product::RegenerateProduct
	bool ___RegenerateProduct_9;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.RaycastHit
struct RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 
{
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5  : public RuntimeObject
{
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject* ___m_SourceStyle_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};

// UnityEngine.SendMouseEvents
struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39  : public RuntimeObject
{
};

struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields
{
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B* ___m_Cameras_4;
	// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>> UnityEngine.SendMouseEvents::s_GetMouseState
	Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9* ___s_GetMouseState_5;
	// UnityEngine.Vector2 UnityEngine.SendMouseEvents::s_MousePosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___s_MousePosition_6;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonPressedThisFrame
	bool ___s_MouseButtonPressedThisFrame_7;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonIsPressed
	bool ___s_MouseButtonIsPressed_8;
};

// UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126 
{
	// System.String UnityEngine.SkeletonBone::name
	String_t* ___name_0;
	// System.String UnityEngine.SkeletonBone::parentName
	String_t* ___parentName_1;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	// UnityEngine.Quaternion UnityEngine.SkeletonBone::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::scale
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke
{
	char* ___name_0;
	char* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for COM marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};

// UnityEngine.TextEditor
struct TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27  : public RuntimeObject
{
	// UnityEngine.TouchScreenKeyboard UnityEngine.TextEditor::keyboardOnScreen
	TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A* ___keyboardOnScreen_0;
	// System.Int32 UnityEngine.TextEditor::controlID
	int32_t ___controlID_1;
	// UnityEngine.GUIStyle UnityEngine.TextEditor::style
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___style_2;
	// System.Boolean UnityEngine.TextEditor::multiline
	bool ___multiline_3;
	// System.Boolean UnityEngine.TextEditor::hasHorizontalCursorPos
	bool ___hasHorizontalCursorPos_4;
	// System.Boolean UnityEngine.TextEditor::isPasswordField
	bool ___isPasswordField_5;
	// System.Boolean UnityEngine.TextEditor::m_HasFocus
	bool ___m_HasFocus_6;
	// UnityEngine.Vector2 UnityEngine.TextEditor::scrollOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___scrollOffset_7;
	// UnityEngine.GUIContent UnityEngine.TextEditor::m_Content
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___m_Content_8;
	// UnityEngine.Rect UnityEngine.TextEditor::m_Position
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___m_Position_9;
	// System.Int32 UnityEngine.TextEditor::m_CursorIndex
	int32_t ___m_CursorIndex_10;
	// System.Int32 UnityEngine.TextEditor::m_SelectIndex
	int32_t ___m_SelectIndex_11;
	// System.Boolean UnityEngine.TextEditor::m_RevealCursor
	bool ___m_RevealCursor_12;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalCursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphicalCursorPos_13;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalSelectCursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphicalSelectCursorPos_14;
	// System.Boolean UnityEngine.TextEditor::m_MouseDragSelectsWholeWords
	bool ___m_MouseDragSelectsWholeWords_15;
	// System.Int32 UnityEngine.TextEditor::m_DblClickInitPos
	int32_t ___m_DblClickInitPos_16;
	// UnityEngine.TextEditor/DblClickSnapping UnityEngine.TextEditor::m_DblClickSnap
	uint8_t ___m_DblClickSnap_17;
	// System.Boolean UnityEngine.TextEditor::m_bJustSelected
	bool ___m_bJustSelected_18;
	// System.Int32 UnityEngine.TextEditor::m_iAltCursorPos
	int32_t ___m_iAltCursorPos_19;
	// System.String UnityEngine.TextEditor::oldText
	String_t* ___oldText_20;
	// System.Int32 UnityEngine.TextEditor::oldPos
	int32_t ___oldPos_21;
	// System.Int32 UnityEngine.TextEditor::oldSelectPos
	int32_t ___oldSelectPos_22;
};

struct TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27_StaticFields
{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp> UnityEngine.TextEditor::s_Keyactions
	Dictionary_2_t6AC338B3CEB934A66B363042F19213FE666F6818* ___s_Keyactions_23;
};

// UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 
{
	// UnityEngine.Font UnityEngine.TextGenerationSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	// UnityEngine.Color UnityEngine.TextGenerationSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	// System.Int32 UnityEngine.TextGenerationSettings::fontSize
	int32_t ___fontSize_2;
	// System.Single UnityEngine.TextGenerationSettings::lineSpacing
	float ___lineSpacing_3;
	// System.Boolean UnityEngine.TextGenerationSettings::richText
	bool ___richText_4;
	// System.Single UnityEngine.TextGenerationSettings::scaleFactor
	float ___scaleFactor_5;
	// UnityEngine.FontStyle UnityEngine.TextGenerationSettings::fontStyle
	int32_t ___fontStyle_6;
	// UnityEngine.TextAnchor UnityEngine.TextGenerationSettings::textAnchor
	int32_t ___textAnchor_7;
	// System.Boolean UnityEngine.TextGenerationSettings::alignByGeometry
	bool ___alignByGeometry_8;
	// System.Boolean UnityEngine.TextGenerationSettings::resizeTextForBestFit
	bool ___resizeTextForBestFit_9;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMinSize
	int32_t ___resizeTextMinSize_10;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMaxSize
	int32_t ___resizeTextMaxSize_11;
	// System.Boolean UnityEngine.TextGenerationSettings::updateBounds
	bool ___updateBounds_12;
	// UnityEngine.VerticalWrapMode UnityEngine.TextGenerationSettings::verticalOverflow
	int32_t ___verticalOverflow_13;
	// UnityEngine.HorizontalWrapMode UnityEngine.TextGenerationSettings::horizontalOverflow
	int32_t ___horizontalOverflow_14;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::generationExtents
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::pivot
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	// System.Boolean UnityEngine.TextGenerationSettings::generateOutOfBounds
	bool ___generateOutOfBounds_17;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
// Native definition for COM marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};

// UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062 
{
	// System.String UnityEngine.UIElements.TextNativeSettings::text
	String_t* ___text_0;
	// UnityEngine.Font UnityEngine.UIElements.TextNativeSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	// System.Int32 UnityEngine.UIElements.TextNativeSettings::size
	int32_t ___size_2;
	// System.Single UnityEngine.UIElements.TextNativeSettings::scaling
	float ___scaling_3;
	// UnityEngine.FontStyle UnityEngine.UIElements.TextNativeSettings::style
	int32_t ___style_4;
	// UnityEngine.Color UnityEngine.UIElements.TextNativeSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	// UnityEngine.TextAnchor UnityEngine.UIElements.TextNativeSettings::anchor
	int32_t ___anchor_6;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::wordWrap
	bool ___wordWrap_7;
	// System.Single UnityEngine.UIElements.TextNativeSettings::wordWrapWidth
	float ___wordWrapWidth_8;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::richText
	bool ___richText_9;
};
// Native definition for P/Invoke marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_pinvoke
{
	char* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};
// Native definition for COM marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_com
{
	Il2CppChar* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};

// UnityEngine.UIElements.TextVertex
struct TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3 
{
	// UnityEngine.Vector3 UnityEngine.UIElements.TextVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Color32 UnityEngine.UIElements.TextVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_1;
	// UnityEngine.Vector2 UnityEngine.UIElements.TextVertex::uv0
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv0_2;
};

// UnityEngine.Touch
struct Touch_t03E51455ED508492B3F278903A0114FA0E87B417 
{
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;
};

// UnityEngine.UICharInfo
struct UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD 
{
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;
};

// UnityEngine.UIVertex
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 
{
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal_1;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___tangent_2;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_3;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv0
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv0_4;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv1
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv1_5;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv2
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv2_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv3
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv3_7;
};

struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields
{
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 ___simpleVert_10;
};

// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaConfig::_ygConfig
	intptr_t ____ygConfig_1;
	// UnityEngine.Yoga.Logger UnityEngine.Yoga.YogaConfig::_logger
	Logger_t092B1218ED93DD47180692D5761559B2054234A0* ____logger_2;
};

struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields
{
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaConfig::Default
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ___Default_0;
};

// UnityEngine.Yoga.YogaNode
struct YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaNode::_ygNode
	intptr_t ____ygNode_0;
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaNode::_config
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ____config_1;
	// System.WeakReference UnityEngine.Yoga.YogaNode::_parent
	WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ____parent_2;
	// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode> UnityEngine.Yoga.YogaNode::_children
	List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165* ____children_3;
	// UnityEngine.Yoga.MeasureFunction UnityEngine.Yoga.YogaNode::_measureFunction
	MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09* ____measureFunction_4;
	// UnityEngine.Yoga.BaselineFunction UnityEngine.Yoga.YogaNode::_baselineFunction
	BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679* ____baselineFunction_5;
	// System.Object UnityEngine.Yoga.YogaNode::_data
	RuntimeObject* ____data_6;
};

// PW.BewerageMaker/<DoFillEnded>d__27
struct U3CDoFillEndedU3Ed__27_t76C751A3503DE1DF03BC6B529BC907B6C70ACDED  : public RuntimeObject
{
	// System.Int32 PW.BewerageMaker/<DoFillEnded>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.BewerageMaker/<DoFillEnded>d__27::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.BewerageMaker PW.BewerageMaker/<DoFillEnded>d__27::<>4__this
	BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3* ___U3CU3E4__this_2;
	// UnityEngine.Vector3 PW.BewerageMaker/<DoFillEnded>d__27::<totalDist>5__2
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CtotalDistU3E5__2_3;
	// UnityEngine.Vector3 PW.BewerageMaker/<DoFillEnded>d__27::<totalRot>5__3
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CtotalRotU3E5__3_4;
	// System.Single PW.BewerageMaker/<DoFillEnded>d__27::<reverseMove>5__4
	float ___U3CreverseMoveU3E5__4_5;
};

// PW.BewerageMaker/<DoPreFill>d__22
struct U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD  : public RuntimeObject
{
	// System.Int32 PW.BewerageMaker/<DoPreFill>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.BewerageMaker/<DoPreFill>d__22::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.BewerageMaker PW.BewerageMaker/<DoPreFill>d__22::<>4__this
	BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3* ___U3CU3E4__this_2;
	// UnityEngine.Transform PW.BewerageMaker/<DoPreFill>d__22::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_3;
	// UnityEngine.Transform PW.BewerageMaker/<DoPreFill>d__22::finalTweenValue
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___finalTweenValue_4;
	// System.Single PW.BewerageMaker/<DoPreFill>d__22::<curPreFill>5__2
	float ___U3CcurPreFillU3E5__2_5;
	// UnityEngine.Vector3 PW.BewerageMaker/<DoPreFill>d__22::<totalDist>5__3
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CtotalDistU3E5__3_6;
	// UnityEngine.Vector3 PW.BewerageMaker/<DoPreFill>d__22::<totalRot>5__4
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CtotalRotU3E5__4_7;
};

// PW.CookableProduct/<MoveToPlace>d__10
struct U3CMoveToPlaceU3Ed__10_t24377E81B7CA3046B4C129EC19CBB75DD979081E  : public RuntimeObject
{
	// System.Int32 PW.CookableProduct/<MoveToPlace>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.CookableProduct/<MoveToPlace>d__10::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.CookableProduct PW.CookableProduct/<MoveToPlace>d__10::<>4__this
	CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9* ___U3CU3E4__this_2;
	// UnityEngine.Vector3 PW.CookableProduct/<MoveToPlace>d__10::targetPos
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___targetPos_3;
};

// UnityEngine.ParticleSystem/Particle
struct Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D 
{
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_ParentRandomSeed
	uint32_t ___m_ParentRandomSeed_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_12;
	// System.Int32 UnityEngine.ParticleSystem/Particle::m_MeshIndex
	int32_t ___m_MeshIndex_13;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_14;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_15;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_Flags
	uint32_t ___m_Flags_16;
};

// PW.PlayerSlots/<DoEmphasize>d__6
struct U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7  : public RuntimeObject
{
	// System.Int32 PW.PlayerSlots/<DoEmphasize>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.PlayerSlots/<DoEmphasize>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.PlayerSlots PW.PlayerSlots/<DoEmphasize>d__6::<>4__this
	PlayerSlots_t9ACB68E0686BD97247ABB9ABAFD440D739F69508* ___U3CU3E4__this_2;
	// System.Int32 PW.PlayerSlots/<DoEmphasize>d__6::index
	int32_t ___index_3;
	// UnityEngine.UI.Image PW.PlayerSlots/<DoEmphasize>d__6::<uiImage>5__2
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___U3CuiImageU3E5__2_4;
	// UnityEngine.UI.Outline PW.PlayerSlots/<DoEmphasize>d__6::<outline>5__3
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___U3CoutlineU3E5__3_5;
	// UnityEngine.Color PW.PlayerSlots/<DoEmphasize>d__6::<outlineColor>5__4
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___U3CoutlineColorU3E5__4_6;
	// System.Single PW.PlayerSlots/<DoEmphasize>d__6::<totalTime>5__5
	float ___U3CtotalTimeU3E5__5_7;
	// System.Single PW.PlayerSlots/<DoEmphasize>d__6::<curTime>5__6
	float ___U3CcurTimeU3E5__6_8;
	// System.Single PW.PlayerSlots/<DoEmphasize>d__6::<alphaVal>5__7
	float ___U3CalphaValU3E5__7_9;
};

// PW.ProductGameObject/<AnimateGoingToSlot>d__6
struct U3CAnimateGoingToSlotU3Ed__6_tC49557D1D118EDB1760047C61A332214F555E525  : public RuntimeObject
{
	// System.Int32 PW.ProductGameObject/<AnimateGoingToSlot>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.ProductGameObject/<AnimateGoingToSlot>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.ProductGameObject PW.ProductGameObject/<AnimateGoingToSlot>d__6::<>4__this
	ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011* ___U3CU3E4__this_2;
	// System.Single PW.ProductGameObject/<AnimateGoingToSlot>d__6::<curTime>5__2
	float ___U3CcurTimeU3E5__2_3;
	// UnityEngine.Vector3 PW.ProductGameObject/<AnimateGoingToSlot>d__6::<totalDist>5__3
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CtotalDistU3E5__3_4;
};

// PW.ProductGameObject/<MoveToPlace>d__8
struct U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF  : public RuntimeObject
{
	// System.Int32 PW.ProductGameObject/<MoveToPlace>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.ProductGameObject/<MoveToPlace>d__8::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// UnityEngine.Vector3 PW.ProductGameObject/<MoveToPlace>d__8::targetPos
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___targetPos_2;
	// PW.ProductGameObject PW.ProductGameObject/<MoveToPlace>d__8::<>4__this
	ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011* ___U3CU3E4__this_3;
	// System.Single PW.ProductGameObject/<MoveToPlace>d__8::<totalTime>5__2
	float ___U3CtotalTimeU3E5__2_4;
	// System.Single PW.ProductGameObject/<MoveToPlace>d__8::<curTime>5__3
	float ___U3CcurTimeU3E5__3_5;
	// UnityEngine.Vector3 PW.ProductGameObject/<MoveToPlace>d__8::<totalDist>5__4
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CtotalDistU3E5__4_6;
};

// PW.ProductWithCover/<OpenCloseDisplay>d__9
struct U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86  : public RuntimeObject
{
	// System.Int32 PW.ProductWithCover/<OpenCloseDisplay>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.ProductWithCover/<OpenCloseDisplay>d__9::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.ProductWithCover PW.ProductWithCover/<OpenCloseDisplay>d__9::<>4__this
	ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103* ___U3CU3E4__this_2;
	// System.Boolean PW.ProductWithCover/<OpenCloseDisplay>d__9::open
	bool ___open_3;
	// System.Boolean PW.ProductWithCover/<OpenCloseDisplay>d__9::alsoReverse
	bool ___alsoReverse_4;
	// System.Single PW.ProductWithCover/<OpenCloseDisplay>d__9::<totalTime>5__2
	float ___U3CtotalTimeU3E5__2_5;
	// System.Single PW.ProductWithCover/<OpenCloseDisplay>d__9::<curTime>5__3
	float ___U3CcurTimeU3E5__3_6;
	// UnityEngine.Vector3 PW.ProductWithCover/<OpenCloseDisplay>d__9::<totalDist>5__4
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CtotalDistU3E5__4_7;
	// UnityEngine.Vector3 PW.ProductWithCover/<OpenCloseDisplay>d__9::<finalPos>5__5
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CfinalPosU3E5__5_8;
};

// PW.ServeOrder/<DoEmphasize>d__9
struct U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362  : public RuntimeObject
{
	// System.Int32 PW.ServeOrder/<DoEmphasize>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PW.ServeOrder/<DoEmphasize>d__9::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PW.ServeOrder PW.ServeOrder/<DoEmphasize>d__9::<>4__this
	ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30* ___U3CU3E4__this_2;
	// UnityEngine.UI.Outline PW.ServeOrder/<DoEmphasize>d__9::<outline>5__2
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___U3CoutlineU3E5__2_3;
	// UnityEngine.Color PW.ServeOrder/<DoEmphasize>d__9::<outlineColor>5__3
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___U3CoutlineColorU3E5__3_4;
	// System.Single PW.ServeOrder/<DoEmphasize>d__9::<totalTime>5__4
	float ___U3CtotalTimeU3E5__4_5;
	// System.Single PW.ServeOrder/<DoEmphasize>d__9::<curTime>5__5
	float ___U3CcurTimeU3E5__5_6;
	// System.Single PW.ServeOrder/<DoEmphasize>d__9::<alphaVal>5__6
	float ___U3CalphaValU3E5__6_7;
};

// UnityEngine.Animations.AnimationClipPlayable
struct AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174  : public RuntimeObject
{
	// System.Single UnityEngine.AnimationEvent::m_Time
	float ___m_Time_0;
	// System.String UnityEngine.AnimationEvent::m_FunctionName
	String_t* ___m_FunctionName_1;
	// System.String UnityEngine.AnimationEvent::m_StringParameter
	String_t* ___m_StringParameter_2;
	// UnityEngine.Object UnityEngine.AnimationEvent::m_ObjectReferenceParameter
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_ObjectReferenceParameter_3;
	// System.Single UnityEngine.AnimationEvent::m_FloatParameter
	float ___m_FloatParameter_4;
	// System.Int32 UnityEngine.AnimationEvent::m_IntParameter
	int32_t ___m_IntParameter_5;
	// System.Int32 UnityEngine.AnimationEvent::m_MessageOptions
	int32_t ___m_MessageOptions_6;
	// UnityEngine.AnimationEventSource UnityEngine.AnimationEvent::m_Source
	int32_t ___m_Source_7;
	// UnityEngine.AnimationState UnityEngine.AnimationEvent::m_StateSender
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::m_AnimatorStateInfo
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::m_AnimatorClipInfo
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_pinvoke
{
	float ___m_Time_0;
	char* ___m_FunctionName_1;
	char* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for COM marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_com
{
	float ___m_Time_0;
	Il2CppChar* ___m_FunctionName_1;
	Il2CppChar* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com* ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields
{
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMixerPlayable
struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields
{
	// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::m_NullPlayable
	AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields
{
	// UnityEngine.Animations.AnimationMotionXToDeltaPlayable UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_NullPlayable
	AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationOffsetPlayable
struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields
{
	// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::m_NullPlayable
	AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationPlayableOutput
struct AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationPosePlayable
struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationPosePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields
{
	// UnityEngine.Animations.AnimationPosePlayable UnityEngine.Animations.AnimationPosePlayable::m_NullPlayable
	AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationRemoveScalePlayable
struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationRemoveScalePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields
{
	// UnityEngine.Animations.AnimationRemoveScalePlayable UnityEngine.Animations.AnimationRemoveScalePlayable::m_NullPlayable
	AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationScriptPlayable
struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationScriptPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields
{
	// UnityEngine.Animations.AnimationScriptPlayable UnityEngine.Animations.AnimationScriptPlayable::m_NullPlayable
	AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimatorControllerPlayable
struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields
{
	// UnityEngine.Animations.AnimatorControllerPlayable UnityEngine.Animations.AnimatorControllerPlayable::m_NullPlayable
	AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A ___m_NullPlayable_1;
};

// UnityEngine.AudioClip
struct AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E* ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072* ___m_PCMSetPositionCallback_5;
};

// UnityEngine.Audio.AudioClipPlayable
struct AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioMixerPlayable
struct AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioPlayableOutput
struct AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1* ___m_FontTextureRebuildCallback_5;
};

struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields
{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC* ___textureRebuilt_4;
};

// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D  : public GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F
{
	// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry> UnityEngine.GUILayoutGroup::entries
	List_1_tA5BCD116CC751A5F35C7D3D7B96DC3A5D22B9C82* ___entries_11;
	// System.Boolean UnityEngine.GUILayoutGroup::isVertical
	bool ___isVertical_12;
	// System.Boolean UnityEngine.GUILayoutGroup::resetCoords
	bool ___resetCoords_13;
	// System.Single UnityEngine.GUILayoutGroup::spacing
	float ___spacing_14;
	// System.Boolean UnityEngine.GUILayoutGroup::sameSize
	bool ___sameSize_15;
	// System.Boolean UnityEngine.GUILayoutGroup::isWindow
	bool ___isWindow_16;
	// System.Int32 UnityEngine.GUILayoutGroup::windowID
	int32_t ___windowID_17;
	// System.Int32 UnityEngine.GUILayoutGroup::m_Cursor
	int32_t ___m_Cursor_18;
	// System.Int32 UnityEngine.GUILayoutGroup::m_StretchableCountX
	int32_t ___m_StretchableCountX_19;
	// System.Int32 UnityEngine.GUILayoutGroup::m_StretchableCountY
	int32_t ___m_StretchableCountY_20;
	// System.Boolean UnityEngine.GUILayoutGroup::m_UserSpecifiedWidth
	bool ___m_UserSpecifiedWidth_21;
	// System.Boolean UnityEngine.GUILayoutGroup::m_UserSpecifiedHeight
	bool ___m_UserSpecifiedHeight_22;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMinWidth
	float ___m_ChildMinWidth_23;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMaxWidth
	float ___m_ChildMaxWidth_24;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMinHeight
	float ___m_ChildMinHeight_25;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMaxHeight
	float ___m_ChildMaxHeight_26;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginLeft
	int32_t ___m_MarginLeft_27;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginRight
	int32_t ___m_MarginRight_28;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginTop
	int32_t ___m_MarginTop_29;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginBottom
	int32_t ___m_MarginBottom_30;
};

struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D_StaticFields
{
	// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::none
	GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F* ___none_31;
};

// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580  : public RuntimeObject
{
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Overflow_12;
	// System.String UnityEngine.GUIStyle::m_Name
	String_t* ___m_Name_13;
};

struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_StaticFields
{
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___s_None_15;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Border_9;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Overflow_12;
	char* ___m_Name_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Normal_1;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Hover_2;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Active_3;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Focused_4;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnFocused_8;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Border_9;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Padding_10;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Margin_11;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Overflow_12;
	Il2CppChar* ___m_Name_13;
};

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord
struct GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E 
{
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FirstAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_FirstAdjustmentRecord_0;
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_SecondAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_SecondAdjustmentRecord_1;
	// UnityEngine.TextCore.LowLevel.FontFeatureLookupFlags UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FeatureLookupFlags
	int32_t ___m_FeatureLookupFlags_2;
};

// UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8 
{
	// System.String UnityEngine.HumanBone::m_BoneName
	String_t* ___m_BoneName_0;
	// System.String UnityEngine.HumanBone::m_HumanName
	String_t* ___m_HumanName_1;
	// UnityEngine.HumanLimit UnityEngine.HumanBone::limit
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke
{
	char* ___m_BoneName_0;
	char* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for COM marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com
{
	Il2CppChar* ___m_BoneName_0;
	Il2CppChar* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};

// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC  : public RuntimeObject
{
	// System.IntPtr UnityEngine.TextGenerator::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.String UnityEngine.TextGenerator::m_LastString
	String_t* ___m_LastString_1;
	// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::m_LastSettings
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 ___m_LastSettings_2;
	// System.Boolean UnityEngine.TextGenerator::m_HasGenerated
	bool ___m_HasGenerated_3;
	// UnityEngine.TextGenerationError UnityEngine.TextGenerator::m_LastValid
	int32_t ___m_LastValid_4;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::m_Verts
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	// System.Collections.Generic.List`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::m_Characters
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	// System.Collections.Generic.List`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::m_Lines
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	// System.Boolean UnityEngine.TextGenerator::m_CachedVerts
	bool ___m_CachedVerts_8;
	// System.Boolean UnityEngine.TextGenerator::m_CachedCharacters
	bool ___m_CachedCharacters_9;
	// System.Boolean UnityEngine.TextGenerator::m_CachedLines
	bool ___m_CachedLines_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	char* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
// Native definition for COM marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppChar* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};

// UnityEngine.UIElements.UIR.Utility
struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD  : public RuntimeObject
{
};

struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields
{
	// System.Action`1<System.Boolean> UnityEngine.UIElements.UIR.Utility::GraphicsResourcesRecreate
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___GraphicsResourcesRecreate_0;
	// System.Action UnityEngine.UIElements.UIR.Utility::EngineUpdate
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___EngineUpdate_1;
	// System.Action UnityEngine.UIElements.UIR.Utility::FlushPendingResources
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___FlushPendingResources_2;
	// System.Action`1<UnityEngine.Camera> UnityEngine.UIElements.UIR.Utility::RegisterIntermediateRenderers
	Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA* ___RegisterIntermediateRenderers_3;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeAdd
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeAdd_4;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeExecute
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeExecute_5;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeCleanup
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeCleanup_6;
	// Unity.Profiling.ProfilerMarker UnityEngine.UIElements.UIR.Utility::s_MarkerRaiseEngineUpdate
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___s_MarkerRaiseEngineUpdate_7;
};

// UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0 
{
	// UnityEngine.ParticleSystem/Particle UnityEngine.ParticleSystem/EmitParams::m_Particle
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_PositionSet
	bool ___m_PositionSet_1;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_VelocitySet
	bool ___m_VelocitySet_2;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AxisOfRotationSet
	bool ___m_AxisOfRotationSet_3;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RotationSet
	bool ___m_RotationSet_4;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AngularVelocitySet
	bool ___m_AngularVelocitySet_5;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartSizeSet
	bool ___m_StartSizeSet_6;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartColorSet
	bool ___m_StartColorSet_7;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RandomSeedSet
	bool ___m_RandomSeedSet_8;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartLifetimeSet
	bool ___m_StartLifetimeSet_9;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_MeshIndexSet
	bool ___m_MeshIndexSet_10;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_ApplyShapeToPosition
	bool ___m_ApplyShapeToPosition_11;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_pinvoke
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_com
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};

// UnityEngine.AnimatorOverrideController
struct AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493  : public RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254
{
	// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback UnityEngine.AnimatorOverrideController::OnOverrideControllerDirty
	OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5* ___OnOverrideControllerDirty_4;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;
};

// UnityEngine.GUIScrollGroup
struct GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5  : public GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D
{
	// System.Single UnityEngine.GUIScrollGroup::calcMinWidth
	float ___calcMinWidth_32;
	// System.Single UnityEngine.GUIScrollGroup::calcMaxWidth
	float ___calcMaxWidth_33;
	// System.Single UnityEngine.GUIScrollGroup::calcMinHeight
	float ___calcMinHeight_34;
	// System.Single UnityEngine.GUIScrollGroup::calcMaxHeight
	float ___calcMaxHeight_35;
	// System.Single UnityEngine.GUIScrollGroup::clientWidth
	float ___clientWidth_36;
	// System.Single UnityEngine.GUIScrollGroup::clientHeight
	float ___clientHeight_37;
	// System.Boolean UnityEngine.GUIScrollGroup::allowHorizontalScroll
	bool ___allowHorizontalScroll_38;
	// System.Boolean UnityEngine.GUIScrollGroup::allowVerticalScroll
	bool ___allowVerticalScroll_39;
	// System.Boolean UnityEngine.GUIScrollGroup::needsHorizontalScrollbar
	bool ___needsHorizontalScrollbar_40;
	// System.Boolean UnityEngine.GUIScrollGroup::needsVerticalScrollbar
	bool ___needsVerticalScrollbar_41;
	// UnityEngine.GUIStyle UnityEngine.GUIScrollGroup::horizontalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___horizontalScrollbar_42;
	// UnityEngine.GUIStyle UnityEngine.GUIScrollGroup::verticalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___verticalScrollbar_43;
};

// UnityEngine.GUISkin
struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_box_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_button_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_toggle_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_label_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_textField_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_textArea_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_window_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumbExtent
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSliderThumbExtent_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSlider_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSliderThumb_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumbExtent
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSliderThumbExtent_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_SliderMixed
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_SliderMixed_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbar_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarThumb_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarLeftButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarRightButton_22;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbar_23;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarThumb_24;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarUpButton_25;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarDownButton_26;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_ScrollView_27;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t1BA4BCF4D4D32DF07E9B84F1750D964DF33B0FEC* ___m_CustomStyles_28;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847* ___m_Settings_29;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_tEFC8016EC28460E6CE058A5F413FAB656883AA5F* ___m_Styles_31;
};

struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields
{
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___ms_Error_30;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98* ___m_SkinChanged_32;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9* ___current_33;
};

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields
{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::preWillRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___preWillRenderCanvases_4;
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___willRenderCanvases_5;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externBeginRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternBeginRenderOverlaysU3Ek__BackingField_6;
	// System.Action`2<System.Int32,System.Int32> UnityEngine.Canvas::<externRenderOverlaysBefore>k__BackingField
	Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8* ___U3CexternRenderOverlaysBeforeU3Ek__BackingField_7;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externEndRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternEndRenderOverlaysU3Ek__BackingField_8;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// PW.BasicCameraControl
struct BasicCameraControl_t06699E83504BCA9DF583E4912B8E523E9E42EC04  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single PW.BasicCameraControl::rotateSpeed
	float ___rotateSpeed_4;
	// System.Single PW.BasicCameraControl::scrollSmooth
	float ___scrollSmooth_5;
};

// PW.BasicGameEvents
struct BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject PW.BasicGameEvents::placeholderPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___placeholderPrefab_4;
};

struct BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A_StaticFields
{
	// PW.BasicGameEvents/OnOrderCancelled PW.BasicGameEvents::onOrderCancelled
	OnOrderCancelled_t2E913C055B0B9B69F1529DF076D33EF0ABA814C3* ___onOrderCancelled_5;
	// PW.BasicGameEvents/OnOrderCompleted PW.BasicGameEvents::onOrderCompleted
	OnOrderCompleted_t32F2E47CDFE18D499A2E2C5869C847C5D96FE854* ___onOrderCompleted_6;
	// PW.BasicGameEvents/OnProductAddedToSlot PW.BasicGameEvents::onProductAddedToSlot
	OnProductAddedToSlot_t6E89D675BEB0512A325DA0C8E7F4707F9A50C381* ___onProductAddedToSlot_7;
	// PW.BasicGameEvents/OnProductDeletedFromSlot PW.BasicGameEvents::onProductDeletedFromSlot
	OnProductDeletedFromSlot_t62F10DC6573C41AAF6971911C3091126E76E7F1A* ___onProductDeletedFromSlot_8;
	// PW.BasicGameEvents/OnPlaceHolderRequired PW.BasicGameEvents::onPlaceHolderRequired
	OnPlaceHolderRequired_tF15B3DFBCD3CD6F1267B73BA3681F8E15D857493* ___onPlaceHolderRequired_9;
};

// PW.BewerageMaker
struct BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean PW.BewerageMaker::useAnimation
	bool ___useAnimation_4;
	// System.Boolean PW.BewerageMaker::useTweeningAnimation
	bool ___useTweeningAnimation_5;
	// System.String PW.BewerageMaker::preFillAnimationStateName
	String_t* ___preFillAnimationStateName_6;
	// System.Single PW.BewerageMaker::preFillProcess
	float ___preFillProcess_7;
	// System.String PW.BewerageMaker::fillEndedAnimationState
	String_t* ___fillEndedAnimationState_8;
	// System.Single PW.BewerageMaker::fillingProcess
	float ___fillingProcess_9;
	// UnityEngine.Transform PW.BewerageMaker::dummyAnimationTarget
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___dummyAnimationTarget_10;
	// UnityEngine.ParticleSystem PW.BewerageMaker::fillParticle
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___fillParticle_11;
	// UnityEngine.Transform PW.BewerageMaker::finalTweenTarget
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___finalTweenTarget_12;
	// UnityEngine.GameObject PW.BewerageMaker::cupType
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cupType_13;
	// UnityEngine.GameObject PW.BewerageMaker::progressHelperprefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___progressHelperprefab_14;
	// UnityEngine.Transform PW.BewerageMaker::fillCupSpot
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___fillCupSpot_15;
	// System.Single PW.BewerageMaker::totalProcess
	float ___totalProcess_16;
	// System.Boolean PW.BewerageMaker::canFillCup
	bool ___canFillCup_17;
	// PW.FillCupHelper PW.BewerageMaker::fillCupHelper
	FillCupHelper_tDEB2B35BCDCA764B728281A92DB81A5099E80A11* ___fillCupHelper_18;
	// PW.ProgressHelper PW.BewerageMaker::m_progressHelper
	ProgressHelper_t99435CC1BFDDD2BF74478C6E61ECB6827C080469* ___m_progressHelper_19;
	// UnityEngine.Collider PW.BewerageMaker::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_20;
	// UnityEngine.Animator PW.BewerageMaker::m_animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ___m_animator_21;
};

// PW.CookingGameObject
struct CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Vector3 PW.CookingGameObject::cookingSpot
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___cookingSpot_4;
	// UnityEngine.Vector3 PW.CookingGameObject::startingPositionOffset
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___startingPositionOffset_5;
	// PW.CookableProduct PW.CookingGameObject::currentProduct
	CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9* ___currentProduct_6;
	// UnityEngine.GameObject PW.CookingGameObject::progressHelperprefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___progressHelperprefab_7;
	// PW.ProgressHelper PW.CookingGameObject::m_progressHelper
	ProgressHelper_t99435CC1BFDDD2BF74478C6E61ECB6827C080469* ___m_progressHelper_8;
	// System.Single PW.CookingGameObject::doorAnimTime
	float ___doorAnimTime_9;
	// System.Single PW.CookingGameObject::cookingProcess
	float ___cookingProcess_10;
	// UnityEngine.Collider PW.CookingGameObject::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_11;
};

// PW.CreateProductOnPlaceHolder
struct CreateProductOnPlaceHolder_t2C8543024E30296891A24622E8130D2E64E031FA  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject PW.CreateProductOnPlaceHolder::objectToGenerate
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___objectToGenerate_4;
};

// PW.DeleteProductOnSlot
struct DeleteProductOnSlot_tF03FFE404683952BBFC266531E23211937324AC8  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean PW.DeleteProductOnSlot::shownDeleteButton
	bool ___shownDeleteButton_4;
	// UnityEngine.UI.Button PW.DeleteProductOnSlot::deleteButton
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___deleteButton_5;
	// System.Int32 PW.DeleteProductOnSlot::SlotIndex
	int32_t ___SlotIndex_6;
};

// PW.FillCupHelper
struct FillCupHelper_tDEB2B35BCDCA764B728281A92DB81A5099E80A11  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Collider PW.FillCupHelper::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_4;
	// PW.BewerageMaker PW.FillCupHelper::m_Machine
	BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3* ___m_Machine_5;
	// UnityEngine.Transform PW.FillCupHelper::fluid
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___fluid_6;
};

// PW.Microwave
struct Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// PW.HeatableProduct PW.Microwave::currentProduct
	HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824* ___currentProduct_4;
	// System.Boolean PW.Microwave::doorIsOpen
	bool ___doorIsOpen_5;
	// UnityEngine.Transform PW.Microwave::door
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___door_6;
	// UnityEngine.Transform PW.Microwave::beginEnteringSpot
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___beginEnteringSpot_7;
	// UnityEngine.Transform PW.Microwave::cookingSpot
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___cookingSpot_8;
	// UnityEngine.GameObject PW.Microwave::progressHelperprefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___progressHelperprefab_9;
	// PW.ProgressHelper PW.Microwave::m_progressHelper
	ProgressHelper_t99435CC1BFDDD2BF74478C6E61ECB6827C080469* ___m_progressHelper_10;
	// System.Single PW.Microwave::heatingProcess
	float ___heatingProcess_11;
};

// MouseCamLook
struct MouseCamLook_t605CA6054A71179E2422B8A39CFB63E769ECCACC  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single MouseCamLook::mouseSensitivity
	float ___mouseSensitivity_4;
	// UnityEngine.Transform MouseCamLook::playerBody
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___playerBody_5;
	// System.Single MouseCamLook::xRotation
	float ___xRotation_6;
};

// PW.OnClickCoverHelper
struct OnClickCoverHelper_t7D6207E3DD137F57AA4B292A98FA92F6402B56B3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityEvent PW.OnClickCoverHelper::methodToCall
	UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* ___methodToCall_4;
	// UnityEngine.Collider PW.OnClickCoverHelper::m_collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_collider_5;
};

// PW.OrderGenerator
struct OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 PW.OrderGenerator::MaxConcurrentOrder
	int32_t ___MaxConcurrentOrder_4;
	// System.Int32 PW.OrderGenerator::currentOrderCount
	int32_t ___currentOrderCount_5;
	// UnityEngine.Sprite[] PW.OrderGenerator::orderSprites
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___orderSprites_6;
	// System.Int32[] PW.OrderGenerator::orderedProducts
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___orderedProducts_7;
	// UnityEngine.Transform PW.OrderGenerator::UIParentForOrders
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___UIParentForOrders_8;
	// UnityEngine.GameObject PW.OrderGenerator::orderRepPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___orderRepPrefab_9;
};

// PlayerMovement
struct PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.CharacterController PlayerMovement::controller
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___controller_4;
	// System.Single PlayerMovement::baseSpeed
	float ___baseSpeed_5;
	// System.Single PlayerMovement::gravity
	float ___gravity_6;
	// System.Single PlayerMovement::jumpHeight
	float ___jumpHeight_7;
	// System.Single PlayerMovement::sprintSpeed
	float ___sprintSpeed_8;
	// System.Single PlayerMovement::speedBoost
	float ___speedBoost_9;
	// UnityEngine.Vector3 PlayerMovement::velocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___velocity_10;
};

// PW.PlayerSlots
struct PlayerSlots_t9ACB68E0686BD97247ABB9ABAFD440D739F69508  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 PW.PlayerSlots::slotCount
	int32_t ___slotCount_4;
	// System.Int32[] PW.PlayerSlots::slotItems
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___slotItems_5;
	// UnityEngine.UI.Image[] PW.PlayerSlots::slotUIObjects
	ImageU5BU5D_t8869694C217655DA7B1315DC02C80F1308B78B78* ___slotUIObjects_6;
};

// PW.ProductGameObject
struct ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject PW.ProductGameObject::serveAsDifferentGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___serveAsDifferentGameObject_5;
	// System.Int32 PW.ProductGameObject::orderID
	int32_t ___orderID_6;
	// System.Boolean PW.ProductGameObject::AddToPlateBeforeServed
	bool ___AddToPlateBeforeServed_7;
	// UnityEngine.Vector3 PW.ProductGameObject::plateOffset
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___plateOffset_8;
	// System.Boolean PW.ProductGameObject::RegenerateProduct
	bool ___RegenerateProduct_9;
};

// PW.ProductManager
struct ProductManager_tAD80313E4C8F277881B27D63991321241418FBCE  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// PW.Product[] PW.ProductManager::Products
	ProductU5BU5D_t43FCCECDF03ACE141268985EA3F7358784CEE920* ___Products_4;
	// System.String PW.ProductManager::ProductScreenshotsPath
	String_t* ___ProductScreenshotsPath_5;
	// UnityEngine.GameObject PW.ProductManager::placeholderPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___placeholderPrefab_6;
};

// PW.ProductWithCover
struct ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Collider PW.ProductWithCover::m_collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_collider_4;
	// UnityEngine.Transform PW.ProductWithCover::coverObject
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___coverObject_5;
	// UnityEngine.Vector3 PW.ProductWithCover::openCoverOffset
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___openCoverOffset_6;
	// System.Boolean PW.ProductWithCover::autoCloseCover
	bool ___autoCloseCover_7;
	// System.Boolean PW.ProductWithCover::IsAnimating
	bool ___IsAnimating_9;
};

// PW.ProgressHelper
struct ProgressHelper_t99435CC1BFDDD2BF74478C6E61ECB6827C080469  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.UI.Image PW.ProgressHelper::m_Image
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_Image_4;
};

// ScreenShotHelper
struct ScreenShotHelper_t4E7FF182DF16556BF7A5015DA285CC99D242E3B2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Texture2D ScreenShotHelper::lastScreenshot
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___lastScreenshot_4;
	// UnityEngine.GameObject ScreenShotHelper::currentGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___currentGameObject_5;
	// UnityEngine.UI.RawImage ScreenShotHelper::testImage
	RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179* ___testImage_6;
	// System.Boolean ScreenShotHelper::isMultipleObject
	bool ___isMultipleObject_7;
	// System.String ScreenShotHelper::ScreenShotPath
	String_t* ___ScreenShotPath_8;
};

// PW.ServeOrder
struct ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 PW.ServeOrder::order_count
	int32_t ___order_count_4;
	// System.Int32 PW.ServeOrder::total_order_served
	int32_t ___total_order_served_5;
	// System.Single PW.ServeOrder::totalServingTime
	float ___totalServingTime_6;
	// System.Single PW.ServeOrder::curServeTime
	float ___curServeTime_7;
	// UnityEngine.UI.Image PW.ServeOrder::m_Image
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_Image_8;
	// System.Int32 PW.ServeOrder::orderID
	int32_t ___orderID_9;
	// UnityEngine.UI.Image PW.ServeOrder::serveTimeRepresentation
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___serveTimeRepresentation_10;
};

// UnityTemplateProject.SimpleCameraController
struct SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityTemplateProject.SimpleCameraController/CameraState UnityTemplateProject.SimpleCameraController::m_TargetCameraState
	CameraState_t20CBEE55211900851700899BE504EE3E90BB3080* ___m_TargetCameraState_4;
	// UnityTemplateProject.SimpleCameraController/CameraState UnityTemplateProject.SimpleCameraController::m_InterpolatingCameraState
	CameraState_t20CBEE55211900851700899BE504EE3E90BB3080* ___m_InterpolatingCameraState_5;
	// System.Single UnityTemplateProject.SimpleCameraController::boost
	float ___boost_6;
	// System.Single UnityTemplateProject.SimpleCameraController::positionLerpTime
	float ___positionLerpTime_7;
	// UnityEngine.AnimationCurve UnityTemplateProject.SimpleCameraController::mouseSensitivityCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___mouseSensitivityCurve_8;
	// System.Single UnityTemplateProject.SimpleCameraController::rotationLerpTime
	float ___rotationLerpTime_9;
	// System.Boolean UnityTemplateProject.SimpleCameraController::invertY
	bool ___invertY_10;
};

// PW.SliceHelper
struct SliceHelper_t695F49C4F4DB1DF0317D652676704FFEC121C05B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject PW.SliceHelper::slicePrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___slicePrefab_4;
	// System.Int32 PW.SliceHelper::sliceCount
	int32_t ___sliceCount_5;
};

// PW.CookableProduct
struct CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9  : public ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011
{
	// UnityEngine.Collider PW.CookableProduct::m_collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_collider_10;
	// UnityEngine.Vector3 PW.CookableProduct::initialPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___initialPosition_11;
	// PW.CookingGameObject PW.CookableProduct::cookingObject
	CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E* ___cookingObject_12;
	// UnityEngine.GameObject PW.CookableProduct::platePrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___platePrefab_13;
	// System.Single PW.CookableProduct::cookingTimeForProduct
	float ___cookingTimeForProduct_14;
	// System.Boolean PW.CookableProduct::IsCooked
	bool ___IsCooked_15;
};

// PW.HeatableProduct
struct HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824  : public ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011
{
	// UnityEngine.Collider PW.HeatableProduct::m_collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_collider_10;
	// UnityEngine.Vector3 PW.HeatableProduct::initialPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___initialPosition_11;
	// PW.Microwave PW.HeatableProduct::m_Machine
	Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE* ___m_Machine_12;
	// UnityEngine.GameObject PW.HeatableProduct::platePrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___platePrefab_13;
	// System.Single PW.HeatableProduct::heatingTimeForProduct
	float ___heatingTimeForProduct_14;
};

// PW.ReadyToServe
struct ReadyToServe_t471256A83D0E86C0CC1DEEC3AFBDE5FCFBE93867  : public ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011
{
	// UnityEngine.GameObject PW.ReadyToServe::platePrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___platePrefab_10;
	// UnityEngine.Collider PW.ReadyToServe::m_collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_collider_11;
};

// PW.StoveGameObject
struct StoveGameObject_t60DB9BA20178D8177199C2B8CCECB35E520DCDA1  : public CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E
{
	// UnityEngine.Transform PW.StoveGameObject::doorTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___doorTransform_12;
	// UnityEngine.Vector3 PW.StoveGameObject::progressHelperOffset
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___progressHelperOffset_13;
	// System.Boolean PW.StoveGameObject::doorIsOpen
	bool ___doorIsOpen_14;
	// System.Boolean PW.StoveGameObject::isAnimating
	bool ___isAnimating_15;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3000[4] = 
{
	static_cast<int32_t>(offsetof(LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60, ___U3CidU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60, ___topLevel_1)),static_cast<int32_t>(offsetof(LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60, ___layoutGroups_2)),static_cast<int32_t>(offsetof(LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60, ___windows_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3001[4] = 
{
	static_cast<int32_t>(offsetof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields, ___s_StoredLayouts_0)),static_cast<int32_t>(offsetof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields, ___s_StoredWindows_1)),static_cast<int32_t>(offsetof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields, ___current_2)),static_cast<int32_t>(offsetof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields, ___kDummyRect_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3002[5] = 
{
	static_cast<int32_t>(offsetof(GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847, ___m_DoubleClickSelectsWord_0)),static_cast<int32_t>(offsetof(GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847, ___m_TripleClickSelectsLine_1)),static_cast<int32_t>(offsetof(GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847, ___m_CursorColor_2)),static_cast<int32_t>(offsetof(GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847, ___m_CursorFlashSpeed_3)),static_cast<int32_t>(offsetof(GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847, ___m_SelectionColor_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3004[30] = 
{
	static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_Font_4)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_box_5)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_button_6)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_toggle_7)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_label_8)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_textField_9)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_textArea_10)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_window_11)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_horizontalSlider_12)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_horizontalSliderThumb_13)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_horizontalSliderThumbExtent_14)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_verticalSlider_15)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_verticalSliderThumb_16)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_verticalSliderThumbExtent_17)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_SliderMixed_18)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_horizontalScrollbar_19)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_horizontalScrollbarThumb_20)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_horizontalScrollbarLeftButton_21)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_horizontalScrollbarRightButton_22)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_verticalScrollbar_23)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_verticalScrollbarThumb_24)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_verticalScrollbarUpButton_25)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_verticalScrollbarDownButton_26)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_ScrollView_27)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_CustomStyles_28)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_Settings_29)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields, ___ms_Error_30)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9, ___m_Styles_31)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields, ___m_SkinChanged_32)),static_cast<int32_t>(offsetof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields, ___current_33)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3005[2] = 
{
	static_cast<int32_t>(offsetof(GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95, ___m_Ptr_0)),static_cast<int32_t>(offsetof(GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95, ___m_SourceStyle_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3006[16] = 
{
	static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Ptr_0)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Normal_1)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Hover_2)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Active_3)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Focused_4)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_OnNormal_5)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_OnHover_6)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_OnActive_7)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_OnFocused_8)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Border_9)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Padding_10)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Margin_11)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Overflow_12)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580, ___m_Name_13)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_StaticFields, ___showKeyboardFocus_14)),static_cast<int32_t>(offsetof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_StaticFields, ___s_None_15)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3007[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3008[1] = 
{
	static_cast<int32_t>(offsetof(GUITargetAttribute_t3F08CE7E00D79CB555B94AA7FA1BCB4B144B922B, ___displayMask_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3009[11] = 
{
	static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___s_ControlCount_0)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___s_SkinMode_1)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___s_OriginalID_2)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___takeCapture_3)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___releaseCapture_4)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___processEvent_5)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___cleanupRoots_6)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___endContainerGUIFromException_7)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___guiChanged_8)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___U3CguiIsExitingU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields, ___s_HasCurrentWindowKeyFocusFunc_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3011[11] = 
{
	static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___minWidth_0)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___maxWidth_1)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___minHeight_2)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___maxHeight_3)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___rect_4)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___stretchWidth_5)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___stretchHeight_6)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___consideredForMargin_7)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F, ___m_Style_8)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F_StaticFields, ___kDummyRect_9)),static_cast<int32_t>(offsetof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F_StaticFields, ___indent_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3012[21] = 
{
	static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___entries_11)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___isVertical_12)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___resetCoords_13)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___spacing_14)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___sameSize_15)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___isWindow_16)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___windowID_17)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_Cursor_18)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_StretchableCountX_19)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_StretchableCountY_20)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_UserSpecifiedWidth_21)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_UserSpecifiedHeight_22)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_ChildMinWidth_23)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_ChildMaxWidth_24)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_ChildMinHeight_25)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_ChildMaxHeight_26)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_MarginLeft_27)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_MarginRight_28)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_MarginTop_29)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D, ___m_MarginBottom_30)),static_cast<int32_t>(offsetof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D_StaticFields, ___none_31)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3013[12] = 
{
	static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___calcMinWidth_32)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___calcMaxWidth_33)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___calcMinHeight_34)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___calcMaxHeight_35)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___clientWidth_36)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___clientHeight_37)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___allowHorizontalScroll_38)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___allowVerticalScroll_39)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___needsHorizontalScrollbar_40)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___needsVerticalScrollbar_41)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___horizontalScrollbar_42)),static_cast<int32_t>(offsetof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5, ___verticalScrollbar_43)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3014[1] = 
{
	static_cast<int32_t>(offsetof(ObjectGUIState_t7BE88DC8B9C7187A77D63BBCBE9DB7B674863C15, ___m_Ptr_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3017[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3018[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3019[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3020[51] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3021[24] = 
{
	static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___keyboardOnScreen_0)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___controlID_1)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___style_2)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___multiline_3)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___hasHorizontalCursorPos_4)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___isPasswordField_5)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_HasFocus_6)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___scrollOffset_7)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_Content_8)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_Position_9)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_CursorIndex_10)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_SelectIndex_11)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_RevealCursor_12)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___graphicalCursorPos_13)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___graphicalSelectCursorPos_14)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_MouseDragSelectsWholeWords_15)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_DblClickInitPos_16)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_DblClickSnap_17)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_bJustSelected_18)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___m_iAltCursorPos_19)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___oldText_20)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___oldPos_21)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27, ___oldSelectPos_22)),static_cast<int32_t>(offsetof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27_StaticFields, ___s_Keyactions_23)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3025[3] = 
{
	static_cast<int32_t>(offsetof(MouseCamLook_t605CA6054A71179E2422B8A39CFB63E769ECCACC, ___mouseSensitivity_4)),static_cast<int32_t>(offsetof(MouseCamLook_t605CA6054A71179E2422B8A39CFB63E769ECCACC, ___playerBody_5)),static_cast<int32_t>(offsetof(MouseCamLook_t605CA6054A71179E2422B8A39CFB63E769ECCACC, ___xRotation_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3026[7] = 
{
	static_cast<int32_t>(offsetof(PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351, ___controller_4)),static_cast<int32_t>(offsetof(PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351, ___baseSpeed_5)),static_cast<int32_t>(offsetof(PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351, ___gravity_6)),static_cast<int32_t>(offsetof(PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351, ___jumpHeight_7)),static_cast<int32_t>(offsetof(PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351, ___sprintSpeed_8)),static_cast<int32_t>(offsetof(PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351, ___speedBoost_9)),static_cast<int32_t>(offsetof(PlayerMovement_t4A0A0A8C937BC1D4FC570D1B5B50847338423351, ___velocity_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3027[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3028[5] = 
{
	static_cast<int32_t>(offsetof(ScreenShotHelper_t4E7FF182DF16556BF7A5015DA285CC99D242E3B2, ___lastScreenshot_4)),static_cast<int32_t>(offsetof(ScreenShotHelper_t4E7FF182DF16556BF7A5015DA285CC99D242E3B2, ___currentGameObject_5)),static_cast<int32_t>(offsetof(ScreenShotHelper_t4E7FF182DF16556BF7A5015DA285CC99D242E3B2, ___testImage_6)),static_cast<int32_t>(offsetof(ScreenShotHelper_t4E7FF182DF16556BF7A5015DA285CC99D242E3B2, ___isMultipleObject_7)),static_cast<int32_t>(offsetof(ScreenShotHelper_t4E7FF182DF16556BF7A5015DA285CC99D242E3B2, ___ScreenShotPath_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3030[6] = 
{
	static_cast<int32_t>(offsetof(CameraState_t20CBEE55211900851700899BE504EE3E90BB3080, ___yaw_0)),static_cast<int32_t>(offsetof(CameraState_t20CBEE55211900851700899BE504EE3E90BB3080, ___pitch_1)),static_cast<int32_t>(offsetof(CameraState_t20CBEE55211900851700899BE504EE3E90BB3080, ___roll_2)),static_cast<int32_t>(offsetof(CameraState_t20CBEE55211900851700899BE504EE3E90BB3080, ___x_3)),static_cast<int32_t>(offsetof(CameraState_t20CBEE55211900851700899BE504EE3E90BB3080, ___y_4)),static_cast<int32_t>(offsetof(CameraState_t20CBEE55211900851700899BE504EE3E90BB3080, ___z_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3031[7] = 
{
	static_cast<int32_t>(offsetof(SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E, ___m_TargetCameraState_4)),static_cast<int32_t>(offsetof(SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E, ___m_InterpolatingCameraState_5)),static_cast<int32_t>(offsetof(SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E, ___boost_6)),static_cast<int32_t>(offsetof(SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E, ___positionLerpTime_7)),static_cast<int32_t>(offsetof(SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E, ___mouseSensitivityCurve_8)),static_cast<int32_t>(offsetof(SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E, ___rotationLerpTime_9)),static_cast<int32_t>(offsetof(SimpleCameraController_t0F2741F52435CF703EC9FFC2C30CF82A61316A0E, ___invertY_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3032[8] = 
{
	static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___target_3)),static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___finalTweenValue_4)),static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___U3CcurPreFillU3E5__2_5)),static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___U3CtotalDistU3E5__3_6)),static_cast<int32_t>(offsetof(U3CDoPreFillU3Ed__22_t823B1EF2C8049F7077A2920BCA9BFC753CEE7ECD, ___U3CtotalRotU3E5__4_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3033[4] = 
{
	static_cast<int32_t>(offsetof(U3CDoFillAnimationU3Ed__23_t02E2CD905F550B46D9ED72540B2147CF5DF661B6, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CDoFillAnimationU3Ed__23_t02E2CD905F550B46D9ED72540B2147CF5DF661B6, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CDoFillAnimationU3Ed__23_t02E2CD905F550B46D9ED72540B2147CF5DF661B6, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CDoFillAnimationU3Ed__23_t02E2CD905F550B46D9ED72540B2147CF5DF661B6, ___U3CfillCurrentU3E5__2_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3034[6] = 
{
	static_cast<int32_t>(offsetof(U3CDoFillEndedU3Ed__27_t76C751A3503DE1DF03BC6B529BC907B6C70ACDED, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CDoFillEndedU3Ed__27_t76C751A3503DE1DF03BC6B529BC907B6C70ACDED, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CDoFillEndedU3Ed__27_t76C751A3503DE1DF03BC6B529BC907B6C70ACDED, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CDoFillEndedU3Ed__27_t76C751A3503DE1DF03BC6B529BC907B6C70ACDED, ___U3CtotalDistU3E5__2_3)),static_cast<int32_t>(offsetof(U3CDoFillEndedU3Ed__27_t76C751A3503DE1DF03BC6B529BC907B6C70ACDED, ___U3CtotalRotU3E5__3_4)),static_cast<int32_t>(offsetof(U3CDoFillEndedU3Ed__27_t76C751A3503DE1DF03BC6B529BC907B6C70ACDED, ___U3CreverseMoveU3E5__4_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3035[18] = 
{
	static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___useAnimation_4)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___useTweeningAnimation_5)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___preFillAnimationStateName_6)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___preFillProcess_7)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___fillEndedAnimationState_8)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___fillingProcess_9)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___dummyAnimationTarget_10)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___fillParticle_11)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___finalTweenTarget_12)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___cupType_13)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___progressHelperprefab_14)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___fillCupSpot_15)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___totalProcess_16)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___canFillCup_17)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___fillCupHelper_18)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___m_progressHelper_19)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___m_Collider_20)),static_cast<int32_t>(offsetof(BewerageMaker_t08DD651ECE6CF52CD5C4FE326C7CFA67F93EF7E3, ___m_animator_21)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3036[4] = 
{
	static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__10_t24377E81B7CA3046B4C129EC19CBB75DD979081E, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__10_t24377E81B7CA3046B4C129EC19CBB75DD979081E, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__10_t24377E81B7CA3046B4C129EC19CBB75DD979081E, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__10_t24377E81B7CA3046B4C129EC19CBB75DD979081E, ___targetPos_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3037[3] = 
{
	static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__13_t9E32B5A36F314F1B0A646B04070F1BBECD786954, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__13_t9E32B5A36F314F1B0A646B04070F1BBECD786954, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__13_t9E32B5A36F314F1B0A646B04070F1BBECD786954, ___U3CU3E4__this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3038[6] = 
{
	static_cast<int32_t>(offsetof(CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9, ___m_collider_10)),static_cast<int32_t>(offsetof(CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9, ___initialPosition_11)),static_cast<int32_t>(offsetof(CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9, ___cookingObject_12)),static_cast<int32_t>(offsetof(CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9, ___platePrefab_13)),static_cast<int32_t>(offsetof(CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9, ___cookingTimeForProduct_14)),static_cast<int32_t>(offsetof(CookableProduct_t27DCD72813FD75D317E2D0CDA20C8C88DB0A2AC9, ___IsCooked_15)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3039[4] = 
{
	static_cast<int32_t>(offsetof(U3CCookingU3Ed__16_t477A3C0B021BBC3BF44AA61EB4F3E43F39446EC4, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CCookingU3Ed__16_t477A3C0B021BBC3BF44AA61EB4F3E43F39446EC4, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CCookingU3Ed__16_t477A3C0B021BBC3BF44AA61EB4F3E43F39446EC4, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CCookingU3Ed__16_t477A3C0B021BBC3BF44AA61EB4F3E43F39446EC4, ___U3CcurTimeU3E5__2_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3040[8] = 
{
	static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___cookingSpot_4)),static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___startingPositionOffset_5)),static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___currentProduct_6)),static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___progressHelperprefab_7)),static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___m_progressHelper_8)),static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___doorAnimTime_9)),static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___cookingProcess_10)),static_cast<int32_t>(offsetof(CookingGameObject_tC7CE5AEA79715239D6CD2AA6015BE5ECC0DFE96E, ___m_Collider_11)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3041[1] = 
{
	static_cast<int32_t>(offsetof(CreateProductOnPlaceHolder_t2C8543024E30296891A24622E8130D2E64E031FA, ___objectToGenerate_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3042[3] = 
{
	static_cast<int32_t>(offsetof(DeleteProductOnSlot_tF03FFE404683952BBFC266531E23211937324AC8, ___shownDeleteButton_4)),static_cast<int32_t>(offsetof(DeleteProductOnSlot_tF03FFE404683952BBFC266531E23211937324AC8, ___deleteButton_5)),static_cast<int32_t>(offsetof(DeleteProductOnSlot_tF03FFE404683952BBFC266531E23211937324AC8, ___SlotIndex_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3043[3] = 
{
	static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__0_t800C169F4245838F789E277FD2416F196B28297F, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__0_t800C169F4245838F789E277FD2416F196B28297F, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__0_t800C169F4245838F789E277FD2416F196B28297F, ___U3CU3E4__this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3045[7] = 
{
	static_cast<int32_t>(offsetof(U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1, ___amount_3)),static_cast<int32_t>(offsetof(U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1, ___U3CtimeAmountU3E5__2_4)),static_cast<int32_t>(offsetof(U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1, ___U3CtotalDistU3E5__3_5)),static_cast<int32_t>(offsetof(U3CFillAnimationU3Ed__6_tD77B095C0ED46F18563A74389FD51E0DD8901CF1, ___U3CtotalScaleU3E5__4_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3046[3] = 
{
	static_cast<int32_t>(offsetof(FillCupHelper_tDEB2B35BCDCA764B728281A92DB81A5099E80A11, ___m_Collider_4)),static_cast<int32_t>(offsetof(FillCupHelper_tDEB2B35BCDCA764B728281A92DB81A5099E80A11, ___m_Machine_5)),static_cast<int32_t>(offsetof(FillCupHelper_tDEB2B35BCDCA764B728281A92DB81A5099E80A11, ___fluid_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3047[3] = 
{
	static_cast<int32_t>(offsetof(U3CMoveToMicrowaveU3Ed__9_t901B8B465F5D06C4FD70452B831F62AB40A78083, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CMoveToMicrowaveU3Ed__9_t901B8B465F5D06C4FD70452B831F62AB40A78083, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CMoveToMicrowaveU3Ed__9_t901B8B465F5D06C4FD70452B831F62AB40A78083, ___U3CU3E4__this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3048[3] = 
{
	static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__10_t6D7FB9E4228E747E03FDD0B84DAB5B67CAECD38A, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__10_t6D7FB9E4228E747E03FDD0B84DAB5B67CAECD38A, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__10_t6D7FB9E4228E747E03FDD0B84DAB5B67CAECD38A, ___U3CU3E4__this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3049[5] = 
{
	static_cast<int32_t>(offsetof(HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824, ___m_collider_10)),static_cast<int32_t>(offsetof(HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824, ___initialPosition_11)),static_cast<int32_t>(offsetof(HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824, ___m_Machine_12)),static_cast<int32_t>(offsetof(HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824, ___platePrefab_13)),static_cast<int32_t>(offsetof(HeatableProduct_t7C49458BAA80DA6A8A56C069818180D2E072B824, ___heatingTimeForProduct_14)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3050[3] = 
{
	static_cast<int32_t>(offsetof(U3COpenMicrowaveAndHeatProductU3Ed__12_tB118EFE51A52BAEAE9756C970D63F5F31A3AA323, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3COpenMicrowaveAndHeatProductU3Ed__12_tB118EFE51A52BAEAE9756C970D63F5F31A3AA323, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3COpenMicrowaveAndHeatProductU3Ed__12_tB118EFE51A52BAEAE9756C970D63F5F31A3AA323, ___U3CU3E4__this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3051[10] = 
{
	static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___open_3)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___alsoReverse_4)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CtotalTimeU3E5__2_5)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CcurTimeU3E5__3_6)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CtotalAngleU3E5__4_7)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CmultiplierU3E5__5_8)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__13_t638F750C069F4D480695639E0117CDF4AC62055B, ___U3CfinalAngleU3E5__6_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3052[4] = 
{
	static_cast<int32_t>(offsetof(U3CHeatingU3Ed__14_t15955D9AD16B142EFE4EC01CC97A570FC472F7F2, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CHeatingU3Ed__14_t15955D9AD16B142EFE4EC01CC97A570FC472F7F2, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CHeatingU3Ed__14_t15955D9AD16B142EFE4EC01CC97A570FC472F7F2, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CHeatingU3Ed__14_t15955D9AD16B142EFE4EC01CC97A570FC472F7F2, ___U3CcurProcessU3E5__2_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3053[8] = 
{
	static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___currentProduct_4)),static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___doorIsOpen_5)),static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___door_6)),static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___beginEnteringSpot_7)),static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___cookingSpot_8)),static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___progressHelperprefab_9)),static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___m_progressHelper_10)),static_cast<int32_t>(offsetof(Microwave_t84A2E415DE369875A2CAE0EB0DDE15A6EA8DA8EE, ___heatingProcess_11)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3054[2] = 
{
	static_cast<int32_t>(offsetof(OnClickCoverHelper_t7D6207E3DD137F57AA4B292A98FA92F6402B56B3, ___methodToCall_4)),static_cast<int32_t>(offsetof(OnClickCoverHelper_t7D6207E3DD137F57AA4B292A98FA92F6402B56B3, ___m_collider_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3055[4] = 
{
	static_cast<int32_t>(offsetof(U3CGenerateOrderRoutineU3Ed__11_t6D86BC9D0B281E30D54EE586F033C7F87E67961C, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CGenerateOrderRoutineU3Ed__11_t6D86BC9D0B281E30D54EE586F033C7F87E67961C, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CGenerateOrderRoutineU3Ed__11_t6D86BC9D0B281E30D54EE586F033C7F87E67961C, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CGenerateOrderRoutineU3Ed__11_t6D86BC9D0B281E30D54EE586F033C7F87E67961C, ___intervalTime_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3056[6] = 
{
	static_cast<int32_t>(offsetof(OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555, ___MaxConcurrentOrder_4)),static_cast<int32_t>(offsetof(OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555, ___currentOrderCount_5)),static_cast<int32_t>(offsetof(OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555, ___orderSprites_6)),static_cast<int32_t>(offsetof(OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555, ___orderedProducts_7)),static_cast<int32_t>(offsetof(OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555, ___UIParentForOrders_8)),static_cast<int32_t>(offsetof(OrderGenerator_t64DD8B1F9251160E7DF1FB97B3BC016E29230555, ___orderRepPrefab_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3058[10] = 
{
	static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___index_3)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CuiImageU3E5__2_4)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CoutlineU3E5__3_5)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CoutlineColorU3E5__4_6)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CtotalTimeU3E5__5_7)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CcurTimeU3E5__6_8)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__6_t27FEA5B6F721AE01BA51C07300BD61916F48C4E7, ___U3CalphaValU3E5__7_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3059[3] = 
{
	static_cast<int32_t>(offsetof(PlayerSlots_t9ACB68E0686BD97247ABB9ABAFD440D739F69508, ___slotCount_4)),static_cast<int32_t>(offsetof(PlayerSlots_t9ACB68E0686BD97247ABB9ABAFD440D739F69508, ___slotItems_5)),static_cast<int32_t>(offsetof(PlayerSlots_t9ACB68E0686BD97247ABB9ABAFD440D739F69508, ___slotUIObjects_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3060[10] = 
{
	static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___productPrefab_0)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___orderID_1)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___productName_2)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___productPrice_3)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___productType_4)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___addToPlateBeforeServed_5)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___dontIncludeInThisScene_6)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___servedAsDifferentGameObject_7)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___plateOffset_8)),static_cast<int32_t>(offsetof(Product_t65A0D3000C2F88EB781E9B24C54F64EFF16117D4, ___RegenerateProduct_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3061[5] = 
{
	static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__6_tC49557D1D118EDB1760047C61A332214F555E525, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__6_tC49557D1D118EDB1760047C61A332214F555E525, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__6_tC49557D1D118EDB1760047C61A332214F555E525, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__6_tC49557D1D118EDB1760047C61A332214F555E525, ___U3CcurTimeU3E5__2_3)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__6_tC49557D1D118EDB1760047C61A332214F555E525, ___U3CtotalDistU3E5__3_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3062[7] = 
{
	static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF, ___targetPos_2)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF, ___U3CU3E4__this_3)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF, ___U3CtotalTimeU3E5__2_4)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF, ___U3CcurTimeU3E5__3_5)),static_cast<int32_t>(offsetof(U3CMoveToPlaceU3Ed__8_t53CCEFDC1B46DC62F68029F5E53FCEDFC5CA33BF, ___U3CtotalDistU3E5__4_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3063[6] = 
{
	0,static_cast<int32_t>(offsetof(ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011, ___serveAsDifferentGameObject_5)),static_cast<int32_t>(offsetof(ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011, ___orderID_6)),static_cast<int32_t>(offsetof(ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011, ___AddToPlateBeforeServed_7)),static_cast<int32_t>(offsetof(ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011, ___plateOffset_8)),static_cast<int32_t>(offsetof(ProductGameObject_tE2BC4CB39651AF47760A2CDC668A28BDB9A76011, ___RegenerateProduct_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3064[1] = 
{
	static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2CBA28A751DF6FEB484554653AFF6BAD965BBB19, ___orderID_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3065[3] = 
{
	static_cast<int32_t>(offsetof(ProductManager_tAD80313E4C8F277881B27D63991321241418FBCE, ___Products_4)),static_cast<int32_t>(offsetof(ProductManager_tAD80313E4C8F277881B27D63991321241418FBCE, ___ProductScreenshotsPath_5)),static_cast<int32_t>(offsetof(ProductManager_tAD80313E4C8F277881B27D63991321241418FBCE, ___placeholderPrefab_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3066[9] = 
{
	static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___open_3)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___alsoReverse_4)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___U3CtotalTimeU3E5__2_5)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___U3CcurTimeU3E5__3_6)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___U3CtotalDistU3E5__4_7)),static_cast<int32_t>(offsetof(U3COpenCloseDisplayU3Ed__9_t9C75239BF9729DCA858287FA29BABC9A93789D86, ___U3CfinalPosU3E5__5_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3067[6] = 
{
	static_cast<int32_t>(offsetof(ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103, ___m_collider_4)),static_cast<int32_t>(offsetof(ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103, ___coverObject_5)),static_cast<int32_t>(offsetof(ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103, ___openCoverOffset_6)),static_cast<int32_t>(offsetof(ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103, ___autoCloseCover_7)),0,static_cast<int32_t>(offsetof(ProductWithCover_tDB4441FC1B929989AA7EDDD116F02665DB1EE103, ___IsAnimating_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3068[1] = 
{
	static_cast<int32_t>(offsetof(ProgressHelper_t99435CC1BFDDD2BF74478C6E61ECB6827C080469, ___m_Image_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3069[3] = 
{
	static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__4_t9BB07FD98271F129F63C328344CCC8D40B7C8233, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__4_t9BB07FD98271F129F63C328344CCC8D40B7C8233, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CAnimateGoingToSlotU3Ed__4_t9BB07FD98271F129F63C328344CCC8D40B7C8233, ___U3CU3E4__this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3070[2] = 
{
	static_cast<int32_t>(offsetof(ReadyToServe_t471256A83D0E86C0CC1DEEC3AFBDE5FCFBE93867, ___platePrefab_10)),static_cast<int32_t>(offsetof(ReadyToServe_t471256A83D0E86C0CC1DEEC3AFBDE5FCFBE93867, ___m_collider_11)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3071[8] = 
{
	static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CoutlineU3E5__2_3)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CoutlineColorU3E5__3_4)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CtotalTimeU3E5__4_5)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CcurTimeU3E5__5_6)),static_cast<int32_t>(offsetof(U3CDoEmphasizeU3Ed__9_tA556BCA728BB93E19FE6894E41E6F106D32DF362, ___U3CalphaValU3E5__6_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3072[7] = 
{
	static_cast<int32_t>(offsetof(ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30, ___order_count_4)),static_cast<int32_t>(offsetof(ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30, ___total_order_served_5)),static_cast<int32_t>(offsetof(ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30, ___totalServingTime_6)),static_cast<int32_t>(offsetof(ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30, ___curServeTime_7)),static_cast<int32_t>(offsetof(ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30, ___m_Image_8)),static_cast<int32_t>(offsetof(ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30, ___orderID_9)),static_cast<int32_t>(offsetof(ServeOrder_tC3400DE0124BF8F724EC0CBC23700DB332C76A30, ___serveTimeRepresentation_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3073[10] = 
{
	static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___open_3)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___alsoReverse_4)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CtotalTimeU3E5__2_5)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CcurTimeU3E5__3_6)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CtotalAngleU3E5__4_7)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CmultiplierU3E5__5_8)),static_cast<int32_t>(offsetof(U3CPlayDoorAnimU3Ed__6_t5BCB4F9F18FF190B15F3AFA7BE76778B9443425D, ___U3CfinalAngleU3E5__6_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3074[4] = 
{
	static_cast<int32_t>(offsetof(StoveGameObject_t60DB9BA20178D8177199C2B8CCECB35E520DCDA1, ___doorTransform_12)),static_cast<int32_t>(offsetof(StoveGameObject_t60DB9BA20178D8177199C2B8CCECB35E520DCDA1, ___progressHelperOffset_13)),static_cast<int32_t>(offsetof(StoveGameObject_t60DB9BA20178D8177199C2B8CCECB35E520DCDA1, ___doorIsOpen_14)),static_cast<int32_t>(offsetof(StoveGameObject_t60DB9BA20178D8177199C2B8CCECB35E520DCDA1, ___isAnimating_15)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3075[2] = 
{
	static_cast<int32_t>(offsetof(BasicCameraControl_t06699E83504BCA9DF583E4912B8E523E9E42EC04, ___rotateSpeed_4)),static_cast<int32_t>(offsetof(BasicCameraControl_t06699E83504BCA9DF583E4912B8E523E9E42EC04, ___scrollSmooth_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3081[6] = 
{
	static_cast<int32_t>(offsetof(BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A, ___placeholderPrefab_4)),static_cast<int32_t>(offsetof(BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A_StaticFields, ___onOrderCancelled_5)),static_cast<int32_t>(offsetof(BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A_StaticFields, ___onOrderCompleted_6)),static_cast<int32_t>(offsetof(BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A_StaticFields, ___onProductAddedToSlot_7)),static_cast<int32_t>(offsetof(BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A_StaticFields, ___onProductDeletedFromSlot_8)),static_cast<int32_t>(offsetof(BasicGameEvents_tF2B783EE2D8EDFBEB4F4FE9E334727312A07389A_StaticFields, ___onPlaceHolderRequired_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3082[2] = 
{
	static_cast<int32_t>(offsetof(SliceHelper_t695F49C4F4DB1DF0317D652676704FFEC121C05B, ___slicePrefab_4)),static_cast<int32_t>(offsetof(SliceHelper_t695F49C4F4DB1DF0317D652676704FFEC121C05B, ___sliceCount_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3084[1] = 
{
	static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA_StaticFields, ___8688D249E9D047B4FC2FB89CE05AFE9EC89252FFCCDD969DE6EEF260DD7FFB21_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3090[9] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3091[3] = 
{
	static_cast<int32_t>(offsetof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields, ___Default_0)),static_cast<int32_t>(offsetof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345, ____ygConfig_1)),static_cast<int32_t>(offsetof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345, ____logger_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3093[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3094[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3095[10] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3096[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3097[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3098[7] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3099[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3101[7] = 
{
	static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____ygNode_0)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____config_1)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____parent_2)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____children_3)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____measureFunction_4)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____baselineFunction_5)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____data_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3102[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3103[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3104[2] = 
{
	static_cast<int32_t>(offsetof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA, ___width_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA, ___height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3105[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3106[2] = 
{
	static_cast<int32_t>(offsetof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C, ___value_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C, ___unit_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3107[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3108[10] = 
{
	static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___text_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___font_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___size_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___scaling_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___style_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___color_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___anchor_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___wordWrap_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___wordWrapWidth_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___richText_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3109[3] = 
{
	static_cast<int32_t>(offsetof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3, ___position_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3, ___color_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3, ___uv0_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3111[2] = 
{
	static_cast<int32_t>(offsetof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields, ___RepaintOverlayPanelsCallback_0)),static_cast<int32_t>(offsetof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields, ___UpdateRuntimePanelsCallback_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3112[3] = 
{
	static_cast<int32_t>(offsetof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97, ___offsetFromWriteStart_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97, ___size_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97, ___source_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3113[4] = 
{
	static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___firstIndex_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___indexCount_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___minIndexVal_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___vertsReferenced_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3114[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3115[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3116[8] = 
{
	static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___GraphicsResourcesRecreate_0)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___EngineUpdate_1)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___FlushPendingResources_2)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RegisterIntermediateRenderers_3)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RenderNodeAdd_4)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RenderNodeExecute_5)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RenderNodeCleanup_6)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___s_MarkerRaiseEngineUpdate_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3119[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3120[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3121[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3122[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3123[4] = 
{
	0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3124[4] = 
{
	0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3125[4] = 
{
	0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3126[2] = 
{
	0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3127[2] = 
{
	0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3129[1] = 
{
	0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3131[6] = 
{
	0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3132[1] = 
{
	0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3133[4] = 
{
	0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3135[5] = 
{
	0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3136[2] = 
{
	0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3137[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3138[4] = 
{
	0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3139[15] = 
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3142[20] = 
{
	static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_FaceIndex_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_FamilyName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_StyleName_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_PointSize_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_Scale_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_LineHeight_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_AscentLine_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_CapLine_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_MeanLine_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_Baseline_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_DescentLine_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SuperscriptOffset_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SuperscriptSize_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SubscriptOffset_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SubscriptSize_14)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_UnderlineOffset_15)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_UnderlineThickness_16)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_StrikethroughOffset_17)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_StrikethroughThickness_18)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_TabWidth_19)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3143[5] = 
{
	static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_X_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_Y_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_Width_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_Height_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields, ___s_ZeroGlyphRect_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3144[5] = 
{
	static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_Width_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_Height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_HorizontalBearingX_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_HorizontalBearingY_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_HorizontalAdvance_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3145[5] = 
{
	static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_Index_0)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_Metrics_1)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_GlyphRect_2)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_Scale_3)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_AtlasIndex_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3146[11] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3147[15] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3148[11] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3149[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3150[4] = 
{
	static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___familyName_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___styleName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___faceIndex_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___filePath_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3151[8] = 
{
	static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_Glyphs_0)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphIndexes_MarshallingArray_A_1)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphMarshallingStruct_IN_2)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphMarshallingStruct_OUT_3)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_FreeGlyphRects_4)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_UsedGlyphRects_5)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_PairAdjustmentRecords_MarshallingArray_6)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphLookupDictionary_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3153[5] = 
{
	static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___index_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___metrics_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___glyphRect_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___scale_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___atlasIndex_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3154[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3155[4] = 
{
	static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_XPlacement_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_YPlacement_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_XAdvance_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_YAdvance_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3156[2] = 
{
	static_cast<int32_t>(offsetof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7, ___m_GlyphIndex_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7, ___m_GlyphValueRecord_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3157[3] = 
{
	static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E, ___m_FirstAdjustmentRecord_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E, ___m_SecondAdjustmentRecord_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E, ___m_FeatureLookupFlags_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3161[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3163[11] = 
{
	static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_Time_0)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_FunctionName_1)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_StringParameter_2)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_ObjectReferenceParameter_3)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_FloatParameter_4)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_IntParameter_5)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_MessageOptions_6)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_Source_7)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_StateSender_8)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_AnimatorStateInfo_9)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_AnimatorClipInfo_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3164[2] = 
{
	static_cast<int32_t>(offsetof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03, ___m_ClipInstanceID_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03, ___m_Weight_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3165[9] = 
{
	static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Name_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Path_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_FullPath_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_NormalizedTime_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Length_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Speed_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_SpeedMultiplier_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Tag_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Loop_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3166[8] = 
{
	static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_FullPath_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_UserName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_Name_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_HasFixedDuration_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_Duration_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_NormalizedTime_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_AnyState_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_TransitionType_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3169[1] = 
{
	static_cast<int32_t>(offsetof(AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493, ___OnOverrideControllerDirty_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3170[5] = 
{
	static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___name_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___parentName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___position_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___rotation_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___scale_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3171[5] = 
{
	static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_Min_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_Max_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_Center_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_AxisLength_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_UseDefaultValues_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3172[3] = 
{
	static_cast<int32_t>(offsetof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8, ___m_BoneName_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8, ___m_HumanName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8, ___limit_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3176[1] = 
{
	static_cast<int32_t>(offsetof(AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3177[1] = 
{
	static_cast<int32_t>(offsetof(AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC, ___stream_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3178[2] = 
{
	static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3179[2] = 
{
	static_cast<int32_t>(offsetof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3180[2] = 
{
	static_cast<int32_t>(offsetof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3181[2] = 
{
	static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3182[1] = 
{
	static_cast<int32_t>(offsetof(AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3183[2] = 
{
	static_cast<int32_t>(offsetof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3184[2] = 
{
	static_cast<int32_t>(offsetof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3185[2] = 
{
	static_cast<int32_t>(offsetof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3186[7] = 
{
	static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___m_AnimatorBindingsVersion_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___constant_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___input_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___output_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___workspace_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___inputStreamAccessor_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___animationHandleBinder_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3187[2] = 
{
	static_cast<int32_t>(offsetof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3191[1] = 
{
	static_cast<int32_t>(offsetof(CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860, ___U3CisMaskU3Ek__BackingField_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3192[1] = 
{
	static_cast<int32_t>(offsetof(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields, ___s_Corners_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3193[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3195[5] = 
{
	static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___preWillRenderCanvases_4)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___willRenderCanvases_5)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___U3CexternBeginRenderOverlaysU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___U3CexternRenderOverlaysBeforeU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___U3CexternEndRenderOverlaysU3Ek__BackingField_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3196[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3199[7] = 
{
	static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Controller_0)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Collider_1)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Point_2)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Normal_3)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_MoveDirection_4)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_MoveLength_5)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Push_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3200[7] = 
{
	static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_Impulse_0)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_RelativeVelocity_1)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_Body_2)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_Collider_3)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_ContactCount_4)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_ReusedContacts_5)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_LegacyContacts_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3201[8] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3202[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3203[2] = 
{
	static_cast<int32_t>(offsetof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields, ___ContactModifyEvent_0)),static_cast<int32_t>(offsetof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields, ___ContactModifyEventCCD_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3204[10] = 
{
	static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___actor_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherActor_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___shape_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherShape_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___rotation_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___position_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherRotation_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherPosition_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___numContacts_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___contacts_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3205[6] = 
{
	static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Point_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Normal_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_FaceID_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Distance_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_UV_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Collider_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3213[5] = 
{
	static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_Point_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_Normal_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_ThisColliderInstanceID_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_OtherColliderInstanceID_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_Separation_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3214[1] = 
{
	static_cast<int32_t>(offsetof(PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3216[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3217[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3218[18] = 
{
	static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___font_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___color_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___fontSize_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___lineSpacing_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___richText_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___scaleFactor_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___fontStyle_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___textAnchor_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___alignByGeometry_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___resizeTextForBestFit_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___resizeTextMinSize_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___resizeTextMaxSize_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___updateBounds_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___verticalOverflow_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___horizontalOverflow_14)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___generationExtents_15)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___pivot_16)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___generateOutOfBounds_17)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3219[11] = 
{
	static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Ptr_0)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_LastString_1)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_LastSettings_2)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_HasGenerated_3)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_LastValid_4)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Verts_5)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Characters_6)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Lines_7)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_CachedVerts_8)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_CachedCharacters_9)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_CachedLines_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3220[10] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3221[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3222[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3223[2] = 
{
	static_cast<int32_t>(offsetof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD, ___cursorPos_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD, ___charWidth_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3224[4] = 
{
	static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___startCharIdx_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___topY_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___leading_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3225[11] = 
{
	static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___position_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___normal_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___tangent_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___color_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv0_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv1_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv2_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv3_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields, ___s_DefaultColor_8)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields, ___s_DefaultTangent_9)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields, ___simpleVert_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3227[2] = 
{
	static_cast<int32_t>(offsetof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields, ___textureRebuilt_4)),static_cast<int32_t>(offsetof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6, ___m_FontTextureRebuildCallback_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3229[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3230[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3231[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3232[14] = 
{
	static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_FingerId_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Position_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_RawPosition_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_PositionDelta_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_TimeDelta_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_TapCount_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Phase_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Type_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Pressure_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_maximumPossiblePressure_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Radius_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_RadiusVariance_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_AltitudeAngle_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_AzimuthAngle_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3235[2] = 
{
	static_cast<int32_t>(offsetof(HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314, ___target_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314, ___camera_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3236[9] = 
{
	static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MouseUsed_0)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_LastHit_1)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_MouseDownHit_2)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_CurrentHit_3)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_Cameras_4)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_GetMouseState_5)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MousePosition_6)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MouseButtonPressedThisFrame_7)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MouseButtonIsPressed_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3238[2] = 
{
	static_cast<int32_t>(offsetof(AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056, ___U3CpreferredExtensionU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056, ___U3CotherExtensionsU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3242[2] = 
{
	static_cast<int32_t>(offsetof(NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7, ___U3CQualifiedNativeNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7, ___U3CDeclarationU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3245[2] = 
{
	static_cast<int32_t>(offsetof(NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA, ___U3CConditionU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA, ___U3CEnabledU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3246[1] = 
{
	static_cast<int32_t>(offsetof(NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51, ___U3CHeaderU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3247[1] = 
{
	static_cast<int32_t>(offsetof(NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621, ___U3CNameU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3248[5] = 
{
	static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CIsThreadSafeU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CIsFreeFunctionU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CThrowsExceptionU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CHasExplicitThisU3Ek__BackingField_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3249[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3250[1] = 
{
	static_cast<int32_t>(offsetof(NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607, ___U3CTargetTypeU3Ek__BackingField_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3251[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3253[3] = 
{
	static_cast<int32_t>(offsetof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1, ___U3CHeaderU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1, ___U3CIntermediateScriptingStructNameU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1, ___U3CCodegenOptionsU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3254[1] = 
{
	static_cast<int32_t>(offsetof(NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7, ___U3CExceptionU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3258[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3259[2] = 
{
	static_cast<int32_t>(offsetof(StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A, ___U3CTypeU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3260[1] = 
{
	static_cast<int32_t>(offsetof(NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625, ___U3CThrowsExceptionU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3261[1] = 
{
	static_cast<int32_t>(offsetof(IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D, ___U3CDoesNotContributeToSizeU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3264[3] = 
{
	static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA, ___U3COptionalU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA, ___U3CGenerateProxyU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3266[17] = 
{
	static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Position_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Velocity_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_AnimatedVelocity_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_InitialVelocity_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_AxisOfRotation_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Rotation_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_AngularVelocity_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_StartSize_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_StartColor_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_RandomSeed_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_ParentRandomSeed_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Lifetime_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_StartLifetime_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_MeshIndex_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_EmitAccumulator0_14)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_EmitAccumulator1_15)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Flags_16)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3267[12] = 
{
	static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_Particle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_PositionSet_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_VelocitySet_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_AxisOfRotationSet_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_RotationSet_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_AngularVelocitySet_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_StartSizeSet_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_StartColorSet_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_RandomSeedSet_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_StartLifetimeSet_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_MeshIndexSet_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_ApplyShapeToPosition_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3269[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3273[3] = 
{
	static_cast<int32_t>(offsetof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields, ___OnAudioConfigurationChanged_0)),static_cast<int32_t>(offsetof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields, ___OnAudioSystemShuttingDown_1)),static_cast<int32_t>(offsetof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields, ___OnAudioSystemStartedUp_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3276[2] = 
{
	static_cast<int32_t>(offsetof(AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20, ___m_PCMReaderCallback_4)),static_cast<int32_t>(offsetof(AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20, ___m_PCMSetPositionCallback_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3280[2] = 
{
	static_cast<int32_t>(offsetof(AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2, ___sampleFramesAvailable_0)),static_cast<int32_t>(offsetof(AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2, ___sampleFramesOverflow_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3281[1] = 
{
	static_cast<int32_t>(offsetof(AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3282[1] = 
{
	static_cast<int32_t>(offsetof(AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3283[1] = 
{
	static_cast<int32_t>(offsetof(AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3285[1] = 
{
	static_cast<int32_t>(offsetof(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields, ___m_LastDisabledRigidbody2D_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3286[6] = 
{
	static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Centroid_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Point_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Normal_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Distance_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Fraction_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Collider_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3299[3] = 
{
	static_cast<int32_t>(offsetof(ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F, ___m_nTag_0)),static_cast<int32_t>(offsetof(ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F, ___m_aValue_1)),static_cast<int32_t>(offsetof(ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F, ___elist_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3304[3] = 
{
	static_cast<int32_t>(offsetof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields, ___IsTextualNodeBitmap_0)),static_cast<int32_t>(offsetof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields, ___CanReadContentAsBitmap_1)),static_cast<int32_t>(offsetof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields, ___HasValueBitmap_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3309[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
