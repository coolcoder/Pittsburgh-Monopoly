﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MouseCamLook::Start()
extern void MouseCamLook_Start_m0CE26867CD828A1CBB413712934BEC98E3B2733C (void);
// 0x00000002 System.Void MouseCamLook::Update()
extern void MouseCamLook_Update_m23878426B4D0D0E665CE2748EC3ECBFFF27B6875 (void);
// 0x00000003 System.Void MouseCamLook::.ctor()
extern void MouseCamLook__ctor_m0A7DAEE06B37C79F4F7C1E9F457180E08C2E31A2 (void);
// 0x00000004 System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_m83FD44DCA324CE3D05A71FD2E2991FCD743F003A (void);
// 0x00000005 System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_m5BB6CE35AF68EE00CFEB4BA5EBA17E10667551D3 (void);
// 0x00000006 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mB37559C5B0638161878D20E00B7C672FC38BBBAA (void);
// 0x00000007 System.Void ScreenShotHelper::TakeScreenShot()
extern void ScreenShotHelper_TakeScreenShot_m67E4683B1C8E309EBF6803FCD8A86CA9EE2089FC (void);
// 0x00000008 System.Void ScreenShotHelper::TakeSingleScreenShot(UnityEngine.Camera)
extern void ScreenShotHelper_TakeSingleScreenShot_m4CC2A7B42AAB9D895ACE969D828692E807D96F9B (void);
// 0x00000009 UnityEngine.Texture2D ScreenShotHelper::PositionCamAndGetScreenTexture(UnityEngine.Camera,System.Int32)
extern void ScreenShotHelper_PositionCamAndGetScreenTexture_m4F65B4E1ACE9887818E61FC1550B76D7E853BA0D (void);
// 0x0000000A System.Void ScreenShotHelper::.ctor()
extern void ScreenShotHelper__ctor_mE54CB23495E683FA01CE6B01FBAED6BB4E7CEE0C (void);
// 0x0000000B UnityEngine.Bounds TransformExtensions::EncapsulateBounds(UnityEngine.Transform)
extern void TransformExtensions_EncapsulateBounds_mA6D6847BE9864627D4EB19DEA9D94607C200892F (void);
// 0x0000000C System.Void UnityTemplateProject.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_mE82248046F093F6C5640BDCC8FF4B9996C6D2381 (void);
// 0x0000000D UnityEngine.Vector3 UnityTemplateProject.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_mB7EE1B0B3331A6CC8F52D5671A504D15A4ECB558 (void);
// 0x0000000E System.Void UnityTemplateProject.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mA30F5F5EC736E8A3B298CF87F9A786F6D7E2CB1D (void);
// 0x0000000F System.Void UnityTemplateProject.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_m094201165462FC27919DE9E98C4D53A061197148 (void);
// 0x00000010 System.Void UnityTemplateProject.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mCC26B175D14198A00A2121289F2B3504FEDAC157 (void);
// 0x00000011 System.Void UnityTemplateProject.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m0EF16D858AC07E47E11EAA3AD0D856DEB282B02E (void);
// 0x00000012 System.Void UnityTemplateProject.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProject.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_m59D846E2B246B06ACE4CF5F0AB68C3A2B3AF6213 (void);
// 0x00000013 System.Void UnityTemplateProject.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_m8174E7238F01C8A72D7793F7A850C8E904190084 (void);
// 0x00000014 System.Void UnityTemplateProject.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m93055CACFEBEBA19E2BD9AF02AF66C66CF56D98F (void);
// 0x00000015 System.Void PW.BewerageMaker::Start()
extern void BewerageMaker_Start_m5D44071C661DAB916F6E9F9A2DDEE9CB414E2E6C (void);
// 0x00000016 System.Void PW.BewerageMaker::OnMouseUp()
extern void BewerageMaker_OnMouseUp_mF5396AB3D4D7F284ED241EC359E5D3FB6EF0743B (void);
// 0x00000017 System.Void PW.BewerageMaker::StartFillingStep()
extern void BewerageMaker_StartFillingStep_mFD73429142BA8A4F403F84BFCCC5E893C17ECC35 (void);
// 0x00000018 System.Void PW.BewerageMaker::StartPreFillAnimationState()
extern void BewerageMaker_StartPreFillAnimationState_mD1B1A81A93DEFE1A630A01F9FC8AF9AF24A0142A (void);
// 0x00000019 System.Collections.IEnumerator PW.BewerageMaker::DoPreFill(UnityEngine.Transform,UnityEngine.Transform)
extern void BewerageMaker_DoPreFill_m9E317793ABAEAE084FEA105DD83ADB7C056DFF35 (void);
// 0x0000001A System.Collections.IEnumerator PW.BewerageMaker::DoFillAnimation()
extern void BewerageMaker_DoFillAnimation_m98D9347CF6C09352B73E41BADBAB1390DDCC9CD5 (void);
// 0x0000001B System.Void PW.BewerageMaker::OnFillEnded()
extern void BewerageMaker_OnFillEnded_mF4ADA5992C7FF2191F322150FBE371C91D53E62D (void);
// 0x0000001C System.Void PW.BewerageMaker::SetTheCup()
extern void BewerageMaker_SetTheCup_mB105C745E13E2E8E4159ECAE3DFF442B838A2B9A (void);
// 0x0000001D System.Void PW.BewerageMaker::UnSetTheCup()
extern void BewerageMaker_UnSetTheCup_mC2174A084A6FDDD81961B8BD4C93EC666016BE4C (void);
// 0x0000001E System.Collections.IEnumerator PW.BewerageMaker::DoFillEnded()
extern void BewerageMaker_DoFillEnded_mC018A1215E2E1187E4E0F69818C2851787B06B73 (void);
// 0x0000001F System.Void PW.BewerageMaker::.ctor()
extern void BewerageMaker__ctor_m67575D863733452BCA856FEFEFF08833491607AD (void);
// 0x00000020 System.Void PW.BewerageMaker/<DoPreFill>d__22::.ctor(System.Int32)
extern void U3CDoPreFillU3Ed__22__ctor_m2CCF453D35D403BB326EAD5B29E3B409EE4EF240 (void);
// 0x00000021 System.Void PW.BewerageMaker/<DoPreFill>d__22::System.IDisposable.Dispose()
extern void U3CDoPreFillU3Ed__22_System_IDisposable_Dispose_m5A5D26E9FE3DC2A6944C9023F215309BEB83FAA2 (void);
// 0x00000022 System.Boolean PW.BewerageMaker/<DoPreFill>d__22::MoveNext()
extern void U3CDoPreFillU3Ed__22_MoveNext_m48DFC618B84811E621BA2B2B19035BAA2A3CF6E3 (void);
// 0x00000023 System.Object PW.BewerageMaker/<DoPreFill>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoPreFillU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB69CB79B25F1A3F3234915A278EA9038877EE8 (void);
// 0x00000024 System.Void PW.BewerageMaker/<DoPreFill>d__22::System.Collections.IEnumerator.Reset()
extern void U3CDoPreFillU3Ed__22_System_Collections_IEnumerator_Reset_m56E15442839DA9FFC484EFA1AC32ECB2C9789F17 (void);
// 0x00000025 System.Object PW.BewerageMaker/<DoPreFill>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CDoPreFillU3Ed__22_System_Collections_IEnumerator_get_Current_m3FADC58F5E631644909FB08D0FC8EBDCAF0E33F1 (void);
// 0x00000026 System.Void PW.BewerageMaker/<DoFillAnimation>d__23::.ctor(System.Int32)
extern void U3CDoFillAnimationU3Ed__23__ctor_mF31A8754803121151C0F709EC129C41093D35376 (void);
// 0x00000027 System.Void PW.BewerageMaker/<DoFillAnimation>d__23::System.IDisposable.Dispose()
extern void U3CDoFillAnimationU3Ed__23_System_IDisposable_Dispose_mFD6E1DBBB118EC1775FBE7E30B9DC58D7548B64B (void);
// 0x00000028 System.Boolean PW.BewerageMaker/<DoFillAnimation>d__23::MoveNext()
extern void U3CDoFillAnimationU3Ed__23_MoveNext_mFE845050E123AFBE705A869D062FE0CBD6AD273D (void);
// 0x00000029 System.Object PW.BewerageMaker/<DoFillAnimation>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoFillAnimationU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29010EF7132A841816272D536822E98AA2646AF8 (void);
// 0x0000002A System.Void PW.BewerageMaker/<DoFillAnimation>d__23::System.Collections.IEnumerator.Reset()
extern void U3CDoFillAnimationU3Ed__23_System_Collections_IEnumerator_Reset_mAAE3BB1AB3CC18037867B61C3CBF326152310CEC (void);
// 0x0000002B System.Object PW.BewerageMaker/<DoFillAnimation>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CDoFillAnimationU3Ed__23_System_Collections_IEnumerator_get_Current_m65263ACC665024966B3DEFA2D52039A61838E692 (void);
// 0x0000002C System.Void PW.BewerageMaker/<DoFillEnded>d__27::.ctor(System.Int32)
extern void U3CDoFillEndedU3Ed__27__ctor_m12BC4C8DAC5B2D461FA5B7F07D9B8A90F86E570A (void);
// 0x0000002D System.Void PW.BewerageMaker/<DoFillEnded>d__27::System.IDisposable.Dispose()
extern void U3CDoFillEndedU3Ed__27_System_IDisposable_Dispose_m7C18B2D7A6F8D0584C2A63A0FB22CEADB1448E2F (void);
// 0x0000002E System.Boolean PW.BewerageMaker/<DoFillEnded>d__27::MoveNext()
extern void U3CDoFillEndedU3Ed__27_MoveNext_mAD9E6E3D4C91315967DF8D4CB525B19D568684EB (void);
// 0x0000002F System.Object PW.BewerageMaker/<DoFillEnded>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoFillEndedU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2217288F2FED4BE1E79762096CEC25C46C88A9C0 (void);
// 0x00000030 System.Void PW.BewerageMaker/<DoFillEnded>d__27::System.Collections.IEnumerator.Reset()
extern void U3CDoFillEndedU3Ed__27_System_Collections_IEnumerator_Reset_mF6E3BA4680E4B827B067715DAF7ED5C40AFA0821 (void);
// 0x00000031 System.Object PW.BewerageMaker/<DoFillEnded>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CDoFillEndedU3Ed__27_System_Collections_IEnumerator_get_Current_m410D676E9B95F62AF9AC3A8F3020306FC528DCFF (void);
// 0x00000032 System.Void PW.CookableProduct::Awake()
extern void CookableProduct_Awake_m7E52E723301EC2AEFEF64EEC6F0C20E65A965B9E (void);
// 0x00000033 System.Void PW.CookableProduct::Start()
extern void CookableProduct_Start_mFF014D7E10DB320A2C16E24BD862EB02D08A4429 (void);
// 0x00000034 System.Void PW.CookableProduct::OnEnable()
extern void CookableProduct_OnEnable_m1A7FA71FE7B92CE79CF84C03F05BD01A449ECF0B (void);
// 0x00000035 System.Void PW.CookableProduct::OnMouseDown()
extern void CookableProduct_OnMouseDown_m2A2351AE1E5FDC6950C2FA5755E5A2C70314ED1C (void);
// 0x00000036 System.Collections.IEnumerator PW.CookableProduct::MoveToPlace(UnityEngine.Vector3)
extern void CookableProduct_MoveToPlace_m920C68A6D5532C7774561309E52952322F7D4E05 (void);
// 0x00000037 System.Void PW.CookableProduct::DoneCooking()
extern void CookableProduct_DoneCooking_m7827D283E6169AB4B193307CAE2A1F195A5E5ED6 (void);
// 0x00000038 System.Boolean PW.CookableProduct::CanGoPlayerSlot()
extern void CookableProduct_CanGoPlayerSlot_m348354C9C878BFB8680079D36480F5DB4C7C56F0 (void);
// 0x00000039 System.Collections.IEnumerator PW.CookableProduct::AnimateGoingToSlot()
extern void CookableProduct_AnimateGoingToSlot_m201A519A0F7D51500E24561C229D2FF257FD17B0 (void);
// 0x0000003A System.Void PW.CookableProduct::.ctor()
extern void CookableProduct__ctor_mBC45BD9CCF71DE1E495674D712B953AF56519D7A (void);
// 0x0000003B System.Collections.IEnumerator PW.CookableProduct::<>n__0(UnityEngine.Vector3)
extern void CookableProduct_U3CU3En__0_m9C5125D13F07C66DF19D71CE66FB5B4EB66BFBB9 (void);
// 0x0000003C System.Collections.IEnumerator PW.CookableProduct::<>n__1()
extern void CookableProduct_U3CU3En__1_m10A3A6B4272585CD94882169939F3D33C5DDBD96 (void);
// 0x0000003D System.Void PW.CookableProduct/<MoveToPlace>d__10::.ctor(System.Int32)
extern void U3CMoveToPlaceU3Ed__10__ctor_mC55673548E84392B1B530512E079018DE2FE5701 (void);
// 0x0000003E System.Void PW.CookableProduct/<MoveToPlace>d__10::System.IDisposable.Dispose()
extern void U3CMoveToPlaceU3Ed__10_System_IDisposable_Dispose_m0E5D06245D9E330897DD6C8D0126006F51DFC6F3 (void);
// 0x0000003F System.Boolean PW.CookableProduct/<MoveToPlace>d__10::MoveNext()
extern void U3CMoveToPlaceU3Ed__10_MoveNext_m3C9E195CE1384D6C56A5EBD067189EA44BC44B5A (void);
// 0x00000040 System.Object PW.CookableProduct/<MoveToPlace>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveToPlaceU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB23A2971E30C793924B37BD1687A47DEF81019 (void);
// 0x00000041 System.Void PW.CookableProduct/<MoveToPlace>d__10::System.Collections.IEnumerator.Reset()
extern void U3CMoveToPlaceU3Ed__10_System_Collections_IEnumerator_Reset_m41249B60A3F337A9819839BD8610BA988BADD499 (void);
// 0x00000042 System.Object PW.CookableProduct/<MoveToPlace>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CMoveToPlaceU3Ed__10_System_Collections_IEnumerator_get_Current_m16A783832AF9EB8009859552CD092AC0015487A2 (void);
// 0x00000043 System.Void PW.CookableProduct/<AnimateGoingToSlot>d__13::.ctor(System.Int32)
extern void U3CAnimateGoingToSlotU3Ed__13__ctor_m29812D63A58CA74C5DCFBF05EF414E5847B39E53 (void);
// 0x00000044 System.Void PW.CookableProduct/<AnimateGoingToSlot>d__13::System.IDisposable.Dispose()
extern void U3CAnimateGoingToSlotU3Ed__13_System_IDisposable_Dispose_m3650EED8EBA5388BB154D9735BEBEBB08F7C87EE (void);
// 0x00000045 System.Boolean PW.CookableProduct/<AnimateGoingToSlot>d__13::MoveNext()
extern void U3CAnimateGoingToSlotU3Ed__13_MoveNext_m67246D8E9025516A11C6509DE4B70CAFD4B35793 (void);
// 0x00000046 System.Object PW.CookableProduct/<AnimateGoingToSlot>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4EE558D306ED76975C4F2531960645E60DA51FB (void);
// 0x00000047 System.Void PW.CookableProduct/<AnimateGoingToSlot>d__13::System.Collections.IEnumerator.Reset()
extern void U3CAnimateGoingToSlotU3Ed__13_System_Collections_IEnumerator_Reset_m985BE789A58FFAD9A4D9D4871B97F0B3326FBA7A (void);
// 0x00000048 System.Object PW.CookableProduct/<AnimateGoingToSlot>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__13_System_Collections_IEnumerator_get_Current_mB747FB2C2F789A01E511E763C47D36464F7AEAF9 (void);
// 0x00000049 System.Void PW.CookingGameObject::Awake()
extern void CookingGameObject_Awake_m42DE51A159764A79B3A3A1D757ED44AF9A07789D (void);
// 0x0000004A System.Void PW.CookingGameObject::Start()
extern void CookingGameObject_Start_mC5BD2EC3DE36A6A23837095DDFBA3B2A3085F0BC (void);
// 0x0000004B UnityEngine.Vector3 PW.CookingGameObject::GetCookingPosition()
extern void CookingGameObject_GetCookingPosition_m0E90FA7E66C116321D0934C16A8AF2BA9B262A7E (void);
// 0x0000004C System.Void PW.CookingGameObject::DoDoorAnimationsIfNeeded()
extern void CookingGameObject_DoDoorAnimationsIfNeeded_mAE0C5623664A8ADAF52194835866E98092BA66EA (void);
// 0x0000004D System.Boolean PW.CookingGameObject::IsEmpty()
extern void CookingGameObject_IsEmpty_m77054C4C7D65A1CFC8B0D51DDD311CBCC88A2EF7 (void);
// 0x0000004E System.Void PW.CookingGameObject::StartCooking(PW.CookableProduct)
extern void CookingGameObject_StartCooking_m2E3F2CF0111C7736EBD1565F921D80EFB892365D (void);
// 0x0000004F System.Void PW.CookingGameObject::ReadyToServe()
extern void CookingGameObject_ReadyToServe_m89116C0838B818583F7542C53F3E3A78C74450BE (void);
// 0x00000050 System.Boolean PW.CookingGameObject::HasStartAnimationPos(UnityEngine.Vector3&)
extern void CookingGameObject_HasStartAnimationPos_m9EBBB5672D510B7276361C3AF29E721E4FF4A118 (void);
// 0x00000051 System.Collections.IEnumerator PW.CookingGameObject::Cooking()
extern void CookingGameObject_Cooking_mD0CF94DB451C9BC02ECC48525CE7E0C9B45C5B49 (void);
// 0x00000052 System.Void PW.CookingGameObject::OnMouseDown()
extern void CookingGameObject_OnMouseDown_mB1666607191D2A29798281FFDFABD127A506E8FD (void);
// 0x00000053 System.Void PW.CookingGameObject::.ctor()
extern void CookingGameObject__ctor_m1396236A333A207991B2F18B5D1015C6CA2AABA8 (void);
// 0x00000054 System.Void PW.CookingGameObject/<Cooking>d__16::.ctor(System.Int32)
extern void U3CCookingU3Ed__16__ctor_m4DC56D9EAF4C03B52FD7E94F4C2D1C9CA00A748C (void);
// 0x00000055 System.Void PW.CookingGameObject/<Cooking>d__16::System.IDisposable.Dispose()
extern void U3CCookingU3Ed__16_System_IDisposable_Dispose_mD1F0ED9D4121157F52D4CF34CF4E76B246184CC0 (void);
// 0x00000056 System.Boolean PW.CookingGameObject/<Cooking>d__16::MoveNext()
extern void U3CCookingU3Ed__16_MoveNext_m109A11AA999071DF83D34CD68AD2D1B992F69227 (void);
// 0x00000057 System.Object PW.CookingGameObject/<Cooking>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCookingU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m936BA20BD362E9BE19A3FAB0F8B153FA1E96F19E (void);
// 0x00000058 System.Void PW.CookingGameObject/<Cooking>d__16::System.Collections.IEnumerator.Reset()
extern void U3CCookingU3Ed__16_System_Collections_IEnumerator_Reset_m6F9C3719E88C0AC5CCCD96F7DEEE8EA8E6C26D1D (void);
// 0x00000059 System.Object PW.CookingGameObject/<Cooking>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CCookingU3Ed__16_System_Collections_IEnumerator_get_Current_m2D8FA3BD1C1002EA90106503166C8FC7ABD6FAB0 (void);
// 0x0000005A System.Void PW.CreateProductOnPlaceHolder::OnMouseDown()
extern void CreateProductOnPlaceHolder_OnMouseDown_m2C0269002F949FAC1031587AC618D471E60E979D (void);
// 0x0000005B System.Void PW.CreateProductOnPlaceHolder::.ctor()
extern void CreateProductOnPlaceHolder__ctor_m6578F0B8702C18E29707B0E238D32826E562BED7 (void);
// 0x0000005C System.Void PW.DeleteProductOnSlot::Start()
extern void DeleteProductOnSlot_Start_m8E643735A01EFF30FCDA6B7E3294D1C6AF69C4C0 (void);
// 0x0000005D System.Void PW.DeleteProductOnSlot::ToggleDeleteMode()
extern void DeleteProductOnSlot_ToggleDeleteMode_m08838505D890F295C7A8E5F846E5533BDFD3E297 (void);
// 0x0000005E System.Void PW.DeleteProductOnSlot::ChangeUI()
extern void DeleteProductOnSlot_ChangeUI_m1A7965B9D14C5C8867B68CD491E0F2AE59D7B3AC (void);
// 0x0000005F System.Void PW.DeleteProductOnSlot::DeletePlayerSlotImage()
extern void DeleteProductOnSlot_DeletePlayerSlotImage_m41A22BA4A70B93E26EDFD4F019FA17C2A6F3ACEB (void);
// 0x00000060 System.Void PW.DeleteProductOnSlot::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void DeleteProductOnSlot_OnPointerClick_m75B670817B4C1019F2E82DAA10332A2D49EDD4F1 (void);
// 0x00000061 System.Void PW.DeleteProductOnSlot::.ctor()
extern void DeleteProductOnSlot__ctor_m334E5890C72B8C02F77D8096473E922E63881E35 (void);
// 0x00000062 System.Void PW.DeleteProductOnSlot::<Start>b__3_0()
extern void DeleteProductOnSlot_U3CStartU3Eb__3_0_m4E7D1D3F708B7F71789A5B35DD01248013B68ACC (void);
// 0x00000063 System.Collections.IEnumerator PW.DrinkableProduct::AnimateGoingToSlot()
extern void DrinkableProduct_AnimateGoingToSlot_mA4CA6EA84F9F55992C257602B7E16E8DB229897E (void);
// 0x00000064 System.Void PW.DrinkableProduct::.ctor()
extern void DrinkableProduct__ctor_mB0A8BA3A9812812A3A253D4D0FE22C0211A64FD6 (void);
// 0x00000065 System.Collections.IEnumerator PW.DrinkableProduct::<>n__0()
extern void DrinkableProduct_U3CU3En__0_m2084F9553CF4FCBDF3F7C219CCDA74FB932DF292 (void);
// 0x00000066 System.Void PW.DrinkableProduct/<AnimateGoingToSlot>d__0::.ctor(System.Int32)
extern void U3CAnimateGoingToSlotU3Ed__0__ctor_mEFE8F2680FFF27D8BB3372EDAC22F190AA97716A (void);
// 0x00000067 System.Void PW.DrinkableProduct/<AnimateGoingToSlot>d__0::System.IDisposable.Dispose()
extern void U3CAnimateGoingToSlotU3Ed__0_System_IDisposable_Dispose_m4C77151BC22656E7EA52279D6E6BE376C5E97459 (void);
// 0x00000068 System.Boolean PW.DrinkableProduct/<AnimateGoingToSlot>d__0::MoveNext()
extern void U3CAnimateGoingToSlotU3Ed__0_MoveNext_m0C490926B98A68CA3C60A1553DE2EB9EA7166FAE (void);
// 0x00000069 System.Object PW.DrinkableProduct/<AnimateGoingToSlot>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC302E0CCA0E3EF891EDA5A877A7E192571967C3 (void);
// 0x0000006A System.Void PW.DrinkableProduct/<AnimateGoingToSlot>d__0::System.Collections.IEnumerator.Reset()
extern void U3CAnimateGoingToSlotU3Ed__0_System_Collections_IEnumerator_Reset_m08A2AB6C674698D177F54867DBA5AE0B0E27B58E (void);
// 0x0000006B System.Object PW.DrinkableProduct/<AnimateGoingToSlot>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__0_System_Collections_IEnumerator_get_Current_mEF17E9AA4A43EEA4B5ACAA9731752EA4EE127E7D (void);
// 0x0000006C System.Void PW.FillCupHelper::Awake()
extern void FillCupHelper_Awake_mAB9F48D744CD334BAD517E9353D8C99473FCE89D (void);
// 0x0000006D System.Void PW.FillCupHelper::SetMachine(PW.BewerageMaker)
extern void FillCupHelper_SetMachine_mE1DA8E4487793EB91491B5C4AA4854A29B6479E8 (void);
// 0x0000006E System.Void PW.FillCupHelper::DoFill(System.Single)
extern void FillCupHelper_DoFill_m842486F9939896D1DF9D93903E9354D44B117F4C (void);
// 0x0000006F System.Collections.IEnumerator PW.FillCupHelper::FillAnimation(System.Single)
extern void FillCupHelper_FillAnimation_m4CB6D4E55563B0D50DD92FC602B40EE1060C08EF (void);
// 0x00000070 System.Void PW.FillCupHelper::FillEnded()
extern void FillCupHelper_FillEnded_mDF3F85EB2C172302B510AD8A9EA4825DFF9D3C05 (void);
// 0x00000071 System.Void PW.FillCupHelper::OnMouseDown()
extern void FillCupHelper_OnMouseDown_m580A47EAE3AB1FB555DC214707E8C5FD8C18D491 (void);
// 0x00000072 System.Void PW.FillCupHelper::AddToPlayerSpot()
extern void FillCupHelper_AddToPlayerSpot_mFDFA2C68C00C26904CA0298884FE5204816FC479 (void);
// 0x00000073 System.Void PW.FillCupHelper::.ctor()
extern void FillCupHelper__ctor_m0D020C7AB618A0C374B6D52D81637AF118295510 (void);
// 0x00000074 System.Void PW.FillCupHelper/<FillAnimation>d__6::.ctor(System.Int32)
extern void U3CFillAnimationU3Ed__6__ctor_m9E2A85FEC6A41133BA17CBFDAB6A9199D9898FDD (void);
// 0x00000075 System.Void PW.FillCupHelper/<FillAnimation>d__6::System.IDisposable.Dispose()
extern void U3CFillAnimationU3Ed__6_System_IDisposable_Dispose_m7FB6A92A604EC0C6B81195D12B834FF3F83D638D (void);
// 0x00000076 System.Boolean PW.FillCupHelper/<FillAnimation>d__6::MoveNext()
extern void U3CFillAnimationU3Ed__6_MoveNext_mE8D995E0144B7F5BB190D6F5FA1B5FBED31A7BCD (void);
// 0x00000077 System.Object PW.FillCupHelper/<FillAnimation>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFillAnimationU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D73BE3695942253F5DB7022D77C6EE41EBD1B79 (void);
// 0x00000078 System.Void PW.FillCupHelper/<FillAnimation>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFillAnimationU3Ed__6_System_Collections_IEnumerator_Reset_m285244F619255A6E96DEBCD41380FA7D0ED3D268 (void);
// 0x00000079 System.Object PW.FillCupHelper/<FillAnimation>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFillAnimationU3Ed__6_System_Collections_IEnumerator_get_Current_m2422E615EE6D97E487460A010221087439FB9D9D (void);
// 0x0000007A System.Void PW.HeatableProduct::Awake()
extern void HeatableProduct_Awake_mD27989284289F9EDE136DCC53DDF42A199A6F4EA (void);
// 0x0000007B System.Void PW.HeatableProduct::OnEnable()
extern void HeatableProduct_OnEnable_mE839F421F5687FB5F5A34C034C44540EEF3ABFD3 (void);
// 0x0000007C System.Void PW.HeatableProduct::Start()
extern void HeatableProduct_Start_m513BF1F393BABE7EF2E9BCA6AF461C6B1D84EF70 (void);
// 0x0000007D System.Void PW.HeatableProduct::OnMouseDown()
extern void HeatableProduct_OnMouseDown_m809F3B297AB8D69E8B6CEE7AD4B8D2007C19B011 (void);
// 0x0000007E System.Collections.IEnumerator PW.HeatableProduct::MoveToMicrowave()
extern void HeatableProduct_MoveToMicrowave_m04F1ACF9952AE90CCE0AC514E3C9461018251F67 (void);
// 0x0000007F System.Collections.IEnumerator PW.HeatableProduct::AnimateGoingToSlot()
extern void HeatableProduct_AnimateGoingToSlot_mC7AE368ECF9DACE180E5DE23CEC53A73D476ED90 (void);
// 0x00000080 System.Void PW.HeatableProduct::.ctor()
extern void HeatableProduct__ctor_m99758E19CA16171904BD59B770EFAA76C2169F02 (void);
// 0x00000081 System.Collections.IEnumerator PW.HeatableProduct::<>n__0(UnityEngine.Vector3)
extern void HeatableProduct_U3CU3En__0_m1CA02C6823A245B69B83217988B58F29850502B9 (void);
// 0x00000082 System.Collections.IEnumerator PW.HeatableProduct::<>n__1()
extern void HeatableProduct_U3CU3En__1_mE0C903218931D29516E5A99F2BE252E49F679AA0 (void);
// 0x00000083 System.Void PW.HeatableProduct/<MoveToMicrowave>d__9::.ctor(System.Int32)
extern void U3CMoveToMicrowaveU3Ed__9__ctor_m3F003BE1A570F4F09B71D5D3EE33802D10157DFF (void);
// 0x00000084 System.Void PW.HeatableProduct/<MoveToMicrowave>d__9::System.IDisposable.Dispose()
extern void U3CMoveToMicrowaveU3Ed__9_System_IDisposable_Dispose_m32C0A27A283C2464B08C079FA05EA5743C793A0C (void);
// 0x00000085 System.Boolean PW.HeatableProduct/<MoveToMicrowave>d__9::MoveNext()
extern void U3CMoveToMicrowaveU3Ed__9_MoveNext_m943C84FA820D0D675211BC864515BBB01A073C6C (void);
// 0x00000086 System.Object PW.HeatableProduct/<MoveToMicrowave>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveToMicrowaveU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA04255AAD683470EB9FDE4CAAD43F5404808EFB1 (void);
// 0x00000087 System.Void PW.HeatableProduct/<MoveToMicrowave>d__9::System.Collections.IEnumerator.Reset()
extern void U3CMoveToMicrowaveU3Ed__9_System_Collections_IEnumerator_Reset_mC2C4B21CB18B8FEFB23474AB04B9CE4A7F5872D6 (void);
// 0x00000088 System.Object PW.HeatableProduct/<MoveToMicrowave>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CMoveToMicrowaveU3Ed__9_System_Collections_IEnumerator_get_Current_mC24AFD4846B1DF6418D79F74A3268A4126F6B12C (void);
// 0x00000089 System.Void PW.HeatableProduct/<AnimateGoingToSlot>d__10::.ctor(System.Int32)
extern void U3CAnimateGoingToSlotU3Ed__10__ctor_m82B88F80A12A42C091085DF76AA9C18682D40951 (void);
// 0x0000008A System.Void PW.HeatableProduct/<AnimateGoingToSlot>d__10::System.IDisposable.Dispose()
extern void U3CAnimateGoingToSlotU3Ed__10_System_IDisposable_Dispose_m8F51147B63B854C0821365F509022C9344B49FE1 (void);
// 0x0000008B System.Boolean PW.HeatableProduct/<AnimateGoingToSlot>d__10::MoveNext()
extern void U3CAnimateGoingToSlotU3Ed__10_MoveNext_mB2059FD8B71BD867B3046AA396567A5E0366B8D2 (void);
// 0x0000008C System.Object PW.HeatableProduct/<AnimateGoingToSlot>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FFC3BF3F10B448155D1E07F3365745CE178E3E4 (void);
// 0x0000008D System.Void PW.HeatableProduct/<AnimateGoingToSlot>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateGoingToSlotU3Ed__10_System_Collections_IEnumerator_Reset_m1E6497D7CC2429B04451B8D3FC7E341B0858F924 (void);
// 0x0000008E System.Object PW.HeatableProduct/<AnimateGoingToSlot>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__10_System_Collections_IEnumerator_get_Current_m468BCF0F0997F3A20FD9D0715CA4736EF398A9B7 (void);
// 0x0000008F System.Boolean PW.Microwave::get_isEmpty()
extern void Microwave_get_isEmpty_m9D08095A1952B576BF29AFF1DD8C482801A6F632 (void);
// 0x00000090 System.Void PW.Microwave::Start()
extern void Microwave_Start_m498227BFE23459563397A7E6CF314CE45904CBFD (void);
// 0x00000091 System.Void PW.Microwave::SetProduct(PW.HeatableProduct,System.Single)
extern void Microwave_SetProduct_mEA595C8917E2D30389AA647AD168E2FFC89B52AF (void);
// 0x00000092 System.Collections.IEnumerator PW.Microwave::OpenMicrowaveAndHeatProduct()
extern void Microwave_OpenMicrowaveAndHeatProduct_mFC7BDF344F6EE2FA1CA7759CCF649BF6591BA4EA (void);
// 0x00000093 System.Collections.IEnumerator PW.Microwave::PlayDoorAnim(System.Boolean,System.Boolean)
extern void Microwave_PlayDoorAnim_m449585915E4CC88A30A97AE208B60B7C2B9C52B0 (void);
// 0x00000094 System.Collections.IEnumerator PW.Microwave::Heating()
extern void Microwave_Heating_mD64BF9BCD8E04F65F55D075AF30A80D099E0A844 (void);
// 0x00000095 System.Void PW.Microwave::OnMouseDown()
extern void Microwave_OnMouseDown_mA1F0D0732C4075D512085D105CB517161D592CD3 (void);
// 0x00000096 System.Void PW.Microwave::.ctor()
extern void Microwave__ctor_m1A826BC4616C8085C6F2EF2CCD833A459035D053 (void);
// 0x00000097 System.Void PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::.ctor(System.Int32)
extern void U3COpenMicrowaveAndHeatProductU3Ed__12__ctor_m9E1B1BA967ACDA5851F7929079E035546A91B73A (void);
// 0x00000098 System.Void PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::System.IDisposable.Dispose()
extern void U3COpenMicrowaveAndHeatProductU3Ed__12_System_IDisposable_Dispose_m49425A50C0BE957152A0B2624DB5C79BDC2CDF04 (void);
// 0x00000099 System.Boolean PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::MoveNext()
extern void U3COpenMicrowaveAndHeatProductU3Ed__12_MoveNext_m9C79F9AFDC3E41F214F8F265EE48EBC95FD1C6D7 (void);
// 0x0000009A System.Object PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COpenMicrowaveAndHeatProductU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8348C53100064A94E6BE21E2FAB6428CD3763EA6 (void);
// 0x0000009B System.Void PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::System.Collections.IEnumerator.Reset()
extern void U3COpenMicrowaveAndHeatProductU3Ed__12_System_Collections_IEnumerator_Reset_m8CC7304E845048F407D2EDB254C7F0EAAF8A93AC (void);
// 0x0000009C System.Object PW.Microwave/<OpenMicrowaveAndHeatProduct>d__12::System.Collections.IEnumerator.get_Current()
extern void U3COpenMicrowaveAndHeatProductU3Ed__12_System_Collections_IEnumerator_get_Current_mB467EDD0021945CE6344828ECC533399B81BACD1 (void);
// 0x0000009D System.Void PW.Microwave/<PlayDoorAnim>d__13::.ctor(System.Int32)
extern void U3CPlayDoorAnimU3Ed__13__ctor_mD63C61479F4CC92D0DE9B33F7B9976A589870221 (void);
// 0x0000009E System.Void PW.Microwave/<PlayDoorAnim>d__13::System.IDisposable.Dispose()
extern void U3CPlayDoorAnimU3Ed__13_System_IDisposable_Dispose_m65B4C482440FB2D735B5246969522285035A0D80 (void);
// 0x0000009F System.Boolean PW.Microwave/<PlayDoorAnim>d__13::MoveNext()
extern void U3CPlayDoorAnimU3Ed__13_MoveNext_m3B898AFBEE74D25A3532304B1D2BB1B325D604AD (void);
// 0x000000A0 System.Object PW.Microwave/<PlayDoorAnim>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayDoorAnimU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C3EEB619ECA89E66E26F61BE56292837D26E3F7 (void);
// 0x000000A1 System.Void PW.Microwave/<PlayDoorAnim>d__13::System.Collections.IEnumerator.Reset()
extern void U3CPlayDoorAnimU3Ed__13_System_Collections_IEnumerator_Reset_mEAD2FC029EA16333F068F7A3036BE4D97DD7DF2B (void);
// 0x000000A2 System.Object PW.Microwave/<PlayDoorAnim>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CPlayDoorAnimU3Ed__13_System_Collections_IEnumerator_get_Current_m88AC95FEFEFE05DD446379884E5905E7934EBA16 (void);
// 0x000000A3 System.Void PW.Microwave/<Heating>d__14::.ctor(System.Int32)
extern void U3CHeatingU3Ed__14__ctor_m58FA3FD9B3B2BBBFFC825C6FE952B9B53E161C4E (void);
// 0x000000A4 System.Void PW.Microwave/<Heating>d__14::System.IDisposable.Dispose()
extern void U3CHeatingU3Ed__14_System_IDisposable_Dispose_mDE11C1265C111B1316BA7A62A966C6E3D92A9688 (void);
// 0x000000A5 System.Boolean PW.Microwave/<Heating>d__14::MoveNext()
extern void U3CHeatingU3Ed__14_MoveNext_m9A6AE9D6B4E352FE14B2A3624CE9B062B111F2DA (void);
// 0x000000A6 System.Object PW.Microwave/<Heating>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHeatingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m315B0A8B73D7854AF74B889B7B34351FB3F4D376 (void);
// 0x000000A7 System.Void PW.Microwave/<Heating>d__14::System.Collections.IEnumerator.Reset()
extern void U3CHeatingU3Ed__14_System_Collections_IEnumerator_Reset_mA62BF73547B0A1F6E7FD5CE3CC27B792B0665EC8 (void);
// 0x000000A8 System.Object PW.Microwave/<Heating>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CHeatingU3Ed__14_System_Collections_IEnumerator_get_Current_mB26C6EF480B8C4006D2C948C25A4CB93587133ED (void);
// 0x000000A9 System.Void PW.OnClickCoverHelper::OnEnable()
extern void OnClickCoverHelper_OnEnable_m72CA0C827CDC4545E530C5602C4005550D322366 (void);
// 0x000000AA System.Void PW.OnClickCoverHelper::OnMouseDown()
extern void OnClickCoverHelper_OnMouseDown_mE4809DCE7BFAA2AE8ED8B3282710B78E6FDA76A2 (void);
// 0x000000AB System.Void PW.OnClickCoverHelper::ActivateCollider()
extern void OnClickCoverHelper_ActivateCollider_m84D7536878BA7EFA0C79ADB1D13C0832219F869D (void);
// 0x000000AC System.Void PW.OnClickCoverHelper::.ctor()
extern void OnClickCoverHelper__ctor_m6482AAC78FA66BFCC4D2654E95A00038B596026D (void);
// 0x000000AD System.Void PW.OrderGenerator::OnEnable()
extern void OrderGenerator_OnEnable_m65248CB8BF28E3EC028F603C2C7C93D277EBE96D (void);
// 0x000000AE System.Void PW.OrderGenerator::OnDisable()
extern void OrderGenerator_OnDisable_m8D8FBC82B19BBA1FF4024FE90477F246F478BD69 (void);
// 0x000000AF System.Void PW.OrderGenerator::BasicGameEvents_onOrderCancelled(System.Int32)
extern void OrderGenerator_BasicGameEvents_onOrderCancelled_mDFCF8A5D4BAF050A52BC6AC9791BF8AB0039D47B (void);
// 0x000000B0 System.Void PW.OrderGenerator::BasicGameEvents_onOrderCompleted(System.Int32,System.Single)
extern void OrderGenerator_BasicGameEvents_onOrderCompleted_mED94FB79877EF1AF2C743E723371BFF2F078E56E (void);
// 0x000000B1 System.Void PW.OrderGenerator::Start()
extern void OrderGenerator_Start_m0F9DCCF6BCAF003C25CB7F8443642637ACD768EC (void);
// 0x000000B2 System.Collections.IEnumerator PW.OrderGenerator::GenerateOrderRoutine(System.Single)
extern void OrderGenerator_GenerateOrderRoutine_mD8CD4F228F949959C26152E31048C6D6495959FA (void);
// 0x000000B3 System.Void PW.OrderGenerator::GenerateOrder()
extern void OrderGenerator_GenerateOrder_m4C699751B3C7F93845A839304D3A0C233E13A457 (void);
// 0x000000B4 UnityEngine.Sprite PW.OrderGenerator::GetSpriteForOrder(System.Int32)
extern void OrderGenerator_GetSpriteForOrder_m7357C137692CF48A99F0519FC93007C1A135642A (void);
// 0x000000B5 System.Void PW.OrderGenerator::.ctor()
extern void OrderGenerator__ctor_m88EA0085E585C50ECE16C54F717A6F72FFF603ED (void);
// 0x000000B6 System.Void PW.OrderGenerator/<GenerateOrderRoutine>d__11::.ctor(System.Int32)
extern void U3CGenerateOrderRoutineU3Ed__11__ctor_m198D0C1D1A8B6C98C329FBF0DDEFA2A75A24CBBC (void);
// 0x000000B7 System.Void PW.OrderGenerator/<GenerateOrderRoutine>d__11::System.IDisposable.Dispose()
extern void U3CGenerateOrderRoutineU3Ed__11_System_IDisposable_Dispose_m19536EC50B60FB9CDCC1E049D7C163C7E24F1BDB (void);
// 0x000000B8 System.Boolean PW.OrderGenerator/<GenerateOrderRoutine>d__11::MoveNext()
extern void U3CGenerateOrderRoutineU3Ed__11_MoveNext_m699F83C0DECB30E288ED8557BF35A25E9EFD901B (void);
// 0x000000B9 System.Object PW.OrderGenerator/<GenerateOrderRoutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateOrderRoutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31A50C29268C0CB68C77E839AE8476F7C9F0AD14 (void);
// 0x000000BA System.Void PW.OrderGenerator/<GenerateOrderRoutine>d__11::System.Collections.IEnumerator.Reset()
extern void U3CGenerateOrderRoutineU3Ed__11_System_Collections_IEnumerator_Reset_mA6287E3DC5B7D6B63CA2A7AB61D665046EB5F20A (void);
// 0x000000BB System.Object PW.OrderGenerator/<GenerateOrderRoutine>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateOrderRoutineU3Ed__11_System_Collections_IEnumerator_get_Current_mD2AA62DD3D224F86D1949357935499985C88E92C (void);
// 0x000000BC System.Void PW.PanGameObject::.ctor()
extern void PanGameObject__ctor_m51981FD29F760556759C19EA5E138089E2D6C0EC (void);
// 0x000000BD System.Void PW.PlayerSlots::OnEnable()
extern void PlayerSlots_OnEnable_m4C4CFFBB180D5F0E8A56FC49370C8C79D23111E9 (void);
// 0x000000BE System.Void PW.PlayerSlots::BasicGameEvents_onProductDeletedFromSlot(System.Int32)
extern void PlayerSlots_BasicGameEvents_onProductDeletedFromSlot_m257EE7310BE616BFF6A83DB344E5BEBE402F123B (void);
// 0x000000BF System.Void PW.PlayerSlots::BasicGameEvents_onProductAddedToSlot(System.Int32)
extern void PlayerSlots_BasicGameEvents_onProductAddedToSlot_mC9E61C79529FB30887C7FF4B2688C665421136C4 (void);
// 0x000000C0 System.Collections.IEnumerator PW.PlayerSlots::DoEmphasize(System.Int32)
extern void PlayerSlots_DoEmphasize_m21F266DB698F0AEA67E16AB0C31FF394B8CC340D (void);
// 0x000000C1 System.Void PW.PlayerSlots::OnDisable()
extern void PlayerSlots_OnDisable_m93BE4E18F5BA58758F1AC3A89C46115073391FD1 (void);
// 0x000000C2 System.Boolean PW.PlayerSlots::CanHoldItem(System.Int32)
extern void PlayerSlots_CanHoldItem_m61924EF3CC74127DB40E2EE8E1413A98782B9295 (void);
// 0x000000C3 System.Boolean PW.PlayerSlots::HoldsItem(System.Int32)
extern void PlayerSlots_HoldsItem_mADED3B2858DD66E3C268E0D8DB7A2255B5AC0836 (void);
// 0x000000C4 System.Void PW.PlayerSlots::.ctor()
extern void PlayerSlots__ctor_mE084FE0E502C68DC78E1A553C2682676A107A229 (void);
// 0x000000C5 System.Void PW.PlayerSlots/<DoEmphasize>d__6::.ctor(System.Int32)
extern void U3CDoEmphasizeU3Ed__6__ctor_m47F5E6CB605E6FDE17AC9AA92CEEBF2BE5C31A82 (void);
// 0x000000C6 System.Void PW.PlayerSlots/<DoEmphasize>d__6::System.IDisposable.Dispose()
extern void U3CDoEmphasizeU3Ed__6_System_IDisposable_Dispose_m172ACC62445FDAFD1FDBBF265080CD00BC2631AA (void);
// 0x000000C7 System.Boolean PW.PlayerSlots/<DoEmphasize>d__6::MoveNext()
extern void U3CDoEmphasizeU3Ed__6_MoveNext_m9C53E480F6988BE9C4734FBB9B2AAC83542D4001 (void);
// 0x000000C8 System.Object PW.PlayerSlots/<DoEmphasize>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoEmphasizeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEF6B1259A7A1F7B7F2729E46A9BDE69C3278468 (void);
// 0x000000C9 System.Void PW.PlayerSlots/<DoEmphasize>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDoEmphasizeU3Ed__6_System_Collections_IEnumerator_Reset_m1766D561AA5D3F587F7E13F00A5E71EF865C7AC5 (void);
// 0x000000CA System.Object PW.PlayerSlots/<DoEmphasize>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDoEmphasizeU3Ed__6_System_Collections_IEnumerator_get_Current_mDB83930F54F4FCD4593F8432B12F107E2B6711F2 (void);
// 0x000000CB System.Void PW.Product::.ctor(System.Int32,System.String,ProductType,System.Single)
extern void Product__ctor_m5647B0C74E26A1451203B6D5EC7498AB33A9F36A (void);
// 0x000000CC System.Collections.IEnumerator PW.ProductGameObject::AnimateGoingToSlot()
extern void ProductGameObject_AnimateGoingToSlot_m117A33A4E7AD26813AEB88647D632D9725D370E6 (void);
// 0x000000CD System.Boolean PW.ProductGameObject::CanGoPlayerSlot()
extern void ProductGameObject_CanGoPlayerSlot_m229CDC0878E97E795E1312A207EDF5D8AE18810F (void);
// 0x000000CE System.Collections.IEnumerator PW.ProductGameObject::MoveToPlace(UnityEngine.Vector3)
extern void ProductGameObject_MoveToPlace_mBA554E360AC594ACFCE88FC874B691A7C2801CE5 (void);
// 0x000000CF System.Void PW.ProductGameObject::.ctor()
extern void ProductGameObject__ctor_m76BC962FC16CD4F9A5D8756281127FD3C1B7EE41 (void);
// 0x000000D0 System.Void PW.ProductGameObject/<AnimateGoingToSlot>d__6::.ctor(System.Int32)
extern void U3CAnimateGoingToSlotU3Ed__6__ctor_m5FBFCD97E5B6FCBB42950667EC277741AB5D996C (void);
// 0x000000D1 System.Void PW.ProductGameObject/<AnimateGoingToSlot>d__6::System.IDisposable.Dispose()
extern void U3CAnimateGoingToSlotU3Ed__6_System_IDisposable_Dispose_m4E319A8D244DC00A12BC43EA9A79B06DD65125F1 (void);
// 0x000000D2 System.Boolean PW.ProductGameObject/<AnimateGoingToSlot>d__6::MoveNext()
extern void U3CAnimateGoingToSlotU3Ed__6_MoveNext_m7B90325EFAF4003277FC5D404D9EE610C3C4CD1F (void);
// 0x000000D3 System.Object PW.ProductGameObject/<AnimateGoingToSlot>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC5D01FE6024D3B3E902F33B36D1F6C7DBA0C080 (void);
// 0x000000D4 System.Void PW.ProductGameObject/<AnimateGoingToSlot>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimateGoingToSlotU3Ed__6_System_Collections_IEnumerator_Reset_m75198CC6DB8F330A8D0CD09192DD67BDD3B26221 (void);
// 0x000000D5 System.Object PW.ProductGameObject/<AnimateGoingToSlot>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__6_System_Collections_IEnumerator_get_Current_mBA1ACC13A9B786C4CEB383D6C0E6F9CA87D9F560 (void);
// 0x000000D6 System.Void PW.ProductGameObject/<MoveToPlace>d__8::.ctor(System.Int32)
extern void U3CMoveToPlaceU3Ed__8__ctor_m01576655B126086265192EE72C27558F232BC8B9 (void);
// 0x000000D7 System.Void PW.ProductGameObject/<MoveToPlace>d__8::System.IDisposable.Dispose()
extern void U3CMoveToPlaceU3Ed__8_System_IDisposable_Dispose_mE4F35501BBCD480FE86167C6C347F13BB3BB7C59 (void);
// 0x000000D8 System.Boolean PW.ProductGameObject/<MoveToPlace>d__8::MoveNext()
extern void U3CMoveToPlaceU3Ed__8_MoveNext_m497E90157E14A8CB01612E8072684E9D123510AF (void);
// 0x000000D9 System.Object PW.ProductGameObject/<MoveToPlace>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveToPlaceU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE243D65C6D04449E63C62F0E6DBFFD8F0BB69ABC (void);
// 0x000000DA System.Void PW.ProductGameObject/<MoveToPlace>d__8::System.Collections.IEnumerator.Reset()
extern void U3CMoveToPlaceU3Ed__8_System_Collections_IEnumerator_Reset_m41AD8AE3E01C10A67DAEB596796A93A1FCF40051 (void);
// 0x000000DB System.Object PW.ProductGameObject/<MoveToPlace>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CMoveToPlaceU3Ed__8_System_Collections_IEnumerator_get_Current_m950C3C5728C3C3AB2DFA46A6E2C0DAC6554B0875 (void);
// 0x000000DC System.Void PW.ProductManager::InstantiatePlaceHolder(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.GameObject)
extern void ProductManager_InstantiatePlaceHolder_m435F9737C238595DF18EC7BB2B527136CECD614A (void);
// 0x000000DD System.Void PW.ProductManager::OnEnable()
extern void ProductManager_OnEnable_m7C5DEAD34B8285582E7C7CA97BA00068570E41FB (void);
// 0x000000DE System.Void PW.ProductManager::OnDisable()
extern void ProductManager_OnDisable_m53B70DEA6E030E4000A0BEF68A8C8D136E11556C (void);
// 0x000000DF System.Boolean PW.ProductManager::RemoveProduct(System.Int32)
extern void ProductManager_RemoveProduct_mD5D04AE35D06BF9DB7528691BEBA67D4F2C9AD26 (void);
// 0x000000E0 System.Void PW.ProductManager::.ctor()
extern void ProductManager__ctor_m16ECC2B795D688D82BEC444EC8266CC1F2379D14 (void);
// 0x000000E1 System.Void PW.ProductManager/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m2F3C6325773FC7FAB353B9DA8299FB5BA3251EB5 (void);
// 0x000000E2 System.Boolean PW.ProductManager/<>c__DisplayClass6_0::<RemoveProduct>b__0(PW.Product)
extern void U3CU3Ec__DisplayClass6_0_U3CRemoveProductU3Eb__0_m04D3F4ECF8A1810775F6F84E1B9E9B237F5C05BB (void);
// 0x000000E3 System.Void PW.ProductWithCover::OnEnable()
extern void ProductWithCover_OnEnable_m9E312BC420860043C98C79CF952AD94A33E7F4A6 (void);
// 0x000000E4 System.Void PW.ProductWithCover::HandleCoverCloseClick()
extern void ProductWithCover_HandleCoverCloseClick_m64F60F104B4F4FB965CEB21D6267DCDFF199C6DC (void);
// 0x000000E5 System.Void PW.ProductWithCover::OnMouseDown()
extern void ProductWithCover_OnMouseDown_mE32486CBB85F8510C948F4075AEDB222C51DB38E (void);
// 0x000000E6 System.Collections.IEnumerator PW.ProductWithCover::OpenCloseDisplay(System.Boolean,System.Boolean)
extern void ProductWithCover_OpenCloseDisplay_m6DBB4D15F3CCB847184FE08FF3FBEA370AB7F050 (void);
// 0x000000E7 System.Void PW.ProductWithCover::.ctor()
extern void ProductWithCover__ctor_m4FCBE9E9EC27B5CDAB7D70F4C1F10DC3142B7183 (void);
// 0x000000E8 System.Void PW.ProductWithCover/<OpenCloseDisplay>d__9::.ctor(System.Int32)
extern void U3COpenCloseDisplayU3Ed__9__ctor_m1A4341FA641B7472C06FD095560628D69D4992CF (void);
// 0x000000E9 System.Void PW.ProductWithCover/<OpenCloseDisplay>d__9::System.IDisposable.Dispose()
extern void U3COpenCloseDisplayU3Ed__9_System_IDisposable_Dispose_mB6AFE25DEF3FD1764EF7162DBB65E558BD2956C7 (void);
// 0x000000EA System.Boolean PW.ProductWithCover/<OpenCloseDisplay>d__9::MoveNext()
extern void U3COpenCloseDisplayU3Ed__9_MoveNext_m4D3D425235D8BD9C19FA5F407CB392C411980FB6 (void);
// 0x000000EB System.Object PW.ProductWithCover/<OpenCloseDisplay>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COpenCloseDisplayU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2C2A4B4FFD46C93D601FB195C0DD00C1B714EDF (void);
// 0x000000EC System.Void PW.ProductWithCover/<OpenCloseDisplay>d__9::System.Collections.IEnumerator.Reset()
extern void U3COpenCloseDisplayU3Ed__9_System_Collections_IEnumerator_Reset_m1889E541D87F29D4A212D20800502374ABC2C5A3 (void);
// 0x000000ED System.Object PW.ProductWithCover/<OpenCloseDisplay>d__9::System.Collections.IEnumerator.get_Current()
extern void U3COpenCloseDisplayU3Ed__9_System_Collections_IEnumerator_get_Current_mA2081CC28629488E1E109471FCFB2AE3E044A5BC (void);
// 0x000000EE System.Void PW.ProgressHelper::UpdateProcessUI(System.Single,System.Single)
extern void ProgressHelper_UpdateProcessUI_mF8B00E2038C279D8952A794DDE1B51E0EFF03BA5 (void);
// 0x000000EF System.Void PW.ProgressHelper::ToggleHelper(System.Boolean)
extern void ProgressHelper_ToggleHelper_m38701100C1E8DB89D517DB5C10E228D3C238E3EA (void);
// 0x000000F0 System.Void PW.ProgressHelper::.ctor()
extern void ProgressHelper__ctor_mF7C45D2595C04F230DB8553C903E1DFA150D8200 (void);
// 0x000000F1 System.Void PW.ReadyToServe::Awake()
extern void ReadyToServe_Awake_m4F566975C4653D8686E0DFEE34ACAB809ACD1C96 (void);
// 0x000000F2 System.Void PW.ReadyToServe::OnMouseDown()
extern void ReadyToServe_OnMouseDown_mB04EE72C791D73F6E00858447019078F63BBF692 (void);
// 0x000000F3 System.Collections.IEnumerator PW.ReadyToServe::AnimateGoingToSlot()
extern void ReadyToServe_AnimateGoingToSlot_m0F3ACECF0313BA019C3B1BAD7BB41CF614B80EDA (void);
// 0x000000F4 System.Void PW.ReadyToServe::.ctor()
extern void ReadyToServe__ctor_mAE30808AFDEA56FB2222DC8B8DED7D34D650B2E8 (void);
// 0x000000F5 System.Collections.IEnumerator PW.ReadyToServe::<>n__0()
extern void ReadyToServe_U3CU3En__0_mEB7F8A17472A9E93E040DB70DEBBBBFF619407F5 (void);
// 0x000000F6 System.Void PW.ReadyToServe/<AnimateGoingToSlot>d__4::.ctor(System.Int32)
extern void U3CAnimateGoingToSlotU3Ed__4__ctor_m5460DF2427802D1E72E4EE164B25C8EBE31B67BF (void);
// 0x000000F7 System.Void PW.ReadyToServe/<AnimateGoingToSlot>d__4::System.IDisposable.Dispose()
extern void U3CAnimateGoingToSlotU3Ed__4_System_IDisposable_Dispose_m0732F9E25892ACFBD974573453F4F6982FC9024D (void);
// 0x000000F8 System.Boolean PW.ReadyToServe/<AnimateGoingToSlot>d__4::MoveNext()
extern void U3CAnimateGoingToSlotU3Ed__4_MoveNext_m1EC3A87ACE589FA40B27AA77E4C9D84B574DB9D4 (void);
// 0x000000F9 System.Object PW.ReadyToServe/<AnimateGoingToSlot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1733903575DC6B12AC472ECF868C8A56506E688 (void);
// 0x000000FA System.Void PW.ReadyToServe/<AnimateGoingToSlot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CAnimateGoingToSlotU3Ed__4_System_Collections_IEnumerator_Reset_mCBFBB6AF12B01D67891781827C02EB9EE3D0E095 (void);
// 0x000000FB System.Object PW.ReadyToServe/<AnimateGoingToSlot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateGoingToSlotU3Ed__4_System_Collections_IEnumerator_get_Current_m9A01F59D52712803F122835F61192CD5FA21EC60 (void);
// 0x000000FC System.Void PW.ServeOrder::ServeMe()
extern void ServeOrder_ServeMe_mBF3AF682543FE9E13A39D9CD06C018C376200140 (void);
// 0x000000FD System.Void PW.ServeOrder::OrderCompleted()
extern void ServeOrder_OrderCompleted_mE97748C8F780C1B3B46BD0EEFDACBCE1E893BBF7 (void);
// 0x000000FE System.Collections.IEnumerator PW.ServeOrder::DoEmphasize()
extern void ServeOrder_DoEmphasize_mF4B0036684A2AFFA2A6001D6CAD1F2B732C3614E (void);
// 0x000000FF System.Void PW.ServeOrder::SetOrder(System.Int32,System.Single)
extern void ServeOrder_SetOrder_m5840AF15E45156B63DB631F74072B3F64C293110 (void);
// 0x00000100 System.Void PW.ServeOrder::SetSprite(UnityEngine.Sprite)
extern void ServeOrder_SetSprite_mAF225D0245B68C781F3EE8D81CA9F6A26B5679CF (void);
// 0x00000101 System.Void PW.ServeOrder::Update()
extern void ServeOrder_Update_mD2C2EE6E121750165A1F65E210BA8205EAF872BC (void);
// 0x00000102 System.Void PW.ServeOrder::CancelOrder()
extern void ServeOrder_CancelOrder_m5A5FDA12C6B6615D4F1816B89667F6CC4E0671D5 (void);
// 0x00000103 System.Void PW.ServeOrder::.ctor()
extern void ServeOrder__ctor_mE87E4FA667474321B80B0ED5E60D077DC74DA153 (void);
// 0x00000104 System.Void PW.ServeOrder/<DoEmphasize>d__9::.ctor(System.Int32)
extern void U3CDoEmphasizeU3Ed__9__ctor_m4ECB7CDF7C1F55E4481C977AF7E0CD1E4F490783 (void);
// 0x00000105 System.Void PW.ServeOrder/<DoEmphasize>d__9::System.IDisposable.Dispose()
extern void U3CDoEmphasizeU3Ed__9_System_IDisposable_Dispose_mDFA6F3DBC548AC3C44B04450C76E6210D1ED4B9A (void);
// 0x00000106 System.Boolean PW.ServeOrder/<DoEmphasize>d__9::MoveNext()
extern void U3CDoEmphasizeU3Ed__9_MoveNext_m6EF8EBE47F79014C77EF74731DBC2347310F61A9 (void);
// 0x00000107 System.Object PW.ServeOrder/<DoEmphasize>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoEmphasizeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB74B6175EF27207C707CE720527CC6D54F83A035 (void);
// 0x00000108 System.Void PW.ServeOrder/<DoEmphasize>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDoEmphasizeU3Ed__9_System_Collections_IEnumerator_Reset_mE041ADE182384DA5E9124EF16BFB74069DDBB56E (void);
// 0x00000109 System.Object PW.ServeOrder/<DoEmphasize>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDoEmphasizeU3Ed__9_System_Collections_IEnumerator_get_Current_mDE8F62E974A87EDC67452E8F86FB0CFC33620566 (void);
// 0x0000010A System.Void PW.StoveGameObject::DoDoorAnimationsIfNeeded()
extern void StoveGameObject_DoDoorAnimationsIfNeeded_mF7AF29FAD713721DAD2F701E9468941B809F393F (void);
// 0x0000010B System.Void PW.StoveGameObject::StartCooking(PW.CookableProduct)
extern void StoveGameObject_StartCooking_m824D97312F26FF200776D142D9402D56854C524F (void);
// 0x0000010C System.Collections.IEnumerator PW.StoveGameObject::PlayDoorAnim(System.Boolean,System.Boolean)
extern void StoveGameObject_PlayDoorAnim_m727447CC6B1000FEB4A4DF2C59FC01F525D20A20 (void);
// 0x0000010D System.Void PW.StoveGameObject::OnMouseDown()
extern void StoveGameObject_OnMouseDown_m624AF76ED2C442A977D912A3B78CBF29C6AC5D09 (void);
// 0x0000010E System.Void PW.StoveGameObject::.ctor()
extern void StoveGameObject__ctor_mBD5FE2677B66D1B342388680E4C6EF4572B21C26 (void);
// 0x0000010F System.Void PW.StoveGameObject/<PlayDoorAnim>d__6::.ctor(System.Int32)
extern void U3CPlayDoorAnimU3Ed__6__ctor_m73CD25431474B87FE24908859F89355F82A49A99 (void);
// 0x00000110 System.Void PW.StoveGameObject/<PlayDoorAnim>d__6::System.IDisposable.Dispose()
extern void U3CPlayDoorAnimU3Ed__6_System_IDisposable_Dispose_m83DAC6CC43BC1C2F8FE99D4F8982653BD81472DA (void);
// 0x00000111 System.Boolean PW.StoveGameObject/<PlayDoorAnim>d__6::MoveNext()
extern void U3CPlayDoorAnimU3Ed__6_MoveNext_m3808DAE84402D229590274037E335C3EC60B8B8F (void);
// 0x00000112 System.Object PW.StoveGameObject/<PlayDoorAnim>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayDoorAnimU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF55B0B53AA06655DC68155F11D95B1424EA45D75 (void);
// 0x00000113 System.Void PW.StoveGameObject/<PlayDoorAnim>d__6::System.Collections.IEnumerator.Reset()
extern void U3CPlayDoorAnimU3Ed__6_System_Collections_IEnumerator_Reset_m07FD0F8134565B4AB799F96F3D8CCFE9CB464FF2 (void);
// 0x00000114 System.Object PW.StoveGameObject/<PlayDoorAnim>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CPlayDoorAnimU3Ed__6_System_Collections_IEnumerator_get_Current_m236A2BE70F5D1032BE95557DD601CAF447FE701F (void);
// 0x00000115 System.Void PW.BasicCameraControl::Update()
extern void BasicCameraControl_Update_m3EDB55A0971045938B92E70EEDE61445BFD6AB26 (void);
// 0x00000116 System.Void PW.BasicCameraControl::LateUpdate()
extern void BasicCameraControl_LateUpdate_mB862A9C300B781C2CCB0FD511C91B07AE98A34E4 (void);
// 0x00000117 System.Void PW.BasicCameraControl::.ctor()
extern void BasicCameraControl__ctor_m9CFF652C274B4BD867EF139ABD57CE23D28517DA (void);
// 0x00000118 System.Void PW.BasicGameEvents::add_onOrderCancelled(PW.BasicGameEvents/OnOrderCancelled)
extern void BasicGameEvents_add_onOrderCancelled_m38509E2701FC60C1F728AA68F37030EECBC5D93E (void);
// 0x00000119 System.Void PW.BasicGameEvents::remove_onOrderCancelled(PW.BasicGameEvents/OnOrderCancelled)
extern void BasicGameEvents_remove_onOrderCancelled_m4F0FC7F85E2B4A20A6E1233F3DC90984AA507765 (void);
// 0x0000011A System.Void PW.BasicGameEvents::RaiseOnOrderCancelled(System.Int32)
extern void BasicGameEvents_RaiseOnOrderCancelled_mA92233A4E1FA7331707E9BDAA18DE58380698EA7 (void);
// 0x0000011B System.Void PW.BasicGameEvents::add_onOrderCompleted(PW.BasicGameEvents/OnOrderCompleted)
extern void BasicGameEvents_add_onOrderCompleted_mB5789734C8152A8629E52C1DC59C8BC8D64C367F (void);
// 0x0000011C System.Void PW.BasicGameEvents::remove_onOrderCompleted(PW.BasicGameEvents/OnOrderCompleted)
extern void BasicGameEvents_remove_onOrderCompleted_m99980DC1CEF7B6F38F560EE79C00E476CD12DB9C (void);
// 0x0000011D System.Void PW.BasicGameEvents::RaiseOnOrderCompleted(System.Int32,System.Single)
extern void BasicGameEvents_RaiseOnOrderCompleted_mF29A8772748E7A07BAD7505AB14A3C6E36DED2ED (void);
// 0x0000011E System.Void PW.BasicGameEvents::add_onProductAddedToSlot(PW.BasicGameEvents/OnProductAddedToSlot)
extern void BasicGameEvents_add_onProductAddedToSlot_m7A95E79212287B2A53D3B29354111709F581AA28 (void);
// 0x0000011F System.Void PW.BasicGameEvents::remove_onProductAddedToSlot(PW.BasicGameEvents/OnProductAddedToSlot)
extern void BasicGameEvents_remove_onProductAddedToSlot_m78BDF72AAC02D690B7B0201B4F5F7B1FCE26184D (void);
// 0x00000120 System.Void PW.BasicGameEvents::RaiseOnProductAddedToSlot(System.Int32)
extern void BasicGameEvents_RaiseOnProductAddedToSlot_m40CEC2748EF5B29A546BC9B214BAD678538D6338 (void);
// 0x00000121 System.Void PW.BasicGameEvents::add_onProductDeletedFromSlot(PW.BasicGameEvents/OnProductDeletedFromSlot)
extern void BasicGameEvents_add_onProductDeletedFromSlot_mDF102E17AAC799C986D821E156521221D44C27D3 (void);
// 0x00000122 System.Void PW.BasicGameEvents::remove_onProductDeletedFromSlot(PW.BasicGameEvents/OnProductDeletedFromSlot)
extern void BasicGameEvents_remove_onProductDeletedFromSlot_m089E2179B14FA531B9A7C13DE9F5686704E03917 (void);
// 0x00000123 System.Void PW.BasicGameEvents::RaiseOnProductDeletedFromSlot(System.Int32)
extern void BasicGameEvents_RaiseOnProductDeletedFromSlot_mEBDB08BFC5C0C000A30D764769D48B60006139DF (void);
// 0x00000124 System.Void PW.BasicGameEvents::add_onPlaceHolderRequired(PW.BasicGameEvents/OnPlaceHolderRequired)
extern void BasicGameEvents_add_onPlaceHolderRequired_mAC31AE207AA792B5F6CCE028A34A70CEDFF5C235 (void);
// 0x00000125 System.Void PW.BasicGameEvents::remove_onPlaceHolderRequired(PW.BasicGameEvents/OnPlaceHolderRequired)
extern void BasicGameEvents_remove_onPlaceHolderRequired_m9452F0B44B3FCCD76CD4CB9B3B1391A5E3CF415D (void);
// 0x00000126 System.Void PW.BasicGameEvents::RaiseInstantiatePlaceHolder(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.GameObject)
extern void BasicGameEvents_RaiseInstantiatePlaceHolder_mD21089052205D67F4985B0F8B64AFD0378781C4F (void);
// 0x00000127 System.Void PW.BasicGameEvents::.ctor()
extern void BasicGameEvents__ctor_m02376F6DEDAA4620349247E93549669A64DA6F26 (void);
// 0x00000128 System.Void PW.BasicGameEvents/OnOrderCancelled::.ctor(System.Object,System.IntPtr)
extern void OnOrderCancelled__ctor_mF032F851C8585E69E27ED7A7850C4932B9A646B4 (void);
// 0x00000129 System.Void PW.BasicGameEvents/OnOrderCancelled::Invoke(System.Int32)
extern void OnOrderCancelled_Invoke_m155453EE081E04ECDCE766A95CEF163771D348F5 (void);
// 0x0000012A System.IAsyncResult PW.BasicGameEvents/OnOrderCancelled::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void OnOrderCancelled_BeginInvoke_mB430EDFF54E9D9C276C8E9E818539C0BDBA18B4F (void);
// 0x0000012B System.Void PW.BasicGameEvents/OnOrderCancelled::EndInvoke(System.IAsyncResult)
extern void OnOrderCancelled_EndInvoke_m43A17E4635CB5043A78475264EFA150960D6CEDE (void);
// 0x0000012C System.Void PW.BasicGameEvents/OnOrderCompleted::.ctor(System.Object,System.IntPtr)
extern void OnOrderCompleted__ctor_m03B5422CAC165D6B4737FDA68344B3B2E790EF92 (void);
// 0x0000012D System.Void PW.BasicGameEvents/OnOrderCompleted::Invoke(System.Int32,System.Single)
extern void OnOrderCompleted_Invoke_m551F4DC241C943D63B3F16E19C2C680A360E03D0 (void);
// 0x0000012E System.IAsyncResult PW.BasicGameEvents/OnOrderCompleted::BeginInvoke(System.Int32,System.Single,System.AsyncCallback,System.Object)
extern void OnOrderCompleted_BeginInvoke_m447A12F1D9E0D5D520541476B68B7C8E3F0DFA6B (void);
// 0x0000012F System.Void PW.BasicGameEvents/OnOrderCompleted::EndInvoke(System.IAsyncResult)
extern void OnOrderCompleted_EndInvoke_m560667A0F3D665EA0940D03FBFFA071D6340C110 (void);
// 0x00000130 System.Void PW.BasicGameEvents/OnProductAddedToSlot::.ctor(System.Object,System.IntPtr)
extern void OnProductAddedToSlot__ctor_mFB35C4601A92249E50E1B7119D9E805F6D08699B (void);
// 0x00000131 System.Void PW.BasicGameEvents/OnProductAddedToSlot::Invoke(System.Int32)
extern void OnProductAddedToSlot_Invoke_m0CA5576864E5B068069E25567E4320468CC5F472 (void);
// 0x00000132 System.IAsyncResult PW.BasicGameEvents/OnProductAddedToSlot::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void OnProductAddedToSlot_BeginInvoke_m928D2B4C211D24036612BFE32ADCA9663C545C6B (void);
// 0x00000133 System.Void PW.BasicGameEvents/OnProductAddedToSlot::EndInvoke(System.IAsyncResult)
extern void OnProductAddedToSlot_EndInvoke_mE23636E6E4885A690FD0530F25672F3CC35033EC (void);
// 0x00000134 System.Void PW.BasicGameEvents/OnProductDeletedFromSlot::.ctor(System.Object,System.IntPtr)
extern void OnProductDeletedFromSlot__ctor_mDF8370623788671109BF3E133329AF55A5F03F14 (void);
// 0x00000135 System.Void PW.BasicGameEvents/OnProductDeletedFromSlot::Invoke(System.Int32)
extern void OnProductDeletedFromSlot_Invoke_m5317593E62E4633443A46CC41E48149BEE9EC62C (void);
// 0x00000136 System.IAsyncResult PW.BasicGameEvents/OnProductDeletedFromSlot::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void OnProductDeletedFromSlot_BeginInvoke_m69CC86787C55B32C575E54AFBC949406E14C8459 (void);
// 0x00000137 System.Void PW.BasicGameEvents/OnProductDeletedFromSlot::EndInvoke(System.IAsyncResult)
extern void OnProductDeletedFromSlot_EndInvoke_m397399989A72C1F973CC67A9CECA289A5AD8BDE3 (void);
// 0x00000138 System.Void PW.BasicGameEvents/OnPlaceHolderRequired::.ctor(System.Object,System.IntPtr)
extern void OnPlaceHolderRequired__ctor_mCA70F750633D199CAEBB4B9967DD416BEC2EC803 (void);
// 0x00000139 System.Void PW.BasicGameEvents/OnPlaceHolderRequired::Invoke(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.GameObject)
extern void OnPlaceHolderRequired_Invoke_m01376F4744A488C163BDF3E915726C4B0C47388B (void);
// 0x0000013A System.IAsyncResult PW.BasicGameEvents/OnPlaceHolderRequired::BeginInvoke(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void OnPlaceHolderRequired_BeginInvoke_mF60F811D7EB81A0AA70A814431E64B211D8F11D4 (void);
// 0x0000013B System.Void PW.BasicGameEvents/OnPlaceHolderRequired::EndInvoke(System.IAsyncResult)
extern void OnPlaceHolderRequired_EndInvoke_m75DA679C1B98A7EC86E86163B36A4AAAE1B4CA6C (void);
// 0x0000013C System.Void PW.SliceHelper::.ctor()
extern void SliceHelper__ctor_m545D86008EC90C178914453F94BA793B7A7FD0E3 (void);
static Il2CppMethodPointer s_methodPointers[316] = 
{
	MouseCamLook_Start_m0CE26867CD828A1CBB413712934BEC98E3B2733C,
	MouseCamLook_Update_m23878426B4D0D0E665CE2748EC3ECBFFF27B6875,
	MouseCamLook__ctor_m0A7DAEE06B37C79F4F7C1E9F457180E08C2E31A2,
	PlayerMovement_Start_m83FD44DCA324CE3D05A71FD2E2991FCD743F003A,
	PlayerMovement_Update_m5BB6CE35AF68EE00CFEB4BA5EBA17E10667551D3,
	PlayerMovement__ctor_mB37559C5B0638161878D20E00B7C672FC38BBBAA,
	ScreenShotHelper_TakeScreenShot_m67E4683B1C8E309EBF6803FCD8A86CA9EE2089FC,
	ScreenShotHelper_TakeSingleScreenShot_m4CC2A7B42AAB9D895ACE969D828692E807D96F9B,
	ScreenShotHelper_PositionCamAndGetScreenTexture_m4F65B4E1ACE9887818E61FC1550B76D7E853BA0D,
	ScreenShotHelper__ctor_mE54CB23495E683FA01CE6B01FBAED6BB4E7CEE0C,
	TransformExtensions_EncapsulateBounds_mA6D6847BE9864627D4EB19DEA9D94607C200892F,
	SimpleCameraController_OnEnable_mE82248046F093F6C5640BDCC8FF4B9996C6D2381,
	SimpleCameraController_GetInputTranslationDirection_mB7EE1B0B3331A6CC8F52D5671A504D15A4ECB558,
	SimpleCameraController_Update_mA30F5F5EC736E8A3B298CF87F9A786F6D7E2CB1D,
	SimpleCameraController__ctor_m094201165462FC27919DE9E98C4D53A061197148,
	CameraState_SetFromTransform_mCC26B175D14198A00A2121289F2B3504FEDAC157,
	CameraState_Translate_m0EF16D858AC07E47E11EAA3AD0D856DEB282B02E,
	CameraState_LerpTowards_m59D846E2B246B06ACE4CF5F0AB68C3A2B3AF6213,
	CameraState_UpdateTransform_m8174E7238F01C8A72D7793F7A850C8E904190084,
	CameraState__ctor_m93055CACFEBEBA19E2BD9AF02AF66C66CF56D98F,
	BewerageMaker_Start_m5D44071C661DAB916F6E9F9A2DDEE9CB414E2E6C,
	BewerageMaker_OnMouseUp_mF5396AB3D4D7F284ED241EC359E5D3FB6EF0743B,
	BewerageMaker_StartFillingStep_mFD73429142BA8A4F403F84BFCCC5E893C17ECC35,
	BewerageMaker_StartPreFillAnimationState_mD1B1A81A93DEFE1A630A01F9FC8AF9AF24A0142A,
	BewerageMaker_DoPreFill_m9E317793ABAEAE084FEA105DD83ADB7C056DFF35,
	BewerageMaker_DoFillAnimation_m98D9347CF6C09352B73E41BADBAB1390DDCC9CD5,
	BewerageMaker_OnFillEnded_mF4ADA5992C7FF2191F322150FBE371C91D53E62D,
	BewerageMaker_SetTheCup_mB105C745E13E2E8E4159ECAE3DFF442B838A2B9A,
	BewerageMaker_UnSetTheCup_mC2174A084A6FDDD81961B8BD4C93EC666016BE4C,
	BewerageMaker_DoFillEnded_mC018A1215E2E1187E4E0F69818C2851787B06B73,
	BewerageMaker__ctor_m67575D863733452BCA856FEFEFF08833491607AD,
	U3CDoPreFillU3Ed__22__ctor_m2CCF453D35D403BB326EAD5B29E3B409EE4EF240,
	U3CDoPreFillU3Ed__22_System_IDisposable_Dispose_m5A5D26E9FE3DC2A6944C9023F215309BEB83FAA2,
	U3CDoPreFillU3Ed__22_MoveNext_m48DFC618B84811E621BA2B2B19035BAA2A3CF6E3,
	U3CDoPreFillU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB69CB79B25F1A3F3234915A278EA9038877EE8,
	U3CDoPreFillU3Ed__22_System_Collections_IEnumerator_Reset_m56E15442839DA9FFC484EFA1AC32ECB2C9789F17,
	U3CDoPreFillU3Ed__22_System_Collections_IEnumerator_get_Current_m3FADC58F5E631644909FB08D0FC8EBDCAF0E33F1,
	U3CDoFillAnimationU3Ed__23__ctor_mF31A8754803121151C0F709EC129C41093D35376,
	U3CDoFillAnimationU3Ed__23_System_IDisposable_Dispose_mFD6E1DBBB118EC1775FBE7E30B9DC58D7548B64B,
	U3CDoFillAnimationU3Ed__23_MoveNext_mFE845050E123AFBE705A869D062FE0CBD6AD273D,
	U3CDoFillAnimationU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29010EF7132A841816272D536822E98AA2646AF8,
	U3CDoFillAnimationU3Ed__23_System_Collections_IEnumerator_Reset_mAAE3BB1AB3CC18037867B61C3CBF326152310CEC,
	U3CDoFillAnimationU3Ed__23_System_Collections_IEnumerator_get_Current_m65263ACC665024966B3DEFA2D52039A61838E692,
	U3CDoFillEndedU3Ed__27__ctor_m12BC4C8DAC5B2D461FA5B7F07D9B8A90F86E570A,
	U3CDoFillEndedU3Ed__27_System_IDisposable_Dispose_m7C18B2D7A6F8D0584C2A63A0FB22CEADB1448E2F,
	U3CDoFillEndedU3Ed__27_MoveNext_mAD9E6E3D4C91315967DF8D4CB525B19D568684EB,
	U3CDoFillEndedU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2217288F2FED4BE1E79762096CEC25C46C88A9C0,
	U3CDoFillEndedU3Ed__27_System_Collections_IEnumerator_Reset_mF6E3BA4680E4B827B067715DAF7ED5C40AFA0821,
	U3CDoFillEndedU3Ed__27_System_Collections_IEnumerator_get_Current_m410D676E9B95F62AF9AC3A8F3020306FC528DCFF,
	CookableProduct_Awake_m7E52E723301EC2AEFEF64EEC6F0C20E65A965B9E,
	CookableProduct_Start_mFF014D7E10DB320A2C16E24BD862EB02D08A4429,
	CookableProduct_OnEnable_m1A7FA71FE7B92CE79CF84C03F05BD01A449ECF0B,
	CookableProduct_OnMouseDown_m2A2351AE1E5FDC6950C2FA5755E5A2C70314ED1C,
	CookableProduct_MoveToPlace_m920C68A6D5532C7774561309E52952322F7D4E05,
	CookableProduct_DoneCooking_m7827D283E6169AB4B193307CAE2A1F195A5E5ED6,
	CookableProduct_CanGoPlayerSlot_m348354C9C878BFB8680079D36480F5DB4C7C56F0,
	CookableProduct_AnimateGoingToSlot_m201A519A0F7D51500E24561C229D2FF257FD17B0,
	CookableProduct__ctor_mBC45BD9CCF71DE1E495674D712B953AF56519D7A,
	CookableProduct_U3CU3En__0_m9C5125D13F07C66DF19D71CE66FB5B4EB66BFBB9,
	CookableProduct_U3CU3En__1_m10A3A6B4272585CD94882169939F3D33C5DDBD96,
	U3CMoveToPlaceU3Ed__10__ctor_mC55673548E84392B1B530512E079018DE2FE5701,
	U3CMoveToPlaceU3Ed__10_System_IDisposable_Dispose_m0E5D06245D9E330897DD6C8D0126006F51DFC6F3,
	U3CMoveToPlaceU3Ed__10_MoveNext_m3C9E195CE1384D6C56A5EBD067189EA44BC44B5A,
	U3CMoveToPlaceU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB23A2971E30C793924B37BD1687A47DEF81019,
	U3CMoveToPlaceU3Ed__10_System_Collections_IEnumerator_Reset_m41249B60A3F337A9819839BD8610BA988BADD499,
	U3CMoveToPlaceU3Ed__10_System_Collections_IEnumerator_get_Current_m16A783832AF9EB8009859552CD092AC0015487A2,
	U3CAnimateGoingToSlotU3Ed__13__ctor_m29812D63A58CA74C5DCFBF05EF414E5847B39E53,
	U3CAnimateGoingToSlotU3Ed__13_System_IDisposable_Dispose_m3650EED8EBA5388BB154D9735BEBEBB08F7C87EE,
	U3CAnimateGoingToSlotU3Ed__13_MoveNext_m67246D8E9025516A11C6509DE4B70CAFD4B35793,
	U3CAnimateGoingToSlotU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4EE558D306ED76975C4F2531960645E60DA51FB,
	U3CAnimateGoingToSlotU3Ed__13_System_Collections_IEnumerator_Reset_m985BE789A58FFAD9A4D9D4871B97F0B3326FBA7A,
	U3CAnimateGoingToSlotU3Ed__13_System_Collections_IEnumerator_get_Current_mB747FB2C2F789A01E511E763C47D36464F7AEAF9,
	CookingGameObject_Awake_m42DE51A159764A79B3A3A1D757ED44AF9A07789D,
	CookingGameObject_Start_mC5BD2EC3DE36A6A23837095DDFBA3B2A3085F0BC,
	CookingGameObject_GetCookingPosition_m0E90FA7E66C116321D0934C16A8AF2BA9B262A7E,
	CookingGameObject_DoDoorAnimationsIfNeeded_mAE0C5623664A8ADAF52194835866E98092BA66EA,
	CookingGameObject_IsEmpty_m77054C4C7D65A1CFC8B0D51DDD311CBCC88A2EF7,
	CookingGameObject_StartCooking_m2E3F2CF0111C7736EBD1565F921D80EFB892365D,
	CookingGameObject_ReadyToServe_m89116C0838B818583F7542C53F3E3A78C74450BE,
	CookingGameObject_HasStartAnimationPos_m9EBBB5672D510B7276361C3AF29E721E4FF4A118,
	CookingGameObject_Cooking_mD0CF94DB451C9BC02ECC48525CE7E0C9B45C5B49,
	CookingGameObject_OnMouseDown_mB1666607191D2A29798281FFDFABD127A506E8FD,
	CookingGameObject__ctor_m1396236A333A207991B2F18B5D1015C6CA2AABA8,
	U3CCookingU3Ed__16__ctor_m4DC56D9EAF4C03B52FD7E94F4C2D1C9CA00A748C,
	U3CCookingU3Ed__16_System_IDisposable_Dispose_mD1F0ED9D4121157F52D4CF34CF4E76B246184CC0,
	U3CCookingU3Ed__16_MoveNext_m109A11AA999071DF83D34CD68AD2D1B992F69227,
	U3CCookingU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m936BA20BD362E9BE19A3FAB0F8B153FA1E96F19E,
	U3CCookingU3Ed__16_System_Collections_IEnumerator_Reset_m6F9C3719E88C0AC5CCCD96F7DEEE8EA8E6C26D1D,
	U3CCookingU3Ed__16_System_Collections_IEnumerator_get_Current_m2D8FA3BD1C1002EA90106503166C8FC7ABD6FAB0,
	CreateProductOnPlaceHolder_OnMouseDown_m2C0269002F949FAC1031587AC618D471E60E979D,
	CreateProductOnPlaceHolder__ctor_m6578F0B8702C18E29707B0E238D32826E562BED7,
	DeleteProductOnSlot_Start_m8E643735A01EFF30FCDA6B7E3294D1C6AF69C4C0,
	DeleteProductOnSlot_ToggleDeleteMode_m08838505D890F295C7A8E5F846E5533BDFD3E297,
	DeleteProductOnSlot_ChangeUI_m1A7965B9D14C5C8867B68CD491E0F2AE59D7B3AC,
	DeleteProductOnSlot_DeletePlayerSlotImage_m41A22BA4A70B93E26EDFD4F019FA17C2A6F3ACEB,
	DeleteProductOnSlot_OnPointerClick_m75B670817B4C1019F2E82DAA10332A2D49EDD4F1,
	DeleteProductOnSlot__ctor_m334E5890C72B8C02F77D8096473E922E63881E35,
	DeleteProductOnSlot_U3CStartU3Eb__3_0_m4E7D1D3F708B7F71789A5B35DD01248013B68ACC,
	DrinkableProduct_AnimateGoingToSlot_mA4CA6EA84F9F55992C257602B7E16E8DB229897E,
	DrinkableProduct__ctor_mB0A8BA3A9812812A3A253D4D0FE22C0211A64FD6,
	DrinkableProduct_U3CU3En__0_m2084F9553CF4FCBDF3F7C219CCDA74FB932DF292,
	U3CAnimateGoingToSlotU3Ed__0__ctor_mEFE8F2680FFF27D8BB3372EDAC22F190AA97716A,
	U3CAnimateGoingToSlotU3Ed__0_System_IDisposable_Dispose_m4C77151BC22656E7EA52279D6E6BE376C5E97459,
	U3CAnimateGoingToSlotU3Ed__0_MoveNext_m0C490926B98A68CA3C60A1553DE2EB9EA7166FAE,
	U3CAnimateGoingToSlotU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC302E0CCA0E3EF891EDA5A877A7E192571967C3,
	U3CAnimateGoingToSlotU3Ed__0_System_Collections_IEnumerator_Reset_m08A2AB6C674698D177F54867DBA5AE0B0E27B58E,
	U3CAnimateGoingToSlotU3Ed__0_System_Collections_IEnumerator_get_Current_mEF17E9AA4A43EEA4B5ACAA9731752EA4EE127E7D,
	FillCupHelper_Awake_mAB9F48D744CD334BAD517E9353D8C99473FCE89D,
	FillCupHelper_SetMachine_mE1DA8E4487793EB91491B5C4AA4854A29B6479E8,
	FillCupHelper_DoFill_m842486F9939896D1DF9D93903E9354D44B117F4C,
	FillCupHelper_FillAnimation_m4CB6D4E55563B0D50DD92FC602B40EE1060C08EF,
	FillCupHelper_FillEnded_mDF3F85EB2C172302B510AD8A9EA4825DFF9D3C05,
	FillCupHelper_OnMouseDown_m580A47EAE3AB1FB555DC214707E8C5FD8C18D491,
	FillCupHelper_AddToPlayerSpot_mFDFA2C68C00C26904CA0298884FE5204816FC479,
	FillCupHelper__ctor_m0D020C7AB618A0C374B6D52D81637AF118295510,
	U3CFillAnimationU3Ed__6__ctor_m9E2A85FEC6A41133BA17CBFDAB6A9199D9898FDD,
	U3CFillAnimationU3Ed__6_System_IDisposable_Dispose_m7FB6A92A604EC0C6B81195D12B834FF3F83D638D,
	U3CFillAnimationU3Ed__6_MoveNext_mE8D995E0144B7F5BB190D6F5FA1B5FBED31A7BCD,
	U3CFillAnimationU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D73BE3695942253F5DB7022D77C6EE41EBD1B79,
	U3CFillAnimationU3Ed__6_System_Collections_IEnumerator_Reset_m285244F619255A6E96DEBCD41380FA7D0ED3D268,
	U3CFillAnimationU3Ed__6_System_Collections_IEnumerator_get_Current_m2422E615EE6D97E487460A010221087439FB9D9D,
	HeatableProduct_Awake_mD27989284289F9EDE136DCC53DDF42A199A6F4EA,
	HeatableProduct_OnEnable_mE839F421F5687FB5F5A34C034C44540EEF3ABFD3,
	HeatableProduct_Start_m513BF1F393BABE7EF2E9BCA6AF461C6B1D84EF70,
	HeatableProduct_OnMouseDown_m809F3B297AB8D69E8B6CEE7AD4B8D2007C19B011,
	HeatableProduct_MoveToMicrowave_m04F1ACF9952AE90CCE0AC514E3C9461018251F67,
	HeatableProduct_AnimateGoingToSlot_mC7AE368ECF9DACE180E5DE23CEC53A73D476ED90,
	HeatableProduct__ctor_m99758E19CA16171904BD59B770EFAA76C2169F02,
	HeatableProduct_U3CU3En__0_m1CA02C6823A245B69B83217988B58F29850502B9,
	HeatableProduct_U3CU3En__1_mE0C903218931D29516E5A99F2BE252E49F679AA0,
	U3CMoveToMicrowaveU3Ed__9__ctor_m3F003BE1A570F4F09B71D5D3EE33802D10157DFF,
	U3CMoveToMicrowaveU3Ed__9_System_IDisposable_Dispose_m32C0A27A283C2464B08C079FA05EA5743C793A0C,
	U3CMoveToMicrowaveU3Ed__9_MoveNext_m943C84FA820D0D675211BC864515BBB01A073C6C,
	U3CMoveToMicrowaveU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA04255AAD683470EB9FDE4CAAD43F5404808EFB1,
	U3CMoveToMicrowaveU3Ed__9_System_Collections_IEnumerator_Reset_mC2C4B21CB18B8FEFB23474AB04B9CE4A7F5872D6,
	U3CMoveToMicrowaveU3Ed__9_System_Collections_IEnumerator_get_Current_mC24AFD4846B1DF6418D79F74A3268A4126F6B12C,
	U3CAnimateGoingToSlotU3Ed__10__ctor_m82B88F80A12A42C091085DF76AA9C18682D40951,
	U3CAnimateGoingToSlotU3Ed__10_System_IDisposable_Dispose_m8F51147B63B854C0821365F509022C9344B49FE1,
	U3CAnimateGoingToSlotU3Ed__10_MoveNext_mB2059FD8B71BD867B3046AA396567A5E0366B8D2,
	U3CAnimateGoingToSlotU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FFC3BF3F10B448155D1E07F3365745CE178E3E4,
	U3CAnimateGoingToSlotU3Ed__10_System_Collections_IEnumerator_Reset_m1E6497D7CC2429B04451B8D3FC7E341B0858F924,
	U3CAnimateGoingToSlotU3Ed__10_System_Collections_IEnumerator_get_Current_m468BCF0F0997F3A20FD9D0715CA4736EF398A9B7,
	Microwave_get_isEmpty_m9D08095A1952B576BF29AFF1DD8C482801A6F632,
	Microwave_Start_m498227BFE23459563397A7E6CF314CE45904CBFD,
	Microwave_SetProduct_mEA595C8917E2D30389AA647AD168E2FFC89B52AF,
	Microwave_OpenMicrowaveAndHeatProduct_mFC7BDF344F6EE2FA1CA7759CCF649BF6591BA4EA,
	Microwave_PlayDoorAnim_m449585915E4CC88A30A97AE208B60B7C2B9C52B0,
	Microwave_Heating_mD64BF9BCD8E04F65F55D075AF30A80D099E0A844,
	Microwave_OnMouseDown_mA1F0D0732C4075D512085D105CB517161D592CD3,
	Microwave__ctor_m1A826BC4616C8085C6F2EF2CCD833A459035D053,
	U3COpenMicrowaveAndHeatProductU3Ed__12__ctor_m9E1B1BA967ACDA5851F7929079E035546A91B73A,
	U3COpenMicrowaveAndHeatProductU3Ed__12_System_IDisposable_Dispose_m49425A50C0BE957152A0B2624DB5C79BDC2CDF04,
	U3COpenMicrowaveAndHeatProductU3Ed__12_MoveNext_m9C79F9AFDC3E41F214F8F265EE48EBC95FD1C6D7,
	U3COpenMicrowaveAndHeatProductU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8348C53100064A94E6BE21E2FAB6428CD3763EA6,
	U3COpenMicrowaveAndHeatProductU3Ed__12_System_Collections_IEnumerator_Reset_m8CC7304E845048F407D2EDB254C7F0EAAF8A93AC,
	U3COpenMicrowaveAndHeatProductU3Ed__12_System_Collections_IEnumerator_get_Current_mB467EDD0021945CE6344828ECC533399B81BACD1,
	U3CPlayDoorAnimU3Ed__13__ctor_mD63C61479F4CC92D0DE9B33F7B9976A589870221,
	U3CPlayDoorAnimU3Ed__13_System_IDisposable_Dispose_m65B4C482440FB2D735B5246969522285035A0D80,
	U3CPlayDoorAnimU3Ed__13_MoveNext_m3B898AFBEE74D25A3532304B1D2BB1B325D604AD,
	U3CPlayDoorAnimU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C3EEB619ECA89E66E26F61BE56292837D26E3F7,
	U3CPlayDoorAnimU3Ed__13_System_Collections_IEnumerator_Reset_mEAD2FC029EA16333F068F7A3036BE4D97DD7DF2B,
	U3CPlayDoorAnimU3Ed__13_System_Collections_IEnumerator_get_Current_m88AC95FEFEFE05DD446379884E5905E7934EBA16,
	U3CHeatingU3Ed__14__ctor_m58FA3FD9B3B2BBBFFC825C6FE952B9B53E161C4E,
	U3CHeatingU3Ed__14_System_IDisposable_Dispose_mDE11C1265C111B1316BA7A62A966C6E3D92A9688,
	U3CHeatingU3Ed__14_MoveNext_m9A6AE9D6B4E352FE14B2A3624CE9B062B111F2DA,
	U3CHeatingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m315B0A8B73D7854AF74B889B7B34351FB3F4D376,
	U3CHeatingU3Ed__14_System_Collections_IEnumerator_Reset_mA62BF73547B0A1F6E7FD5CE3CC27B792B0665EC8,
	U3CHeatingU3Ed__14_System_Collections_IEnumerator_get_Current_mB26C6EF480B8C4006D2C948C25A4CB93587133ED,
	OnClickCoverHelper_OnEnable_m72CA0C827CDC4545E530C5602C4005550D322366,
	OnClickCoverHelper_OnMouseDown_mE4809DCE7BFAA2AE8ED8B3282710B78E6FDA76A2,
	OnClickCoverHelper_ActivateCollider_m84D7536878BA7EFA0C79ADB1D13C0832219F869D,
	OnClickCoverHelper__ctor_m6482AAC78FA66BFCC4D2654E95A00038B596026D,
	OrderGenerator_OnEnable_m65248CB8BF28E3EC028F603C2C7C93D277EBE96D,
	OrderGenerator_OnDisable_m8D8FBC82B19BBA1FF4024FE90477F246F478BD69,
	OrderGenerator_BasicGameEvents_onOrderCancelled_mDFCF8A5D4BAF050A52BC6AC9791BF8AB0039D47B,
	OrderGenerator_BasicGameEvents_onOrderCompleted_mED94FB79877EF1AF2C743E723371BFF2F078E56E,
	OrderGenerator_Start_m0F9DCCF6BCAF003C25CB7F8443642637ACD768EC,
	OrderGenerator_GenerateOrderRoutine_mD8CD4F228F949959C26152E31048C6D6495959FA,
	OrderGenerator_GenerateOrder_m4C699751B3C7F93845A839304D3A0C233E13A457,
	OrderGenerator_GetSpriteForOrder_m7357C137692CF48A99F0519FC93007C1A135642A,
	OrderGenerator__ctor_m88EA0085E585C50ECE16C54F717A6F72FFF603ED,
	U3CGenerateOrderRoutineU3Ed__11__ctor_m198D0C1D1A8B6C98C329FBF0DDEFA2A75A24CBBC,
	U3CGenerateOrderRoutineU3Ed__11_System_IDisposable_Dispose_m19536EC50B60FB9CDCC1E049D7C163C7E24F1BDB,
	U3CGenerateOrderRoutineU3Ed__11_MoveNext_m699F83C0DECB30E288ED8557BF35A25E9EFD901B,
	U3CGenerateOrderRoutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31A50C29268C0CB68C77E839AE8476F7C9F0AD14,
	U3CGenerateOrderRoutineU3Ed__11_System_Collections_IEnumerator_Reset_mA6287E3DC5B7D6B63CA2A7AB61D665046EB5F20A,
	U3CGenerateOrderRoutineU3Ed__11_System_Collections_IEnumerator_get_Current_mD2AA62DD3D224F86D1949357935499985C88E92C,
	PanGameObject__ctor_m51981FD29F760556759C19EA5E138089E2D6C0EC,
	PlayerSlots_OnEnable_m4C4CFFBB180D5F0E8A56FC49370C8C79D23111E9,
	PlayerSlots_BasicGameEvents_onProductDeletedFromSlot_m257EE7310BE616BFF6A83DB344E5BEBE402F123B,
	PlayerSlots_BasicGameEvents_onProductAddedToSlot_mC9E61C79529FB30887C7FF4B2688C665421136C4,
	PlayerSlots_DoEmphasize_m21F266DB698F0AEA67E16AB0C31FF394B8CC340D,
	PlayerSlots_OnDisable_m93BE4E18F5BA58758F1AC3A89C46115073391FD1,
	PlayerSlots_CanHoldItem_m61924EF3CC74127DB40E2EE8E1413A98782B9295,
	PlayerSlots_HoldsItem_mADED3B2858DD66E3C268E0D8DB7A2255B5AC0836,
	PlayerSlots__ctor_mE084FE0E502C68DC78E1A553C2682676A107A229,
	U3CDoEmphasizeU3Ed__6__ctor_m47F5E6CB605E6FDE17AC9AA92CEEBF2BE5C31A82,
	U3CDoEmphasizeU3Ed__6_System_IDisposable_Dispose_m172ACC62445FDAFD1FDBBF265080CD00BC2631AA,
	U3CDoEmphasizeU3Ed__6_MoveNext_m9C53E480F6988BE9C4734FBB9B2AAC83542D4001,
	U3CDoEmphasizeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEF6B1259A7A1F7B7F2729E46A9BDE69C3278468,
	U3CDoEmphasizeU3Ed__6_System_Collections_IEnumerator_Reset_m1766D561AA5D3F587F7E13F00A5E71EF865C7AC5,
	U3CDoEmphasizeU3Ed__6_System_Collections_IEnumerator_get_Current_mDB83930F54F4FCD4593F8432B12F107E2B6711F2,
	Product__ctor_m5647B0C74E26A1451203B6D5EC7498AB33A9F36A,
	ProductGameObject_AnimateGoingToSlot_m117A33A4E7AD26813AEB88647D632D9725D370E6,
	ProductGameObject_CanGoPlayerSlot_m229CDC0878E97E795E1312A207EDF5D8AE18810F,
	ProductGameObject_MoveToPlace_mBA554E360AC594ACFCE88FC874B691A7C2801CE5,
	ProductGameObject__ctor_m76BC962FC16CD4F9A5D8756281127FD3C1B7EE41,
	U3CAnimateGoingToSlotU3Ed__6__ctor_m5FBFCD97E5B6FCBB42950667EC277741AB5D996C,
	U3CAnimateGoingToSlotU3Ed__6_System_IDisposable_Dispose_m4E319A8D244DC00A12BC43EA9A79B06DD65125F1,
	U3CAnimateGoingToSlotU3Ed__6_MoveNext_m7B90325EFAF4003277FC5D404D9EE610C3C4CD1F,
	U3CAnimateGoingToSlotU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC5D01FE6024D3B3E902F33B36D1F6C7DBA0C080,
	U3CAnimateGoingToSlotU3Ed__6_System_Collections_IEnumerator_Reset_m75198CC6DB8F330A8D0CD09192DD67BDD3B26221,
	U3CAnimateGoingToSlotU3Ed__6_System_Collections_IEnumerator_get_Current_mBA1ACC13A9B786C4CEB383D6C0E6F9CA87D9F560,
	U3CMoveToPlaceU3Ed__8__ctor_m01576655B126086265192EE72C27558F232BC8B9,
	U3CMoveToPlaceU3Ed__8_System_IDisposable_Dispose_mE4F35501BBCD480FE86167C6C347F13BB3BB7C59,
	U3CMoveToPlaceU3Ed__8_MoveNext_m497E90157E14A8CB01612E8072684E9D123510AF,
	U3CMoveToPlaceU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE243D65C6D04449E63C62F0E6DBFFD8F0BB69ABC,
	U3CMoveToPlaceU3Ed__8_System_Collections_IEnumerator_Reset_m41AD8AE3E01C10A67DAEB596796A93A1FCF40051,
	U3CMoveToPlaceU3Ed__8_System_Collections_IEnumerator_get_Current_m950C3C5728C3C3AB2DFA46A6E2C0DAC6554B0875,
	ProductManager_InstantiatePlaceHolder_m435F9737C238595DF18EC7BB2B527136CECD614A,
	ProductManager_OnEnable_m7C5DEAD34B8285582E7C7CA97BA00068570E41FB,
	ProductManager_OnDisable_m53B70DEA6E030E4000A0BEF68A8C8D136E11556C,
	ProductManager_RemoveProduct_mD5D04AE35D06BF9DB7528691BEBA67D4F2C9AD26,
	ProductManager__ctor_m16ECC2B795D688D82BEC444EC8266CC1F2379D14,
	U3CU3Ec__DisplayClass6_0__ctor_m2F3C6325773FC7FAB353B9DA8299FB5BA3251EB5,
	U3CU3Ec__DisplayClass6_0_U3CRemoveProductU3Eb__0_m04D3F4ECF8A1810775F6F84E1B9E9B237F5C05BB,
	ProductWithCover_OnEnable_m9E312BC420860043C98C79CF952AD94A33E7F4A6,
	ProductWithCover_HandleCoverCloseClick_m64F60F104B4F4FB965CEB21D6267DCDFF199C6DC,
	ProductWithCover_OnMouseDown_mE32486CBB85F8510C948F4075AEDB222C51DB38E,
	ProductWithCover_OpenCloseDisplay_m6DBB4D15F3CCB847184FE08FF3FBEA370AB7F050,
	ProductWithCover__ctor_m4FCBE9E9EC27B5CDAB7D70F4C1F10DC3142B7183,
	U3COpenCloseDisplayU3Ed__9__ctor_m1A4341FA641B7472C06FD095560628D69D4992CF,
	U3COpenCloseDisplayU3Ed__9_System_IDisposable_Dispose_mB6AFE25DEF3FD1764EF7162DBB65E558BD2956C7,
	U3COpenCloseDisplayU3Ed__9_MoveNext_m4D3D425235D8BD9C19FA5F407CB392C411980FB6,
	U3COpenCloseDisplayU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2C2A4B4FFD46C93D601FB195C0DD00C1B714EDF,
	U3COpenCloseDisplayU3Ed__9_System_Collections_IEnumerator_Reset_m1889E541D87F29D4A212D20800502374ABC2C5A3,
	U3COpenCloseDisplayU3Ed__9_System_Collections_IEnumerator_get_Current_mA2081CC28629488E1E109471FCFB2AE3E044A5BC,
	ProgressHelper_UpdateProcessUI_mF8B00E2038C279D8952A794DDE1B51E0EFF03BA5,
	ProgressHelper_ToggleHelper_m38701100C1E8DB89D517DB5C10E228D3C238E3EA,
	ProgressHelper__ctor_mF7C45D2595C04F230DB8553C903E1DFA150D8200,
	ReadyToServe_Awake_m4F566975C4653D8686E0DFEE34ACAB809ACD1C96,
	ReadyToServe_OnMouseDown_mB04EE72C791D73F6E00858447019078F63BBF692,
	ReadyToServe_AnimateGoingToSlot_m0F3ACECF0313BA019C3B1BAD7BB41CF614B80EDA,
	ReadyToServe__ctor_mAE30808AFDEA56FB2222DC8B8DED7D34D650B2E8,
	ReadyToServe_U3CU3En__0_mEB7F8A17472A9E93E040DB70DEBBBBFF619407F5,
	U3CAnimateGoingToSlotU3Ed__4__ctor_m5460DF2427802D1E72E4EE164B25C8EBE31B67BF,
	U3CAnimateGoingToSlotU3Ed__4_System_IDisposable_Dispose_m0732F9E25892ACFBD974573453F4F6982FC9024D,
	U3CAnimateGoingToSlotU3Ed__4_MoveNext_m1EC3A87ACE589FA40B27AA77E4C9D84B574DB9D4,
	U3CAnimateGoingToSlotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1733903575DC6B12AC472ECF868C8A56506E688,
	U3CAnimateGoingToSlotU3Ed__4_System_Collections_IEnumerator_Reset_mCBFBB6AF12B01D67891781827C02EB9EE3D0E095,
	U3CAnimateGoingToSlotU3Ed__4_System_Collections_IEnumerator_get_Current_m9A01F59D52712803F122835F61192CD5FA21EC60,
	ServeOrder_ServeMe_mBF3AF682543FE9E13A39D9CD06C018C376200140,
	ServeOrder_OrderCompleted_mE97748C8F780C1B3B46BD0EEFDACBCE1E893BBF7,
	ServeOrder_DoEmphasize_mF4B0036684A2AFFA2A6001D6CAD1F2B732C3614E,
	ServeOrder_SetOrder_m5840AF15E45156B63DB631F74072B3F64C293110,
	ServeOrder_SetSprite_mAF225D0245B68C781F3EE8D81CA9F6A26B5679CF,
	ServeOrder_Update_mD2C2EE6E121750165A1F65E210BA8205EAF872BC,
	ServeOrder_CancelOrder_m5A5FDA12C6B6615D4F1816B89667F6CC4E0671D5,
	ServeOrder__ctor_mE87E4FA667474321B80B0ED5E60D077DC74DA153,
	U3CDoEmphasizeU3Ed__9__ctor_m4ECB7CDF7C1F55E4481C977AF7E0CD1E4F490783,
	U3CDoEmphasizeU3Ed__9_System_IDisposable_Dispose_mDFA6F3DBC548AC3C44B04450C76E6210D1ED4B9A,
	U3CDoEmphasizeU3Ed__9_MoveNext_m6EF8EBE47F79014C77EF74731DBC2347310F61A9,
	U3CDoEmphasizeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB74B6175EF27207C707CE720527CC6D54F83A035,
	U3CDoEmphasizeU3Ed__9_System_Collections_IEnumerator_Reset_mE041ADE182384DA5E9124EF16BFB74069DDBB56E,
	U3CDoEmphasizeU3Ed__9_System_Collections_IEnumerator_get_Current_mDE8F62E974A87EDC67452E8F86FB0CFC33620566,
	StoveGameObject_DoDoorAnimationsIfNeeded_mF7AF29FAD713721DAD2F701E9468941B809F393F,
	StoveGameObject_StartCooking_m824D97312F26FF200776D142D9402D56854C524F,
	StoveGameObject_PlayDoorAnim_m727447CC6B1000FEB4A4DF2C59FC01F525D20A20,
	StoveGameObject_OnMouseDown_m624AF76ED2C442A977D912A3B78CBF29C6AC5D09,
	StoveGameObject__ctor_mBD5FE2677B66D1B342388680E4C6EF4572B21C26,
	U3CPlayDoorAnimU3Ed__6__ctor_m73CD25431474B87FE24908859F89355F82A49A99,
	U3CPlayDoorAnimU3Ed__6_System_IDisposable_Dispose_m83DAC6CC43BC1C2F8FE99D4F8982653BD81472DA,
	U3CPlayDoorAnimU3Ed__6_MoveNext_m3808DAE84402D229590274037E335C3EC60B8B8F,
	U3CPlayDoorAnimU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF55B0B53AA06655DC68155F11D95B1424EA45D75,
	U3CPlayDoorAnimU3Ed__6_System_Collections_IEnumerator_Reset_m07FD0F8134565B4AB799F96F3D8CCFE9CB464FF2,
	U3CPlayDoorAnimU3Ed__6_System_Collections_IEnumerator_get_Current_m236A2BE70F5D1032BE95557DD601CAF447FE701F,
	BasicCameraControl_Update_m3EDB55A0971045938B92E70EEDE61445BFD6AB26,
	BasicCameraControl_LateUpdate_mB862A9C300B781C2CCB0FD511C91B07AE98A34E4,
	BasicCameraControl__ctor_m9CFF652C274B4BD867EF139ABD57CE23D28517DA,
	BasicGameEvents_add_onOrderCancelled_m38509E2701FC60C1F728AA68F37030EECBC5D93E,
	BasicGameEvents_remove_onOrderCancelled_m4F0FC7F85E2B4A20A6E1233F3DC90984AA507765,
	BasicGameEvents_RaiseOnOrderCancelled_mA92233A4E1FA7331707E9BDAA18DE58380698EA7,
	BasicGameEvents_add_onOrderCompleted_mB5789734C8152A8629E52C1DC59C8BC8D64C367F,
	BasicGameEvents_remove_onOrderCompleted_m99980DC1CEF7B6F38F560EE79C00E476CD12DB9C,
	BasicGameEvents_RaiseOnOrderCompleted_mF29A8772748E7A07BAD7505AB14A3C6E36DED2ED,
	BasicGameEvents_add_onProductAddedToSlot_m7A95E79212287B2A53D3B29354111709F581AA28,
	BasicGameEvents_remove_onProductAddedToSlot_m78BDF72AAC02D690B7B0201B4F5F7B1FCE26184D,
	BasicGameEvents_RaiseOnProductAddedToSlot_m40CEC2748EF5B29A546BC9B214BAD678538D6338,
	BasicGameEvents_add_onProductDeletedFromSlot_mDF102E17AAC799C986D821E156521221D44C27D3,
	BasicGameEvents_remove_onProductDeletedFromSlot_m089E2179B14FA531B9A7C13DE9F5686704E03917,
	BasicGameEvents_RaiseOnProductDeletedFromSlot_mEBDB08BFC5C0C000A30D764769D48B60006139DF,
	BasicGameEvents_add_onPlaceHolderRequired_mAC31AE207AA792B5F6CCE028A34A70CEDFF5C235,
	BasicGameEvents_remove_onPlaceHolderRequired_m9452F0B44B3FCCD76CD4CB9B3B1391A5E3CF415D,
	BasicGameEvents_RaiseInstantiatePlaceHolder_mD21089052205D67F4985B0F8B64AFD0378781C4F,
	BasicGameEvents__ctor_m02376F6DEDAA4620349247E93549669A64DA6F26,
	OnOrderCancelled__ctor_mF032F851C8585E69E27ED7A7850C4932B9A646B4,
	OnOrderCancelled_Invoke_m155453EE081E04ECDCE766A95CEF163771D348F5,
	OnOrderCancelled_BeginInvoke_mB430EDFF54E9D9C276C8E9E818539C0BDBA18B4F,
	OnOrderCancelled_EndInvoke_m43A17E4635CB5043A78475264EFA150960D6CEDE,
	OnOrderCompleted__ctor_m03B5422CAC165D6B4737FDA68344B3B2E790EF92,
	OnOrderCompleted_Invoke_m551F4DC241C943D63B3F16E19C2C680A360E03D0,
	OnOrderCompleted_BeginInvoke_m447A12F1D9E0D5D520541476B68B7C8E3F0DFA6B,
	OnOrderCompleted_EndInvoke_m560667A0F3D665EA0940D03FBFFA071D6340C110,
	OnProductAddedToSlot__ctor_mFB35C4601A92249E50E1B7119D9E805F6D08699B,
	OnProductAddedToSlot_Invoke_m0CA5576864E5B068069E25567E4320468CC5F472,
	OnProductAddedToSlot_BeginInvoke_m928D2B4C211D24036612BFE32ADCA9663C545C6B,
	OnProductAddedToSlot_EndInvoke_mE23636E6E4885A690FD0530F25672F3CC35033EC,
	OnProductDeletedFromSlot__ctor_mDF8370623788671109BF3E133329AF55A5F03F14,
	OnProductDeletedFromSlot_Invoke_m5317593E62E4633443A46CC41E48149BEE9EC62C,
	OnProductDeletedFromSlot_BeginInvoke_m69CC86787C55B32C575E54AFBC949406E14C8459,
	OnProductDeletedFromSlot_EndInvoke_m397399989A72C1F973CC67A9CECA289A5AD8BDE3,
	OnPlaceHolderRequired__ctor_mCA70F750633D199CAEBB4B9967DD416BEC2EC803,
	OnPlaceHolderRequired_Invoke_m01376F4744A488C163BDF3E915726C4B0C47388B,
	OnPlaceHolderRequired_BeginInvoke_mF60F811D7EB81A0AA70A814431E64B211D8F11D4,
	OnPlaceHolderRequired_EndInvoke_m75DA679C1B98A7EC86E86163B36A4AAAE1B4CA6C,
	SliceHelper__ctor_m545D86008EC90C178914453F94BA793B7A7FD0E3,
};
static const int32_t s_InvokerIndices[316] = 
{
	3190,
	3190,
	3190,
	3190,
	3190,
	3190,
	3190,
	2609,
	1150,
	3190,
	4502,
	3190,
	3183,
	3190,
	3190,
	2609,
	2673,
	815,
	2609,
	3190,
	3190,
	3190,
	3190,
	3190,
	1151,
	3109,
	3190,
	3190,
	3190,
	3109,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	3190,
	3190,
	2319,
	3190,
	3053,
	3109,
	3190,
	2319,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	3183,
	3190,
	3053,
	2609,
	3190,
	1793,
	3109,
	3190,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	3190,
	3190,
	3190,
	3190,
	2609,
	3190,
	3190,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	2609,
	2639,
	2311,
	3190,
	3190,
	3190,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	3190,
	3190,
	3109,
	3109,
	3190,
	2319,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3053,
	3190,
	1473,
	3109,
	1135,
	3109,
	3190,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	3190,
	3190,
	3190,
	3190,
	2592,
	1376,
	3190,
	2311,
	3190,
	2307,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	2592,
	2592,
	2307,
	3190,
	1846,
	1846,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	508,
	3109,
	3053,
	2319,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	819,
	3190,
	3190,
	1846,
	3190,
	3190,
	1865,
	3190,
	3190,
	3190,
	1135,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	1498,
	2555,
	3190,
	3190,
	3190,
	3109,
	3190,
	3109,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	3109,
	1376,
	2609,
	3190,
	3190,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	2609,
	1135,
	3190,
	3190,
	2592,
	3190,
	3053,
	3109,
	3190,
	3109,
	3190,
	3190,
	3190,
	4780,
	4780,
	4776,
	4780,
	4780,
	4399,
	4780,
	4780,
	4776,
	4780,
	4780,
	4776,
	4780,
	4780,
	4057,
	3190,
	1468,
	2592,
	665,
	2609,
	1468,
	1376,
	454,
	2609,
	1468,
	2592,
	665,
	2609,
	1468,
	2592,
	665,
	2609,
	1468,
	819,
	245,
	2609,
	3190,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	316,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
